/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/product.js":
/*!*********************************!*\
  !*** ./resources/js/product.js ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var Product = /*#__PURE__*/function () {
  function Product() {
    _classCallCheck(this, Product);

    this.$autoSku = $('#auto_sku');
    this.$description = $('#description');
    this.$btnGallery = $('.btn_gallery');
    this.$category = $('#category');
    this.$sku = $('#sku');
    this.$chooseImagesContainer = $('#choose_images_container');
    this.init();
  }

  _createClass(Product, [{
    key: "init",
    value: function init() {
      var _this = this;

      this.initCKEditor();
      this.initSelectThumb();
      this.initPriceInput();
      this.$autoSku.on('change', function () {
        if (_this.$autoSku.prop('checked')) {
          _this.$sku.val('').prop('disabled', true);
        } else {
          _this.$sku.prop('disabled', false);
        }
      });
      $(document).on('click', '.btn_choose_images', function (event) {
        event.preventDefault();
        var imageBox = $(event.target).parents('.image-box');
        CKFinder.popup({
          chooseFiles: true,
          width: 1000,
          height: 700,
          connector: '/ckfinder/connector',
          onInit: function onInit(finder) {
            finder.on('files:choose', function (evt) {
              var file = evt.data.files.first();
              imageBox.find('input').val(file.getUrl());
              imageBox.find('img').attr('src', file.getUrl());

              if (imageBox.data('success') !== true) {
                imageBox.attr('data-success', 'true');

                _this.$chooseImagesContainer.append("<div class=\"image-box\" data-success=\"false\" style=\"width: max-content;display: inline-block;\">\n                            <input type=\"hidden\" value=\"/vendor/media/placeholder.png\" class=\"image-data\" data-default=\"/vendor/media/placeholder.png\">\n                            <div class=\"preview-image-wrapper \">\n                                <img src=\"/vendor/media/placeholder.png\" alt=\"preview image\" class=\"preview_image\" width=\"150\">\n                                <a class=\"btn_remove_choose_image\" title=\"Remove image\" href=\"#\">\n                                    <i class=\"fa fa-times\"></i>\n                                </a>\n                            </div>\n                            <div class=\"image-box-actions\">\n                                <a href=\"#\" class=\"btn_choose_images\">\n                                    Ch\u1ECDn h\xECnh\n                                </a>\n                            </div>\n                        </div>");
              }
            });
          }
        });
      });
      $(document).on('click', '.btn_remove_choose_image', function (event) {
        event.preventDefault();
        var imageBox = $(event.target).parents('.image-box');
        imageBox.remove();

        if (_this.$chooseImagesContainer.find('.image-box').length === 0) {
          _this.$chooseImagesContainer.html("<div class=\"image-box\" data-success=\"false\" style=\"width: max-content;display: inline-block;\">\n                            <input type=\"hidden\" value=\"/vendor/media/placeholder.png\" class=\"image-data\" data-default=\"/vendor/media/placeholder.png\">\n                            <div class=\"preview-image-wrapper \">\n                                <img src=\"/vendor/media/placeholder.png\" alt=\"preview image\" class=\"preview_image\" width=\"150\">\n                                <a class=\"btn_remove_choose_image\" title=\"Remove image\" href=\"#\">\n                                    <i class=\"fa fa-times\"></i>\n                                </a>\n                            </div>\n                            <div class=\"image-box-actions\">\n                                <a href=\"#\" class=\"btn_choose_images\">\n                                    Ch\u1ECDn h\xECnh\n                                </a>\n                            </div>\n                        </div>");
        }
      });
      $('.submit-form').click(function (event) {
        var imageBoxs = $('[data-success=true]');

        if (imageBoxs.length === 0) {
          $('#images').val(JSON.stringify([]));
        } else {
          // let arr = imageBoxs.toArray().reduce((current, next) => {
          //     let input = $(next).find('input');
          //     current.push(input.val())
          //     return current;
          // }, []);
          var arr = [];

          for (var i = 0; i < imageBoxs.length; i++) {
            var input = $(imageBoxs[i]).find('input');
            arr.push(input.val());
          }

          $('#images').val(JSON.stringify(arr));
        }

        $('form').submit();
      });
      $('input[categories]').on('change', function (event) {
        var check = $(event.target).prop('checked');
        var type = $(event.target).data('cat');

        if (check == true) {
          if (type == 2) {
            $('input[data-cat=1]').prop('disabled', true).parent().addClass('checkbox-disabled');
          } else {
            $('input[data-cat=2]').prop('disabled', true).parent().addClass('checkbox-disabled');
          }
        } else {
          var count = $('input[data-cat=' + type + ']:checked');

          if (count.length == 0) {
            $('input[categories]').prop('disabled', false).parent().removeClass('checkbox-disabled');
          }
        }
      });
    }
  }, {
    key: "initCKEditor",
    value: function initCKEditor() {
      if (window.CKEDITOR !== undefined) {
        var config = {
          filebrowserImageBrowseUrl: '/ckfinder/browser',
          filebrowserImageUploadUrl: $('meta[name="csrf-token"]').attr('content'),
          filebrowserWindowWidth: '1200',
          filebrowserWindowHeight: '750',
          height: 500,
          allowedContent: true
        };
        CKEDITOR.replace(this.$description[0], config); // $('[data-action=destroy-editor]')
        //     .off('click')
        //     .on('click', event => {
        //         event.preventDefault();
        //         if (CKEDITOR.instances['content']) {
        //             CKEDITOR.instances['content'].destroy();
        //         } else {
        //             CKEDITOR.replace(document.getElementById('content'), config);
        //         }
        //     })
      }
    }
  }, {
    key: "initSelectThumb",
    value: function initSelectThumb() {
      this.$btnGallery.click(function (event) {
        event.preventDefault();
        CKFinder.popup({
          chooseFiles: true,
          width: 1000,
          height: 700,
          onInit: function onInit(finder) {
            finder.on('files:choose', function (evt) {
              var file = evt.data.files.first();
              $('#thumbnail-box').find('input').val(file.getUrl());
              $('#thumbnail-box').find('img').attr('src', file.getUrl());
            });
          }
        });
      });
    } // initSelectCategory() {
    //     this.$category.select2({
    //         placeholder: 'Chọn chuyên mục',
    //         ajax: {
    //             url: $('#category').data('url'),
    //             headers: {'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')},
    //             method: 'POST',
    //             dataType: 'json',
    //             delay: 250,
    //             data: function (params) {
    //                 return {
    //                     term: params.term,
    //                     page: params.page,
    //                 };
    //             },
    //             processResults: function (data, params) {
    //                 params.page = params.page || 1;
    //                 return {
    //                     results: $.map(data.data, function (item) {
    //                         return {
    //                             text: item.name,
    //                             id: item.id,
    //                             title: item.name
    //                         }
    //                     }),
    //                     pagination: {
    //                         more: (params.page * 10) < data.total
    //                     }
    //                 };
    //             },
    //             cache: true
    //         },
    //         escapeMarkup: markup => markup
    //     });
    //
    //     if (this.$category.data('data') !== undefined) {
    //         let data = this.$category.data('data');
    //         let newOption = new Option(data.name, data.id, true, false);
    //         this.$category.append(newOption).trigger('change');
    //     }
    // }

  }, {
    key: "initPriceInput",
    value: function initPriceInput() {
      $('#price').keydown(function (event) {
        if (event.keyCode >= 48 && event.keyCode <= 57 || event.keyCode === 8) {
          return true;
        } else {
          event.preventDefault();
        }
      });
      $('#price').keyup(function (event) {
        var x = $(event.target).val().toString().replaceAll('.', '').replaceAll(',', '');

        if (x !== '') {
          x = parseInt(x).toLocaleString('en-US', {
            style: 'currency',
            currency: 'VND'
          }) + '';
          $(event.target).val(x.replace('₫', ''));
        }
      });
    }
  }]);

  return Product;
}();

$(function () {
  new Product();
});

/***/ }),

/***/ 1:
/*!***************************************!*\
  !*** multi ./resources/js/product.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\laragon\www\chutdt\resources\js\product.js */"./resources/js/product.js");


/***/ })

/******/ });