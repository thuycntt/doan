const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/category.js', 'public/vendor/js');
mix.js('resources/js/product.js', 'public/vendor/js');
mix.js('resources/js/menuitem.js', 'public/vendor/js');
mix.js('resources/js/question.js', 'public/vendor/js');
mix.js('resources/js/diary.js', 'public/vendor/js');
mix.js('resources/js/dashboard.js', 'public/vendor/js');
