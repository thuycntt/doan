### API for auth

#### Login

**REQUEST**

Post form-data:
- username: required
- password: required


```
{domain}/api/login
```
**RESPONSE**
```
{
    "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9hZG1pbi55ZW52bi5pbmZvXC9hcGlcL2xvZ2luIiwiaWF0IjoxNjAzMTIxMzk2LCJleHAiOjE2MDMxMjg1OTYsIm5iZiI6MTYwMzEyMTM5NiwianRpIjoiYXJ2d0VTRmFocHRzSjZ0RSIsInN1YiI6MiwicHJ2IjoiMWQwYTAyMGFjZjVjNGI2YzQ5Nzk4OWRmMWFiZjBmYmQ0ZThjOGQ2MyJ9.8u39aq5rAWOo_NQixv0zE-JQedjAsHSBUEhDWuECKwU",
    "token_type": "bearer",
    "expires_in": 7200,
    "expires_at": 1603128596,
    "user": {
        "id": 2,
        "username": "thuyle",
        "dob": "1998-02-19",
        "phone": "0971381136",
        "address": "205 Phan Xích Long, Phường 2, Phú Nhuận, Thành phố Hồ Chí Minh",
        "about": "Chút chút",
        "avatar": "vendor/media/user-default.jpg",
        "name": "Lê Thuỷ",
        "email": "thuylekm16gts@gmail.com",
        "created_at": "2020-10-19T14:47:18.000000Z",
        "updated_at": "2020-10-19T14:47:18.000000Z"
    }
}
```


#### Update customer info

**REQUEST**

Post form-data:
- customer_id: required
- dob: nullable | format YYYY-MM-DD
- phone: nullable
- about: nullable
- avatar: nullable | file image
- name: nullable


```
{domain}/api/customer/customer-update-info
```
**RESPONSE**
```
{
    "id": 2,
    "username": "thuyle",
    "dob": "1998-02-19",
    "phone": "0971381136",
    "address": "205 Phan Xích Long, Phường 2, Phú Nhuận, Thành phố Hồ Chí Minh",
    "about": "sfdfd",
    "avatar": "http://admin.chutdt.io/customer_avatar/2/chao-mass-ga.png",
    "name": "Lê Thuỷ",
    "email": "thuylekm16gts@gmail.com",
    "created_at": "2020-10-25T02:51:42.000000Z",
    "updated_at": "2020-10-26T14:30:39.000000Z"
}
```


#### Logout

**REQUEST**
```
{domain}/api/logout
```
**RESPONSE**
```
{
    "message": "Logout successful!"
}
```

