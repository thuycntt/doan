### API for get address

#### Get list province

**REQUEST**

Get param:
- key: optional | use for search

```
{domain}/api/address/list-provinces?key=Hà Nội
```
**RESPONSE**
```
[
    {
        "name": "Hà Nội",
        "slug": "ha-noi",
        "id": 1,
        "prefix": "Thành Phố"
    },
    {
        "name": "Hà Giang",
        "slug": "ha-giang",
        "id": 2,
        "prefix": "Tỉnh"
    },
    ...
]
```


#### Get list districts

**REQUEST**

Get param:
- key: optional | use for search
- province_id: ID of province

```
{domain}/api/address/list-districts?province_id=1
```
**RESPONSE**
```
[
    {
        "name": "Ba Đình",
        "slug": "ba-dinh",
        "id": 1,
        "prefix": "Quận",
        "province_id": 1
    },
    {
        "name": "Hoàn Kiếm",
        "slug": "hoan-kiem",
        "id": 2,
        "prefix": "Quận",
        "province_id": 1
    },
    ...
]
```


#### Get list wards

**REQUEST**

Get param:
- key: optional | use for search
- district_id: ID of district

```
{domain}/api/address/list-wards?district_id=1
```
**RESPONSE**
```
[
    {
        "name": "Phúc Xá",
        "slug": "phuc-xa",
        "id": 1,
        "prefix": "Phường",
        "district_id": 1
    },
    {
        "name": "Trúc Bạch",
        "slug": "truc-bach",
        "id": 2,
        "prefix": "Phường",
        "district_id": 1
    },
    ...
]
```

