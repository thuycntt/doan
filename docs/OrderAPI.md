### API for order

#### Get list order by customer

**REQUEST**

Get param:
- page: default 1
- per_page: default 10
- customer_id: required

```
{domain}/api/order/list-order?customer_id=2
```

**list status**
- STATUS_VERIFY = 1;
- STATUS_PENDING = 2;
- STATUS_GETTING_PRODUCT = 3;
- STATUS_SHIPPING = 4;
- STATUS_CANCEL = 6;
- STATUS_COMPLETE = 5;

**RESPONSE**
```
{
    "current_page": 1,
    "data": [
        {
            "id": 2,
            "uuid": "2DuFiC9Gqf2nHaf0",
            "customer_id": 1,
            "email": "giang@gmail.com",
            "full_name": "Giang Nguyễn",
            "phone": "0971828111",
            "address": "131/10 TCH 18, Phường Phúc Xá, Quận Ba Đình. Thành Phố Hà Nội",
            "note": "Giao som nha",
            "status": 1,
            "created_at": "11/10/2020 05:44",
            "updated_at": "11/10/2020 05:44"
        },
        {
            "id": 1,
            "uuid": "AqGeBauafhvbqyBg",
            "customer_id": 1,
            "email": "giang@gmail.com",
            "full_name": "Giang Nguyễn",
            "phone": "0971828111",
            "address": "131/10 TCH 18, Phường Phúc Xá, Quận Ba Đình. Thành Phố Hà Nội",
            "note": "Giao som nha",
            "status": 5,
            "created_at": "11/10/2020 01:09",
            "updated_at": "11/10/2020 01:11"
        }
    ],
    "first_page_url": "http://admin.chutdt.io/api/order/list-order?page=1",
    "from": 1,
    "last_page": 1,
    "last_page_url": "http://admin.chutdt.io/api/order/list-order?page=1",
    "next_page_url": null,
    "path": "http://admin.chutdt.io/api/order/list-order",
    "per_page": 10,
    "prev_page_url": null,
    "to": 2,
    "total": 2
}
```
#### Get list transaction by customer

**REQUEST**

Get param:
- page: default 1
- per_page: default 10
- customer_id: required

```
{domain}/api/order/list-transaction?customer_id=2
```
**list status**

- STATUS_PENDING = 1;
- STATUS_SUCCESS = 2;
- STATUS_CANCEL = 3;
- STATUS_FAILED = 4;

**RESPONSE**
```
{
    "current_page": 1,
    "data": [
        {
            "id": 5,
            "uuid": "8ea26ba5-58d1-4b37-87f5-22d7a50b3fd9",
            "type": 2,
            "order_id": 5,
            "customer_id": 4,
            "transaction_info": "Thanh toán COD cho đơn hàng eobzi5bv9LlvHy7a",
            "data": null,
            "status": 1,
            "amount": "446000",
            "created_at": "2020-09-30T10:10:49.000000Z",
            "updated_at": "2020-09-30T10:10:49.000000Z"
        },
        ...
    ],
    "first_page_url": "http://chutdt.io/api/order/list-transaction?page=1",
    "from": 1,
    "last_page": 1,
    "last_page_url": "http://chutdt.io/api/order/list-transaction?page=1",
    "next_page_url": null,
    "path": "http://chutdt.io/api/order/list-transaction",
    "per_page": 10,
    "prev_page_url": null,
    "to": 2,
    "total": 2
}
```
