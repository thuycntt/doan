## Diary API


#### Get list diaries By Customer

**REQUEST**

Get param:
- per_page: default = 10 
- page: default = 1
- customer_id: required 
```
{domain}/api/diary/get-diary?customer_id=1
```
**RESPONSE**
```
{
    "current_page": 1,
    "data": [
        {
            "id": 1,
            "note": "Putang ninamo bobo la",
            "images": null,
            "seen_at": null,
            "seen_by": null,
            "customer_id": 1,
            "status": 1,
            "date": "20/10/2020",
            "created_at": "20/10/2020 14:55",
            "updated_at": "20/10/2020 14:55"
        }
    ],
    "first_page_url": "http://admin.chutdt.io/api/diary/get-diary?page=1",
    "from": 1,
    "last_page": 1,
    "last_page_url": "http://admin.chutdt.io/api/diary/get-diary?page=1",
    "next_page_url": null,
    "path": "http://admin.chutdt.io/api/diary/get-diary",
    "per_page": 10,
    "prev_page_url": null,
    "to": 1,
    "total": 1
}
```

---
#### Save diary

**REQUEST**

Post form-data:
- note: required
- customer_id: required 
- date: date of diary
- images: array | nullable | array of image file
```
{domain}/api/diary/save-diary
```
**RESPONSE**
```
{
    "note": "asdjasdj asdkalsdjasioj iojoiajioasjdoias",
    "images": [
        "http://chutdt.io/customer_diary/1/28-09-20/119353430_3509200085778670_2270974248825416146_n.jpg",
        "http://chutdt.io/customer_diary/1/28-09-20/119219287_2683268005227130_627196037765024102_n.jpg"
    ],
    "customer_id": "1",
    "seen_at": null,
    "seen_by": null,
    "status": 1,
    "date": "26/09/2020",
    "updated_at": "2020-09-28T13:28:53.000000Z",
    "created_at": "2020-09-28T13:28:53.000000Z",
    "id": 3
}
```
