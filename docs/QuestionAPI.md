#Question API

#### Get list Question By Customer

**REQUEST**

Get param:
- per_page: default = 10 
- page: default = 1
- customer_id: required 
```
{domain}/api/question/list-question?customer_id=1
```
**RESPONSE**
```
{
    "current_page": 1,
    "data": [
        {
            "id": 1,
            "question": "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
            "customer_id": 1,
            "status": 1,
            "user_id": 1,
            "created_at": "12/10/2020 12:53",
            "updated_at": "12/10/2020 12:53"
        }
    ],
    "first_page_url": "http://admin.chutdt.io/api/question/list-question?page=1",
    "from": 1,
    "last_page": 1,
    "last_page_url": "http://admin.chutdt.io/api/question/list-question?page=1",
    "next_page_url": null,
    "path": "http://admin.chutdt.io/api/question/list-question",
    "per_page": 10,
    "prev_page_url": null,
    "to": 1,
    "total": 1
}
```

#### Get list Reply By Question

**REQUEST**

Get param:
- question_id: required 
```
{domain}/api/question/list-reply?question_id=5
```
**RESPONSE**
```
[
    {
        "id": 6,
        "reply": "hihi",
        "images": null,
        "question_id": 5,
        "diary_id": null,
        "user_id": 1,
        "customer_id": null,
        "created_at": "2020-09-22T15:59:36.000000Z",
        "updated_at": "2020-09-22T15:59:36.000000Z",
        "user": {
            "id": 1,
            "username": "chutdt",
            "dob": null,
            "phone": null,
            "address": null,
            "about": null,
            "avatar": null,
            "name": "Bé Chút <3",
            "email": "chutdt@gmail.com",
            "email_verified_at": null,
            "created_at": "2020-09-22T15:12:40.000000Z",
            "updated_at": "2020-09-22T15:12:40.000000Z"
        },
        "customer": null
    },
    {
        "id": 7,
        "reply": "hi",
        "images": null,
        "question_id": 5,
        "diary_id": null,
        "user_id": 1,
        "customer_id": null,
        "created_at": "2020-09-22T15:59:48.000000Z",
        "updated_at": "2020-09-22T15:59:48.000000Z",
        "user": {
            "id": 1,
            "username": "chutdt",
            "dob": null,
            "phone": null,
            "address": null,
            "about": null,
            "avatar": null,
            "name": "Bé Chút <3",
            "email": "chutdt@gmail.com",
            "email_verified_at": null,
            "created_at": "2020-09-22T15:12:40.000000Z",
            "updated_at": "2020-09-22T15:12:40.000000Z"
        },
        "customer": null
    },
    {
        "id": 8,
        "reply": "hihihi",
        "images": null,
        "question_id": 5,
        "diary_id": null,
        "user_id": null,
        "customer_id": 5,
        "created_at": "2020-09-22T16:01:44.000000Z",
        "updated_at": "2020-09-22T16:01:44.000000Z",
        "user": null,
        "customer": {
            "id": 5,
            "username": "joesph91",
            "dob": "1991-01-23",
            "phone": "818-534-9095 x287",
            "address": "11829 Runolfsdottir Prairie Apt. 940\nLake Roel, SD 99874",
            "about": "Possimus aut voluptas eaque. Esse ab tempore pariatur nobis. Reiciendis neque est aperiam aut.",
            "avatar": "vendor/media/user-default.jpg",
            "name": "Asia Dare IV",
            "email": "benny.simonis@example.org",
            "created_at": "2020-09-22T15:12:41.000000Z",
            "updated_at": "2020-09-22T15:12:41.000000Z"
        }
    }
]
```

---
#### Save Question

**REQUEST**

Post form-data:
- question: required
- customer_id: required 
```
{domain}/api/question/save-question
```
**RESPONSE**
```
{
    "question": "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
    "customer_id": "1",
    "status": 1,
    "updated_at": "12/10/2020 12:53",
    "created_at": "12/10/2020 12:53",
    "id": 1
}
```

#### Save Reply of Question

**REQUEST**

Post form-data:
- reply: required
- question_id:required
- customer_id: required 
```
{domain}/api/question/save-reply
```
**RESPONSE**
```
{
    "reply": "Ok man",
    "question_id": "1",
    "customer_id": "1",
    "updated_at": "12/10/2020 13:08",
    "created_at": "12/10/2020 13:08",
    "id": 2
}
```


