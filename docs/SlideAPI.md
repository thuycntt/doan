#SLIDE API

#### Get list Question By Customer

**REQUEST**

Get param:
- slide_id: required
```
{domain}/api/slide/get-slide?slide_id=1
```
**RESPONSE**
```
{
    "slide": {
        "id": 1,
        "created_at": "2020-09-11T13:54:29.000000Z",
        "updated_at": "2020-09-11T13:54:29.000000Z",
        "name": "Slide top",
        "description": "Slide top",
        "position": 1
    },
    "slideItem": [
        {
            "slide_id": 1,
            "url": "https://www.yenviet.com.vn/",
            "image": "/userfiles/images/slide/slide-top/slider_1.jpg",
            "description": null,
            "order": 1,
            "status": 1
        },
        {
            "slide_id": 1,
            "url": "https://www.yenviet.com.vn/",
            "image": "/userfiles/images/slide/slide-top/slider_2.jpg",
            "description": null,
            "order": 2,
            "status": 1
        },
        {
            "slide_id": 1,
            "url": "https://www.yenviet.com.vn/",
            "image": "/userfiles/images/slide/slide-top/slider_3.jpg",
            "description": null,
            "order": 3,
            "status": 1
        }
    ]
}
```
