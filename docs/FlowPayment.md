## FLow payment

Khách hàng sau khi nhập thông tin (tên, địa chỉ, phone - những thông tin này mặc định sẽ lấy từ thông tin của khách hàng khi login) để đặt hàng và sẽ chọn phương thức thanh toán bằng ví momo hoặc thanh toán khi nhận hàng (COD).

#### Khách hàng thanh toán băng ví momo

**Flow thanh toán bằng momo:**

Tạo 1 url cho phía nguời dùng chuyển hướng sang trang thanh toán của momo để thực hiện thanh toán, sau khi thánh toán thành công sẽ chuyển hướng về lại trang của mình kèm những thông tin sau khi thanh toán dưới dạng get param trên url. Bên phía frontend sẽ lấy những param đó để gửi về phía backend kiểm tra và lưu lại thanh toán. Nếu thanh toán thành công sẽ tiến hành đặt hàng cho khác hàng đó

Step 1: 
Gửi request để tạo thanh toán và nhận url:

**REQUEST**

Post form data:
- customer_id: required
- amount: số tiền cần thanh toán
- redirect_url: url để sau khi thanh toán thành công sẽ chuyển hướng về từ trang của momo 

```
{domain}/api/transaction/create-momo
```

**RESPONSE**
```
{
    "url": "https://test-payment.momo.vn/gw_payment/payment/qr?partnerCode=MOMOSPPE20200910&accessKey=WeVrkrHLqF8jr0b6&requestId=2020-09-18 21:16:55&amount=235000&orderId=69032645-6b2b-477f-95f2-230b4774c1da&signature=217c98db878807039e7ebce9c538b55ab69e23f91a6229a4088d5c03da10f9e7&requestType=captureMoMoWallet",
    "transaction_uuid": "69032645-6b2b-477f-95f2-230b4774c1da"
}
```

Step 2: Chuyển hướng khách hàng sang trang thanh toán của momo bằng url đã lấy được

VD:
![](https://i.imgur.com/wUsJ8v1.png)

Step 3: Sau khi thanh toán (thành công hoặc không thành công hoặc khách hàng bấm huỷ bỏ) sẽ đều chuyển hướng về lại url mà đã cung cấp ở step 1 (redirect_url)

VD:

```
http://example.io/url-redirtect?partnerCode=MOMOSPPE20200910&accessKey=WeVrkrHLqF8jr0b6&requestId=2020-09-18%2021:16:55&amount=235000&orderId=69032645-6b2b-477f-95f2-230b4774c1da&orderInfo=test%20momo&orderType=momo_wallet&transId=438107411532&message=Order%20was%20canceled%20by%20user&localMessage=%C4%90%C6%A1n%20h%C3%A0ng%20%C4%91%C3%A3%20b%E1%BB%8B%20hu%E1%BB%B7%20b%E1%BB%8F&responseTime=2020-09-18%2021:13:53&errorCode=49&payType=web&extraData=merchantName=Yen&signature=b1a15c8accc33b0ccce46400fa84c27b252a4692c21f8106d4d87300acd402db
```

Phias frontend sẽ lấy hết những get param trên url và gửi về cho phía backend

**REQUEST**

POST form data:
- partnerCode : reuqired
- accessKey : reuqired
- orderId : reuqired
- localMessage : reuqired
- message : reuqired
- transId : reuqired
- orderInfo : reuqired
- amount : reuqired
- errorCode : reuqired
- responseTime : reuqired
- requestId : reuqired
- payType : reuqired
- orderType : reuqired
- extraData : reuqired
- signature : reuqired
```
{domain}/api/transaction/validate-momo
```

**RESPONSE**
```
{
    "status": ...,
    "message": "...",
    "transaction_uuid": "..."
}
```

Status trả về sẽ có các giá trị sau: 
- 2: thanh toán thành công và phía backend kiểm tra thanh toán thành công
- 3: thanh toán bị huỷ bỏ bởi khách hàng
- 4: thanh toán lỗi hoặc không thành công


#### Thanh toán COD
Nếu khách hàng chọn thanh toán bằng COD thì sẽ ko làm gì cả


## Tiến hành đặt hàng

Sau khi đã thực hiện thanh toán (néu chọn momo thì sẽ gửi request tạo đơn đặt hàng)

**REQUEST**

POST form data:
- email : reuqired
- full_name : reuqired
- phone : reuqired
- address : reuqired
- province_id : reuqired
- district_id : reuqired
- ward_id : reuqired
- customer_id : reuqired
- note: not require | ghi chú thêm của khách hàng
- products : mảng sản phẩm có dạng [{id:1,quantity:1},{id:2,quantity:1},...] đã được JSON.stringfy
- transaction_uuid: not required | chỉ truyền khi khách hàng đã thanh toán thành công bằng momo
```
{domain}/api/order/create
```

**RESPONSE**
```
{
    "order": {
        "uuid": "V5kmmM8JNis5tn3B",
        "customer_id": "1",
        "email": "giang@gmail.com",
        "full_name": "Giang Nguyễn",
        "phone": "0971828111",
        "address": "131/10 TCH 18, Phường Phúc Xá, Quận Ba Đình. Thành Phố Hà Nội",
        "note": "Giao som nha",
        "status": 1,
        "updated_at": "11/10/2020 06:19",
        "created_at": "11/10/2020 06:19",
        "id": 4
    },
    "transaction": {
        "uuid": "Knkg3bXmczqO2dul",
        "type": 2,
        "order_id": 4,
        "customer_id": "1",
        "transaction_info": "Thanh toán COD cho đơn hàng V5kmmM8JNis5tn3B",
        "data": null,
        "status": 1,
        "amount": 672000,
        "updated_at": "11/10/2020 06:19",
        "created_at": "11/10/2020 06:19",
        "id": 4
    }
}
```
