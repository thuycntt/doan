### API for article

#### Get list article

**REQUEST**

Get param:
- page: default 1
- per_page: default 10
- title: default '' | use for search

```
{domain}/api/article/list-article?page=1&per_page=10
```

**status 1 is active article**

**RESPONSE**
```
{
    "current_page": 1,
    "data": [
        {
            "title": "LỢI ÍCH LÀM ĐẸP KÌ DIỆU TỪ TỔ YẾN",
            "slug": "loi-ich-lam-dep-ki-dieu-tu-to-yen",
            "short_content": "Rất nhiều nghiên cứu đã chỉ ra rằng, những người phụ nữ có điều kiện sử dụng Tổ yến thường xuyên mỗi ngày sẽ có làn da đẹp hơn và trẻ lâu hơn so với những người khác. Vì sao lại như vậy? Cùng Yến Việt đi tìm câu trả lời trong bài viết dưới đây nhé!",
            "thumbnail": "http://admin.chutdt.io/userfiles/images/article/loi-ich-cua-yen.png",
            "created_at": "11/09/2020 02:19",
            "author_name": "chutdt",
            "is_feature": 1,
            "status": 1
        }
    ],
    "first_page_url": "http://admin.chutdt.io/api/article/list-article?page=1",
    "from": 1,
    "last_page": 1,
    "last_page_url": "http://admin.chutdt.io/api/article/list-article?page=1",
    "next_page_url": null,
    "path": "http://admin.chutdt.io/api/article/list-article",
    "per_page": "10",
    "prev_page_url": null,
    "to": 6,
    "total": 6
}
```


#### Get article detail

**REQUEST**

Get param:
- id: require

```
{domain}/api/article/article-detail?id=1
```
**RESPONSE**
```
{
    "id": 1,
    "title": "LỢI ÍCH LÀM ĐẸP KÌ DIỆU TỪ TỔ YẾN",
    "slug": "loi-ich-lam-dep-ki-dieu-tu-to-yen",
    "short_content": "Rất nhiều nghiên cứu đã chỉ ra rằng, những người phụ nữ có điều kiện sử dụng Tổ yến thường xuyên mỗi ngày sẽ có làn da đẹp hơn và trẻ lâu hơn so với những người khác. Vì sao lại như vậy? Cùng Yến Việt đi tìm câu trả lời trong bài viết dưới đây nhé!",
    "content": "<p>Rất nhiều nghi&ecirc;n cứu đ&atilde; chỉ ra rằng, những người phụ nữ c&oacute; điều kiện sử dụng Tổ yến thường xuy&ecirc;n mỗi ng&agrave;y sẽ c&oacute; l&agrave;n da đẹp hơn v&agrave; trẻ l&acirc;u hơn so với những người kh&aacute;c. V&igrave; sao lại như vậy? C&ugrave;ng Yến Việt đi t&igrave;m c&acirc;u trả lời trong b&agrave;i viết dưới đ&acirc;y nh&eacute;!</p>\n\n<p>V&igrave; sao Tổ yến c&oacute; thể l&agrave;m đẹp da?</p>\n\n<p>Trong Tổ yến c&oacute; chứa hợp chất Threonine. Một trong những yếu tố ch&iacute;nh h&igrave;nh th&agrave;nh n&ecirc;n Elastine v&agrave; Collagen. Hợp chất n&agrave;y c&oacute; c&oacute; t&aacute;c dụng th&uacute;c đẩy qu&aacute; tr&igrave;nh t&aacute;i tạo c&aacute;c tế b&agrave;o da mặt. Gi&uacute;p da sạch mụn, căng mịn v&agrave; tr&agrave;n đầy sức sống. Ngo&agrave;i ra, trong Tổ yến c&ograve;n chứa tới 18 loại Axit amin thiết yếu cho sức khỏe.&nbsp;</p>\n\n<p><img alt=\"LỢI ÍCH LÀM ĐẸP KÌ DIỆU TỪ TỔ YẾN\" data-thumb=\"original\" original-height=\"433\" original-width=\"577\" src=\"https://bizweb.dktcdn.net/100/344/969/files/blog-1.png?v=1578470817475\" /><br />\nC&oacute; rất nhiều&nbsp;c&aacute;ch&nbsp;l&agrave;m đẹp bằng Tổ yến&nbsp;kh&aacute;c nhau được nhiều người &aacute;p dụng. Điểm t&ecirc;n những c&aacute;ch l&agrave;m đẹp n&agrave;y ngay sau đ&acirc;y:</p>\n\n<p>Đắp mặt nạ Tổ yến:</p>\n\n<p>Đ&acirc;y l&agrave; c&aacute;ch l&agrave;m đẹp quen thuộc v&agrave; cũng kh&aacute; đơn giản từ tổ yến m&agrave; ai cũng c&oacute; thể tự thực hiện được tại nh&agrave;.</p>\n\n<p>C&aacute;ch l&agrave;m mặt nạ tổ yến:</p>\n\n<ul>\n\t<li>Đem Tổ yến kh&ocirc; ng&acirc;m với nước sạch cho mềm ra. Trong l&uacute;c đợi th&igrave; bạn c&oacute; thể bắt đầu l&agrave;m sạch da mặt của m&igrave;nh.</li>\n\t<li>Sau khi Tổ yến đ&atilde; mềm th&igrave; lấy đắp trực tiếp l&ecirc;n da mặt, để từ 20 &ndash; 30 ph&uacute;t cho dưỡng chất thẩm thấu v&agrave;o da v&agrave; rửa sạch bằng nước ấm.</li>\n</ul>\n\n<p>Nếu kh&ocirc;ng sử dụng ngay, bạn cũng c&oacute; thể xay mịn tổ Yến th&agrave;nh bột để sử dụng sau đ&oacute;.</p>\n\n<p>Nấu ch&egrave; Tổ yến:</p>\n\n<p>Trong số những m&oacute;n ăn chế biến từ tổ Yến th&igrave; ch&egrave; tổ Yến l&agrave; m&oacute;n ăn rất được y&ecirc;u th&iacute;ch bởi c&aacute;ch chế biến kh&aacute; đơn giản, nguy&ecirc;n liệu chuẩn bị kh&ocirc;ng qu&aacute; cầu k&igrave; v&agrave; m&ugrave;i vị thơm ngon, hấp dẫn.</p>\n\n<p>C&aacute;ch l&agrave;m: Nếu bạn sử dụng Tổ yến th&ocirc; th&igrave; trước khi nấu, cần ng&acirc;m nước, loại bỏ sạch l&ocirc;ng v&agrave; tạp chất, Nếu sử dụng tổ Yến tinh chế th&igrave; chỉ cần ng&acirc;m trong nước sạch với thời gian từ 29 ph&uacute;t cho đến 60 ph&uacute;t (cho đến khi sợi yến tơi ra). Sau đ&oacute; chưng với đường ph&egrave;n c&aacute;ch thuỷ l&agrave; d&ugrave;ng được. Bạn cũng c&oacute; thể th&ecirc;m một số nguy&ecirc;n liệu kh&aacute;c như t&aacute;o đỏ, hạt sen hay gừng tươi.</p>\n\n<p>HOẶC: Lựa chọn Tổ Yến Thi&ecirc;n Nhi&ecirc;n Nguy&ecirc;n Chất Sấy Thăng Hoa YenViet:</p>\n\n<ul>\n\t<li>Tổ Yến đ&atilde; được l&agrave;m sạch 100% tạp chất.</li>\n\t<li>Chế biến nhanh gọn, chỉ cần chế nước s&ocirc;i đợi khoảng 5p l&agrave; sử dụng được.</li>\n\t<li>Kh&ocirc;ng chất bảo quản.</li>\n\t<li>Nhiều vị cho bạn lựa chọn.</li>\n</ul>\n\n<p><img alt=\"LỢI ÍCH LÀM ĐẸP KÌ DIỆU TỪ TỔ YẾN\" data-thumb=\"original\" original-height=\"468\" original-width=\"592\" src=\"https://bizweb.dktcdn.net/100/344/969/files/blog-2-1.png?v=1578470839857\" /></p>\n\n<p>Cho bạn tha hồ sử dụng tổ Yến mỗi ng&agrave;y, kh&ocirc;ng cần chế biến cầu kỳ m&agrave; gi&aacute; cả lại phải chăng.</p>\n\n<p><em>Để được tư vấn v&agrave; mua h&agrave;ng nhanh ch&oacute;ng, vui l&ograve;ng li&ecirc;n hệ 028 6265 1818 hoặc click giỏ h&agrave;ng ngay.</em></p>",
    "thumbnail": "http://admin.chutdt.io/userfiles/images/article/loi-ich-cua-yen.png",
    "status": 1,
    "is_feature": 1,
    "author_name": "chutdt",
    "author_id": 1,
    "created_at": "11/09/2020 02:19",
    "updated_at": "2020-09-11T09:35:49.000000Z",
    "tags": [
        {
            "id": 1,
            "name": "Tổ yến",
            "slug": "to-yen",
            "description": "Tổ yến",
            "created_at": "2020-09-11T02:19:01.000000Z",
            "updated_at": "2020-09-11T02:19:01.000000Z",
            "pivot": {
                "article_id": 1,
                "tag_id": 1
            }
        },
        ...
    ],
    "list_news": [
        {
            "title": "Khởi nghiệp từ mô hình nuôi chim yến",
            "slug": "khoi-nghiep-tu-mo-hinh-nuoi-chim-yen",
            "short_content": "Ngành nuôi chim yến bắt đầu từ Khánh Hòa, nơi chim yến đầu tiên về ở. Từ năm 1995, cơn bão số 5 đã đẩy đàn chim Yến từ Thái Lan trôi dạt về đảo Khánh Hòa. Năm 1997, Khánh Hòa bắt đầu có tổ yến và mỗi lần nhắc đến yến người ta nghĩ ngay đến Khánh Hòa. Từ Yến Sào Khánh Hòa, một số người dân Quảng Trị đã tìm hiểu, nghiên cứu kỹ thuật, bắt đầu khởi nghiệp từ mô hình nuôi chim yến.",
            "thumbnail": "http://admin.chutdt.io/userfiles/images/article/khoi-nghiep-tu-mo-hinh-nuoi-chim-yen.jpg",
            "created_at": "2020-09-11T09:48:03.000000Z",
            "author_name": "chutdt",
            "is_feature": 1,
            "status": 1
        },
        ...
    ]
}
```
#### Get list article new

**REQUEST**

Get param:
- number: default = 6

```
{domain}/api/article/list-article-new?number=4
```

**status 1 is active article**

**RESPONSE**
```
[
    {
        "id": 6,
        "title": "Khởi nghiệp từ mô hình nuôi chim yến",
        "slug": "khoi-nghiep-tu-mo-hinh-nuoi-chim-yen",
        "short_content": "Ngành nuôi chim yến bắt đầu từ Khánh Hòa, nơi chim yến đầu tiên về ở. Từ năm 1995, cơn bão số 5 đã đẩy đàn chim Yến từ Thái Lan trôi dạt về đảo Khánh Hòa. Năm 1997, Khánh Hòa bắt đầu có tổ yến và mỗi lần nhắc đến yến người ta nghĩ ngay đến Khánh Hòa. Từ Yến Sào Khánh Hòa, một số người dân Quảng Trị đã tìm hiểu, nghiên cứu kỹ thuật, bắt đầu khởi nghiệp từ mô hình nuôi chim yến.",
        "thumbnail": "http://admin.chutdt.io/userfiles/images/article/khoi-nghiep-tu-mo-hinh-nuoi-chim-yen.jpg",
        "created_at": "11/09/2020 09:48",
        "author_name": "chutdt",
        "status": 1
    },
    {
        "id": 5,
        "title": "Nuôi yến trong nhà: 9 yếu tố giúp bạn thành công",
        "slug": "nuoi-yen-trong-nha-9-yeu-to-giup-ban-thanh-cong",
        "short_content": "Việc thiết kế xây dựng nhà nuôi yến và hoàn thiện quy trình nuôi yến trong nhà là cấp thiết để làm cơ sở cho việc phát triển nghề nuôi yến trong nhà trên toàn quốc.",
        "thumbnail": "http://admin.chutdt.io/userfiles/images/article/nuoi-yen-trong-nha-600x480.jpg",
        "created_at": "11/09/2020 09:41",
        "author_name": "chutdt",
        "status": 1
    },
    {
        "id": 4,
        "title": "TỔ YẾN THẬT VÀ NGUYÊN CHẤT - NGUỒN DINH DƯỠNG QUÝ GIÁ",
        "slug": "to-yen-that-va-nguyen-chat-nguon-dinh-duong-quy-gia",
        "short_content": "Theo PGS-TS Ngô Đăng Nghĩa - Viện Công nghệ sinh học và môi trường, Trường Đại học Nha Trang, thời gian gần đây một số nghiên cứu đã ghi nhận tổ yến có tính chất tương tự hormone (hormone - like substances), là nhân tố phát triển (growth factor) cho sự phân bào và các mô. Những chất này sẽ kích thích sự phân chia và phát triển tế bào, thúc đẩy sự phát triển mô và tái sinh mô mới. Do đó tổ yến có cơ sở khoa học khi điều trị bệnh, hồi phục và làm trẻ hóa làn da.",
        "thumbnail": "http://admin.chutdt.io/userfiles/images/article/to-yen-yenviet1.jpg",
        "created_at": "11/09/2020 02:25",
        "author_name": "chutdt",
        "status": 1
    },
    {
        "id": 3,
        "title": "QUY TRÌNH YV-PURENEST: TỔ YẾN THẬT HOÀN TOÀN THIÊN NHIÊN",
        "slug": "quy-trinh-yv-purenest-to-yen-that-hoan-toan-thien-nhien",
        "short_content": "Yến Việt cam kết đưa ra thị trường những sản phẩm tổ yến thật và sạch cho người Việt Nam. Cam kết này được công ty hiện thực hóa bằng quy trình khoa học kiểm định chất lượng YV-PureNest trong chế biến tổ yến thiên nhiên.",
        "thumbnail": "http://admin.chutdt.io/userfiles/images/article/nha-may-yenviet1.jpg",
        "created_at": "11/09/2020 02:23",
        "author_name": "chutdt",
        "status": 1
    }
]
```

#### Get List Tag

**REQUEST**

Get param:
- per_page:default = 10
- page: default = 1

```
{domain}/api/article/list-tag
```
**RESPONSE**
```
{
    "current_page": 1,
    "data": [
        {
            "id": 1,
            "name": "Tổ yến",
            "slug": "to-yen",
            "description": "Tổ yến",
            "created_at": "2020-09-11T02:19:01.000000Z",
            "updated_at": "2020-09-11T02:19:01.000000Z"
        },
       ...
    ],
    "first_page_url": "http://chutdt.io/api/article/list-tag?page=1",
    "from": 1,
    "last_page": 1,
    "last_page_url": "http://chutdt.io/api/article/list-tag?page=1",
    "next_page_url": null,
    "path": "http://chutdt.io/api/article/list-tag",
    "per_page": 10,
    "prev_page_url": null,
    "to": 8,
    "total": 8
}
```
#### Get List Article By Tag

**REQUEST**

Get param:
- per_page:default = 10
- page: default = 1
- tag_id: required

```
{domain}/api/article/list-article-by-tag?tag_id=1
```
**RESPONSE**
```
{
    "current_page": 1,
    "data": [
        {
            "title": "LỢI ÍCH LÀM ĐẸP KÌ DIỆU TỪ TỔ YẾN",
            "slug": "loi-ich-lam-dep-ki-dieu-tu-to-yen",
            "short_content": "Rất nhiều nghiên cứu đã chỉ ra rằng, những người phụ nữ có điều kiện sử dụng Tổ yến thường xuyên mỗi ngày sẽ có làn da đẹp hơn và trẻ lâu hơn so với những người khác. Vì sao lại như vậy? Cùng Yến Việt đi tìm câu trả lời trong bài viết dưới đây nhé!",
            "thumbnail": "http://admin.chutdt.io/userfiles/images/article/loi-ich-cua-yen.png",
            "id": 1
        },
        ...
    ],
    "first_page_url": "http://admin.chutdt.io/api/article/list-article-by-tag?page=1",
    "from": 1,
    "last_page": 1,
    "last_page_url": "http://admin.chutdt.io/api/article/list-article-by-tag?page=1",
    "next_page_url": null,
    "path": "http://admin.chutdt.io/api/article/list-article-by-tag",
    "per_page": 10,
    "prev_page_url": null,
    "to": 4,
    "total": 4
}
```


