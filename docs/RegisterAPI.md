## API for register new customer


**REQUEST**

Post form-data:
- username: required
- password: required 
- password_confirmation: required
- email: required
- name: required
```
{domain}/api/register
```
**RESPONSE**
```
{
    "status": 1,
    "customer": {
        "username": "giangnn",
        "email": "giangnn@gmail.com",
        "name": "Giang Nguyễn",
        "avatar": "vendor/media/user-default.jpg",
        "updated_at": "2020-10-10T13:14:22.000000Z",
        "created_at": "2020-10-10T13:14:22.000000Z",
        "id": 3
    }
}

// OR 

{
    "status": 0,
}
```
Status 1 is register successfully
Status 0 is register failed
