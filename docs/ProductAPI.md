#PRODUCT API

#### Save ProductRate

**REQUEST**

Post form-data:
- product_id: required
- customer_id: required
- rate: required, max = 5, min = 1
- comment: nullable
```
{domain}/api/product/save-product-rate
```
**RESPONSE**
```
{
    "product_id": "1",
    "customer_id": "2",
    "rate": "5",
    "updated_at": "2020-09-22T16:37:14.000000Z",
    "created_at": "2020-09-22T16:37:14.000000Z",
    "id": 1
}
```

#### Get ProductRate 

**REQUEST**

Get param:
- per_page: default = 10
- page: default = 1
- product_id: required
```
{domain}/api/product/get-product-rate?product_id=1
```
**RESPONSE**
```
{
    "current_page": 1,
    "data": [
        {
            "id": 1,
            "product_id": 1,
            "customer_id": 2,
            "rate": 5,
            "comment": null,
            "created_at": "2020-09-22T16:37:14.000000Z",
            "updated_at": "2020-09-22T16:37:14.000000Z",
            "customer": {
                "id": 2,
                "username": "shammes",
                "dob": "1996-11-16",
                "phone": "407-207-1302 x609",
                "address": "447 Lemke Isle\nNorth Angiefurt, CA 35142",
                "about": "Est eos vel corporis impedit modi. Quisquam soluta laborum architecto distinctio ad. Minima ex tempora et ducimus et hic aliquid.",
                "avatar": "vendor/media/user-default.jpg",
                "name": "Nasir Mertz",
                "email": "kovacek.gudrun@example.org",
                "created_at": "2020-09-22T15:12:41.000000Z",
                "updated_at": "2020-09-22T15:12:41.000000Z"
            }
        }
    ],
    "first_page_url": "http://chutdt.io/api/product/get-product-rate?page=1",
    "from": 1,
    "last_page": 1,
    "last_page_url": "http://chutdt.io/api/product/get-product-rate?page=1",
    "next_page_url": null,
    "path": "http://chutdt.io/api/product/get-product-rate",
    "per_page": 10,
    "prev_page_url": null,
    "to": 1,
    "total": 1
}
```
#### list product new

**REQUEST**

Get param:
- number: default = 6
```
{domain}/api/product/list-product-new?number=4
```
**RESPONSE**
```
[
    {
        "sku": "HNNPXQD5",
        "name": "HỘP QUÀ TẶNG - VỊ HẠT SEN",
        "slug": "hop-qua-tang-vi-hat-sen",
        "price": 276000,
        "thumbnail": "http://admin.chutdt.io/userfiles/images/product-image/to-yen-chung-san-hop-qua-tang-vi-hat-sen/6a87b1aec90f3051691e%20(1).png",
        "created_at": "19/10/2020 14:36",
        "discount": 20,
        "rate": 3.6,
        "quantity": 465
    },
    {
        "sku": "T9IJXPFC",
        "name": "Tổ Yến Nguyên Chất Chưng Sẵn Vị Hạt Sen (10% tổ yến) - Hộp 5 Lọ",
        "slug": "to-yen-nguyen-chat-chung-san-vi-hat-sen-10-to-yen-hop-5-lo",
        "price": 203000,
        "thumbnail": "http://admin.chutdt.io/userfiles/images/product-image/to-yen-chung-san-hop-05-lo-vi-hat-sen/4.png",
        "created_at": "19/10/2020 14:27",
        "discount": 0,
        "rate": 3.4,
        "quantity": 255
    },
    {
        "sku": "MDJWCTLR",
        "name": "Tổ Yến Nguyên Chất Chưng Sẵn Vị Hạt Sen (10% tổ yến) - Hộp 1 Lọ",
        "slug": "to-yen-nguyen-chat-chung-san-vi-hat-sen-10-to-yen-hop-1-lo",
        "price": 43000,
        "thumbnail": "http://admin.chutdt.io/userfiles/images/product-image/to-yen-chung-san-hop-01-lo-vi-hat-sen/s1l.png",
        "created_at": "19/10/2020 14:24",
        "discount": 2,
        "rate": 4,
        "quantity": 568
    },
    {
        "sku": "CDJKUG8O",
        "name": "Nước Yến YenViet - Khay 30 lon",
        "slug": "nuoc-yen-yenviet-khay-30-lon",
        "price": 285000,
        "thumbnail": "http://admin.chutdt.io/userfiles/images/product-image/khay-30-lon-nuoc-yen-yenviet/yen-lon-khay.png",
        "created_at": "14/10/2020 14:31",
        "discount": 11,
        "rate": 4.4,
        "quantity": 799
    }
]
```

#### List Product

**REQUEST**

Get param:
- per_page: default = 10;
- page: default = 1;
```
{domain}/api/product/list-product
```
**RESPONSE**
```
{
    "current_page": 1,
    "data": [
        {
            "id": 17,
            "sku": "HNNPXQD5",
            "name": "HỘP QUÀ TẶNG - VỊ HẠT SEN",
            "slug": "hop-qua-tang-vi-hat-sen",
            "price": 276000,
            "quantity": 465,
            "short_description": null,
            "thumbnail": "http://admin.chutdt.io/userfiles/images/product-image/to-yen-chung-san-hop-qua-tang-vi-hat-sen/6a87b1aec90f3051691e%20(1).png",
            "discount": 20,
            "deleted_at": null,
            "created_at": "19/10/2020 21:36",
            "updated_at": "19/10/2020 21:36",
            "rate": 3.6
        },
        ...
    ],
    "first_page_url": "http://admin.chutdt.io/api/product/list-product?page=1",
    "from": 1,
    "last_page": 2,
    "last_page_url": "http://admin.chutdt.io/api/product/list-product?page=2",
    "next_page_url": "http://admin.chutdt.io/api/product/list-product?page=2",
    "path": "http://admin.chutdt.io/api/product/list-product",
    "per_page": 10,
    "prev_page_url": null,
    "to": 10,
    "total": 16
}
```
#### Product Detail

**REQUEST**

Get param:
- sku: required
```
{domain}/api/product/product-detail?sku=CDJKUG8O
```
**RESPONSE**
```
{
    "id": 14,
    "sku": "CDJKUG8O",
    "name": "Nước Yến YenViet - Khay 30 lon",
    "slug": "nuoc-yen-yenviet-khay-30-lon",
    "category_id": 5,
    "price": 285000,
    "quantity": 799,
    "description": "<p><em>L&agrave; thức uống bổ dưỡng tiện lợi, c&oacute; chứa yến thật, ho&agrave;n to&agrave;n thi&ecirc;n nhi&ecirc;n, h&agrave;m lượng Tổ Yến vượt trội. C&oacute; c&ocirc;ng dụng gi&uacute;p bồi bổ cơ thể, m&aacute;t da v&agrave; giải kh&aacute;t.</em></p>\n\n<hr />\n<p><strong>NƯỚC YẾN YENVIET</strong></p>\n\n<p><img data-thumb=\"original\" original-height=\"560\" original-width=\"490\" src=\"https://bizweb.dktcdn.net/100/344/969/files/yen-lon.png?v=1557567042653\" /></p>\n\n<p><b>Thể t&iacute;ch</b>: 190 ml.</p>\n\n<p><b>Th&agrave;nh Phần:</b>&nbsp;Nước, Đường ph&egrave;n (9.5%), Tổ yến 0.1%, Hương yến tổng hợp, chất l&agrave;m d&agrave;y (E406), chất ổn định (E327, E401, E407, E415, E466), chất bảo quản (E202).</p>\n\n<p><b>Thời hạn sử dụng</b>: 24 th&aacute;ng kể từ ng&agrave;y sản xuất.</p>\n\n<p><b>Hướng dẫn sử dụng &amp; Bảo quản</b>: D&ugrave;ng ngon hơn nếu để lạnh. Bảo quản nơi tho&aacute;ng m&aacute;t, tr&aacute;nh &aacute;nh nắng trực tiếp.</p>\n\n<p><i>Để được tư vấn v&agrave; mua h&agrave;ng nhanh ch&oacute;ng, vui l&ograve;ng li&ecirc;n hệ 028 6265 1818 hoặc click giỏ h&agrave;ng ngay.</i></p>",
    "short_description": null,
    "thumbnail": "http://admin.chutdt.io/userfiles/images/product-image/khay-30-lon-nuoc-yen-yenviet/yen-lon-khay.png",
    "discount": 0,
    "deleted_at": null,
    "created_at": "14/10/2020 14:31",
    "updated_at": "14/10/2020 14:31",
    "product_images": [
        {
            "id": 27,
            "image": "http://admin.chutdt.io/userfiles/images/product-image/khay-30-lon-nuoc-yen-yenviet/yen-lon-khay.png",
            "product_id": 14,
            "created_at": "2020-10-14T14:31:48.000000Z",
            "updated_at": "2020-10-14T14:31:48.000000Z"
        },
        ...
    ],
    "category": {
        "id": 5,
        "name": "Nước Yến",
        "slug": "nuoc-yen",
        "parent_id": null,
        "description": "Nước Yến",
        "order": 5,
        "deleted_at": null,
        "created_at": "2020-08-30T02:24:43.000000Z",
        "updated_at": "2020-09-11T13:14:19.000000Z"
    },
    "product_rates": [
        {
            "id": 66,
            "product_id": 14,
            "customer_id": 2,
            "rate": 4,
            "comment": "Sản phẩm dễ uống, có mùi sâm, không quá ngọt. Tuy nhiên, nên thay đổi nắp sản phẩm vì nắp hiệ. tại khó mở và dễ gây đứt tay khách hàng. Mình đã thử mở nắp hộp và bị cứa 1 đường rất dài. Mọi người nên cẩn thận khi mở sản phẩm.",
            "created_at": "31/10/2020 02:58",
            "updated_at": "31/10/2020 02:58",
            "customer": {
                "name": "Lê Thuỷ",
                "id": 2,
                "email": "thuylekm16gts@gmail.com"
            }
        },
        ...
    ],
    "rate": 3.8,
    "product_news": [
        {
            "sku": "HNNPXQD5",
            "name": "HỘP QUÀ TẶNG - VỊ HẠT SEN",
            "slug": "hop-qua-tang-vi-hat-sen",
            "thumbnail": "http://admin.chutdt.io/userfiles/images/product-image/to-yen-chung-san-hop-qua-tang-vi-hat-sen/6a87b1aec90f3051691e%20(1).png",
            "price": 276000,
            "discount": 0
        },
        ...
    ]
}
```
#### List Filter Product

**REQUEST**

Get param:
- category_id: optional
- from_price: optional
- to_price: optional
- order: price, asc
```
{domain}/api/product/list-filter-product?to_price=40000
```
**RESPONSE**
```
{
    "current_page": 1,
    "data": [
        {
            "sku": "CFJJWASF",
            "name": "Cháo Yến Dinh Dưỡng YenViet - Vị Gà",
            "slug": "chao-yen-dinh-duong-yenviet-vi-ga",
            "price": 8000,
            "thumbnail": "http://admin.chutdt.io/userfiles/images/product-image/chao-yen-yenviet-vi-ga/chao-mass-ga.png"
        },
        {
            "sku": "8146SFR7",
            "name": "Cháo Yến Dinh Dưỡng YenViet - Chay Rau Nấm",
            "slug": "chao-yen-dinh-duong-yenviet-chay-rau-nam",
            "price": 8000,
            "thumbnail": "http://admin.chutdt.io/userfiles/images/product-image/chao-yen-yenviet-chay-rau-nam/chao-mass-chay-rau-nam.png"
        },
        {
            "sku": "RQ2MQPUT",
            "name": "Cháo Yến Dinh Dưỡng YenViet - Vị Thịt Bằm",
            "slug": "chao-yen-dinh-duong-yenviet-vi-thit-bam",
            "price": 8600,
            "thumbnail": "http://admin.chutdt.io/userfiles/images/product-image/chao-yen-yenviet-vi-thit-bam/chao-mass-bam.png"
        }
    ],
    "first_page_url": "http://admin.chutdt.io/api/product/list-filter-product?page=1",
    "from": 1,
    "last_page": 1,
    "last_page_url": "http://admin.chutdt.io/api/product/list-filter-product?page=1",
    "next_page_url": null,
    "path": "http://admin.chutdt.io/api/product/list-filter-product",
    "per_page": 10,
    "prev_page_url": null,
    "to": 3,
    "total": 3
}
```


#### List Product new

**REQUEST**

Get param:
- number: default = 6;
```
{domain}/api/product/list-product
```
**RESPONSE**
```
[
    {
        "sku": "HNNPXQD5",
        "name": "HỘP QUÀ TẶNG - VỊ HẠT SEN",
        "slug": "hop-qua-tang-vi-hat-sen",
        "price": 276000,
        "thumbnail": "http://admin.chutdt.io/userfiles/images/product-image/to-yen-chung-san-hop-qua-tang-vi-hat-sen/6a87b1aec90f3051691e%20(1).png",
        "created_at": "19/10/2020 14:36",
        "discount": 20,
        "rate": 3.6,
        "quantity": 465
    },
    ...
]
```


#### List Trending Product

**REQUEST**

Get param:
- number: default = 6;
```
{domain}/api/product/list-trending-product
```
**RESPONSE**
```
[
    {
        "sku": "HNNPXQD5",
        "name": "HỘP QUÀ TẶNG - VỊ HẠT SEN",
        "slug": "hop-qua-tang-vi-hat-sen",
        "price": 276000,
        "thumbnail": "http://admin.chutdt.io/userfiles/images/product-image/to-yen-chung-san-hop-qua-tang-vi-hat-sen/6a87b1aec90f3051691e%20(1).png",
        "created_at": "19/10/2020 14:36",
        "discount": 20,
        "rate": 3.6,
        "quantity": 465
    },
    ...
]
```
#### List Equipment Product

**REQUEST**

Get param:
- per_page: default = 10;
 - page: default = 1;
```
{domain}/api/product/list-equipment
```
**RESPONSE**
```
[
     {
                "id": 19,
                "sku": "VR1MW6WF",
                "name": "AMPLIFIER TEST PT7 (DÙNG PIN PHÁT ÂM THANH)",
                "slug": "amplifier-test-pt7-dung-pin-phat-am-thanh",
                "price": 850000,
                "quantity": 431,
                "short_description": null,
                "thumbnail": "http://admin.chutdt.io/userfiles/images/product-image/amplifier-test-pronest-pt-7-dung-pin-phat-am-thanh/amplifier-test-pronest-pt-7-dung-pin-phat-am-thanh.png",
                "discount": 6,
                "deleted_at": null,
                "created_at": "06/12/2020 17:39",
                "updated_at": "20/12/2020 12:06",
                "rate": 4.2
            },
    ...
]
```
