#CATEGORY API

#### Get Category

**REQUEST**

```
{domain}/api/category/get-category
```
**RESPONSE**
```
[
    {
        "id": 1,
        "name": "Cháo Yến Trẻ Em Nest Grow",
        "slug": "chao-yen-tre-em-nest-grow",
        "parent_id": null,
        "description": "Cháo Yến Trẻ Em Nest Grow",
        "children": [
            {
                "id": 8,
                "name": "Thịt Bằm Rau Củ",
                "slug": "thit-bam-rau-cu",
                "parent_id": 1,
                "description": "Thịt Bằm Rau Củ",
                "order": 1,
                "deleted_at": null,
                "created_at": "2020-08-30T02:25:21.000000Z",
                "updated_at": "2020-08-30T02:25:25.000000Z",
                "children": []
            },
            {
                "id": 9,
                "name": "Bò Bằm Bó Xôi",
                "slug": "bo-bam-bo-xoi",
                "parent_id": 1,
                "description": "Bò Bằm Bó Xôi",
                "order": 2,
                "deleted_at": null,
                "created_at": "2020-08-30T02:25:38.000000Z",
                "updated_at": "2020-08-30T02:25:42.000000Z",
                "children": []
            }
        ]
    },
    {
        "id": 2,
        "name": "Tổ Yến Sấy Thăng Hoa",
        "slug": "to-yen-say-thang-hoa",
        "parent_id": null,
        "description": "Tổ Yến Sấy Thăng Hoa",
        "children": [
            {
                "id": 10,
                "name": "Với Gừng tươi",
                "slug": "voi-gung-tuoi",
                "parent_id": 2,
                "description": "Với Gừng tươi",
                "order": 1,
                "deleted_at": null,
                "created_at": "2020-08-30T02:27:09.000000Z",
                "updated_at": "2020-08-30T02:27:15.000000Z",
                "children": []
            },
            {
                "id": 11,
                "name": "Với Táo đỏ và Hạt sen",
                "slug": "voi-tao-do-va-hat-sen",
                "parent_id": 2,
                "description": "Với Táo đỏ và Hạt sen",
                "order": 2,
                "deleted_at": null,
                "created_at": "2020-08-30T02:27:27.000000Z",
                "updated_at": "2020-08-30T02:27:31.000000Z",
                "children": []
            },
            {
                "id": 12,
                "name": "Với Hạt sen và Nước dừa tươi",
                "slug": "voi-hat-sen-va-nuoc-dua-tuoi",
                "parent_id": 2,
                "description": "Với Hạt sen và Nước dừa tươi",
                "order": 3,
                "deleted_at": null,
                "created_at": "2020-08-30T02:27:38.000000Z",
                "updated_at": "2020-08-30T02:27:42.000000Z",
                "children": []
            },
            {
                "id": 13,
                "name": "Với Hạt sen Nước dừa và Đường Palatinose",
                "slug": "voi-hat-sen-nuoc-dua-va-duong-palatinose",
                "parent_id": 2,
                "description": "Với Hạt sen Nước dừa và Đường Palatinose",
                "order": 4,
                "deleted_at": null,
                "created_at": "2020-08-30T02:27:53.000000Z",
                "updated_at": "2020-08-30T02:27:57.000000Z",
                "children": []
            },
            {
                "id": 14,
                "name": "Với Táo đỏ Hạt sen và Đường Palatinose",
                "slug": "voi-tao-do-hat-sen-va-duong-palatinose",
                "parent_id": 2,
                "description": "Với Táo đỏ Hạt sen và Đường Palatinose",
                "order": 5,
                "deleted_at": null,
                "created_at": "2020-08-30T02:28:08.000000Z",
                "updated_at": "2020-08-30T02:28:14.000000Z",
                "children": []
            }
        ]
    },
    {
        "id": 3,
        "name": "Đông Trùng Hạ Thảo",
        "slug": "dong-trung-ha-thao",
        "parent_id": null,
        "description": "Đông Trùng Hạ Thảo",
        "children": [
            {
                "id": 15,
                "name": "Đông Trùng Hạ Thảo Chưng Sẵn",
                "slug": "dong-trung-ha-thao-chung-san",
                "parent_id": 3,
                "description": "Đông Trùng Hạ Thảo Chưng Sẵn",
                "order": 1,
                "deleted_at": null,
                "created_at": "2020-08-30T02:31:30.000000Z",
                "updated_at": "2020-08-30T02:31:37.000000Z",
                "children": []
            },
            {
                "id": 16,
                "name": "ĐTHT Mật Ong Rừng Nguyên Chất",
                "slug": "dtht-mat-ong-rung-nguyen-chat",
                "parent_id": 3,
                "description": "ĐTHT Mật Ong Rừng Nguyên Chất",
                "order": 2,
                "deleted_at": null,
                "created_at": "2020-08-30T02:31:48.000000Z",
                "updated_at": "2020-08-30T02:31:52.000000Z",
                "children": []
            },
            {
                "id": 17,
                "name": "Cháo Đông Trùng Hạ Thảo",
                "slug": "chao-dong-trung-ha-thao",
                "parent_id": 3,
                "description": "Cháo Đông Trùng Hạ Thảo",
                "order": 3,
                "deleted_at": null,
                "created_at": "2020-08-30T02:32:05.000000Z",
                "updated_at": "2020-08-30T02:32:09.000000Z",
                "children": []
            },
            {
                "id": 18,
                "name": "Nước Đông Trùng Hạ Thảo VietFuji",
                "slug": "nuoc-dong-trung-ha-thao-vietfuji",
                "parent_id": 3,
                "description": "Nước Đông Trùng Hạ Thảo VietFuji",
                "order": 4,
                "deleted_at": null,
                "created_at": "2020-08-30T02:32:17.000000Z",
                "updated_at": "2020-08-30T02:32:24.000000Z",
                "children": []
            }
        ]
    },
    {
        "id": 4,
        "name": "YenViet Nest IQ",
        "slug": "yenviet-nest-iq",
        "parent_id": null,
        "description": "YenViet Nest IQ",
        "children": [
            {
                "id": 19,
                "name": "Cháo Yến Trẻ Em",
                "slug": "chao-yen-tre-em",
                "parent_id": 4,
                "description": "Cháo Yến Trẻ Em",
                "order": 1,
                "deleted_at": null,
                "created_at": "2020-08-30T02:32:42.000000Z",
                "updated_at": "2020-08-30T02:33:01.000000Z",
                "children": []
            },
            {
                "id": 20,
                "name": "Tổ Yến Chưng Sẵn Trẻ Em",
                "slug": "to-yen-chung-san-tre-em",
                "parent_id": 4,
                "description": "Tổ Yến Chưng Sẵn Trẻ Em",
                "order": 2,
                "deleted_at": null,
                "created_at": "2020-08-30T02:32:51.000000Z",
                "updated_at": "2020-08-30T02:33:01.000000Z",
                "children": []
            }
        ]
    },
    {
        "id": 5,
        "name": "Nước Yến",
        "slug": "nuoc-yen",
        "parent_id": null,
        "description": "Nước Yến",
        "children": []
    },
    {
        "id": 6,
        "name": "Cháo Yến",
        "slug": "chao-yen",
        "parent_id": null,
        "description": "Cháo Yến",
        "children": []
    },
    {
        "id": 7,
        "name": "Tổ Yến Nguyên Chất Chưng Sẵn",
        "slug": "to-yen-nguyen-chat-chung-san",
        "parent_id": null,
        "description": "Tổ Yến Nguyên Chất Chưng Sẵn",
        "children": []
    }
]
```
