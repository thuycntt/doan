#MENU API

#### Get Menu

**REQUEST**

Get param:
- menu_id: required
```
{domain}/api/menu/get-menu?menu_id=1
```
**RESPONSE**
```
{
    "menu": {
        "id": 1,
        "name": "Sản phẩm",
        "description": "Sản phẩm",
        "position": 1,
        "created_at": "2020-09-23T11:10:29.000000Z",
        "updated_at": "2020-09-23T11:10:29.000000Z"
    },
    "menuItem": [
        {
            "id": 1,
            "name": "Đông Trùng Hạ Thảo YenViet",
            "parent_id": null,
            "description": "Đông trùng hạ thảo",
            "url": "https://www.yenviet.com.vn/dong-trung-ha-thao-yenviet",
            "menu_id": 1,
            "children": [
                {
                    "id": 2,
                    "name": "Cháo Đông Trùng Hạ Thảo",
                    "description": "Cháo",
                    "menu_id": 1,
                    "parent_id": 1,
                    "url": "https://www.yenviet.com.vn/chao-dong-trung-ha-thao",
                    "order": 1,
                    "deleted_at": null,
                    "created_at": "2020-09-23T11:12:09.000000Z",
                    "updated_at": "2020-09-23T11:12:14.000000Z",
                    "children": []
                },
                {
                    "id": 5,
                    "name": "Nước Đông Trùng Hạ Thảo",
                    "description": "Nước đông trùng hạ thảo",
                    "menu_id": 1,
                    "parent_id": 1,
                    "url": "https://www.yenviet.com.vn/nuoc-dong-trung-ha-thao-vietfuji-2",
                    "order": 2,
                    "deleted_at": null,
                    "created_at": "2020-09-23T11:14:18.000000Z",
                    "updated_at": "2020-09-23T11:14:23.000000Z",
                    "children": []
                }
            ]
        },
        {
            "id": 3,
            "name": "YenViet Nest IQ",
            "parent_id": null,
            "description": "YenViet Nest IQ",
            "url": "https://www.yenviet.com.vn/yenviet-nest-iq",
            "menu_id": 1,
            "children": [
                {
                    "id": 4,
                    "name": "Cháo Yến Trẻ em",
                    "description": "Cháo yến",
                    "menu_id": 1,
                    "parent_id": 3,
                    "url": "https://www.yenviet.com.vn/chao-yen-tre-em-yenviet-nest-iq",
                    "order": 1,
                    "deleted_at": null,
                    "created_at": "2020-09-23T11:13:15.000000Z",
                    "updated_at": "2020-09-23T11:13:20.000000Z",
                    "children": []
                }
            ]
        }
    ]
}
```
