### API for Customer

#### update info Customer

**REQUEST**

Post form-data:
- customer_id: required
- dob: nullable
- phone: nullable
- about: nullable
- avatar: file, mimes:jpg,png,jpeg
- name: nullable


```
{domain}/api/customer/customer-update-info
```
**RESPONSE**
```
{
    "id": 2,
    "username": "thuyle",
    "dob": "1998-02-19",
    "phone": "0971381136",
    "address": "205 Phan Xích Long, Phường 2, Phú Nhuận, Thành phố Hồ Chí Minh",
    "about": "Chút chút",
    "avatar": "http://admin.chutdt.io/vendor/media/user-default.jpg",
    "name": "Lê Thủy",
    "email": "thuylekm16gts@gmail.com",
    "created_at": "2020-10-22T15:26:54.000000Z",
    "updated_at": "2020-10-27T15:58:27.000000Z"
}
```
#### update password customer

**REQUEST**

Post form-data:
- customer_id: required
- password: required
- old_password: required
- password_confirmation: required


```
{domain}/api/customer/update-password
```
**RESPONSE**
```
{
    "message": "Cập nhật thành công"
}
//OR
{
    "message": "Password không đúng"
}
```
