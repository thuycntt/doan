<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('uuid')->unique();
            $table->foreignId('customer_id')
                ->nullable()
                ->constrained()
                ->onDelete('cascade');
            $table->string('email');
            $table->string('full_name', 50);
            $table->string('phone', 20);
            $table->string('address');
            $table->unsignedBigInteger('ward_id');
            $table->text('note')->nullable();
            $table->tinyInteger('status');
            $table->string('ship_fee');
            $table->text('cancel_note')->nullable();
            $table->date('delivery_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
