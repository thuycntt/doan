<?php


use App\Models\Category;

class OrderSeeder extends \Illuminate\Database\Seeder
{
    public function run()
    {
        $allCustomers = \App\Models\Customer::all();
        $allProducts = \App\Models\Product::leftJoin('category_product', 'products.id', '=', 'category_product.product_id')
            ->leftJoin('categories', 'category_product.category_id', '=', 'categories.id')
            ->where('categories.type', Category::TYPE_FOOD)->get('products.*');

        $notes = collect([
            'giao trong giờ hành chính nhé shop',
            'giao trong hôm nay được không ạ',
            'cho hỏi mua nhiều có được giảm giá không ạ',
            'cảm ơn shop ạ'
        ]);
        $shipfee=collect([
            '20000','15000','12500','30000','10000','1000','5000','18000'
        ]);

        $orderStatus = collect([
            \App\Models\Order::STATUS_VERIFY,
            \App\Models\Order::STATUS_SHIPPING,
            \App\Models\Order::STATUS_COMPLETE,
            \App\Models\Order::STATUS_GETTING_PRODUCT,
            \App\Models\Order::STATUS_PENDING,
        ]);

        $wards = \App\Models\Ward::all();

        for ($i = 0; $i < 47; $i++) {
            $createdAt = \Carbon\Carbon::now()->subMinutes(rand(1, 24 * 60 * 30))->toDateTimeString();
            $customer = $allCustomers->random();
            $products = $allProducts->random(rand(1, 5));
            $fee = $shipfee->random();
            $transaction = [
                'uuid' => $this->generateTransactionId(),
                'type' => rand(1, 10) % 2 === 0 ? \App\Models\Transaction::COD : \App\Models\Transaction::MOMO,
                'customer_id' => $customer->id,
                'data' => null,
                'created_at' => $createdAt,
                'updated_at' => $createdAt,
            ];
            $date = \Carbon\Carbon::now()->addDays(rand(0,7))->toDateString();

            $order = [
                'uuid' => $this->generateOrderId(),
                'customer_id' => $customer->id,
                'email' => $customer->email,
                'full_name' => $customer->name,
                'phone' => $customer->phone,
                'address' => $customer->address,
                'note' => rand(1, 10) % 2 === 0 ? $notes->random() : null,
                'status' => $orderStatus->random(),
                'ship_fee'=>$fee,
                'created_at' => $createdAt,
                'updated_at' => $createdAt,
                'delivery_date' => $date,
                'ward_id' => $wards->random()->id
            ];

            if ($transaction['type'] == \App\Models\Transaction::MOMO) {
                $order['status'] = collect([
                    \App\Models\Order::STATUS_SHIPPING,
                    \App\Models\Order::STATUS_COMPLETE,
                    \App\Models\Order::STATUS_GETTING_PRODUCT,
                    \App\Models\Order::STATUS_PENDING,
                ])->random();
            }

            $order = \App\Models\Order::create($order);
            $amount = 0;
            $productSync = [];
            foreach ($products as $product) {
                $quantity = rand(1, 5);
                $amount = ($amount + ((int)$product->price-$product->price*($product->discount/100)) * $quantity);
                $productSync[$product['id']] = [
                    'quantity' => $quantity,
                    'product_price'=>$product->price,
                    'product_discount'=>$product->discount,
                ];
            }
            $order->products()->sync($productSync);


            $transaction['order_id'] = $order->id;
            $transaction['amount'] = $amount;

            if ($transaction['type'] === \App\Models\Transaction::COD) {
                $transaction['transaction_info'] = 'Thanh toán COD cho đơn hàng ' . $order['uuid'];
                $transaction['status'] = ($order->status === \App\Models\Order::STATUS_COMPLETE) ? \App\Models\Transaction::STATUS_SUCCESS : \App\Models\Transaction::STATUS_PENDING;
            } else {
                $transaction['transaction_info'] = 'Thanh toán Momo cho đơn hàng ' . $order['uuid'];
                $transaction['status'] = \App\Models\Transaction::STATUS_SUCCESS;
            }

            \App\Models\Transaction::create($transaction);
        }
    }

    /**
     * @return string
     */
    private function generateOrderId()
    {
        $id = \Str::random(16);
        while (\App\Models\Order::where('uuid', $id)->first() !== null) {
            $id = \Str::random(16);
        }
        return $id;
    }

    /**
     * @return string
     */
    private function generateTransactionId()
    {
        $id = \Str::random(16);
        while (\App\Models\Transaction::where('uuid', $id)->first() !== null) {
            $id = \Str::random(16);
        }
        return $id;
    }
}
