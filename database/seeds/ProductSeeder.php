<?php


class ProductSeeder extends \Illuminate\Database\Seeder
{
    public function run()
    {
        require __DIR__ . '/data/chutdt.php';

        $customers = \App\Models\Customer::all();
        $user = \App\Models\User::all();

        $comments = collect([
            'Mình mua tặng nên hơi gấp kh chụp ảnh. Bao bì hộp yến trông ổn, có điều kh có túi xách mà khá to nên ôm hơi bất tiện. Nước yến ổn, nắp hơi sắc bén. Hài lòng.',
            'Tuy giao hàng hơi lâu nhưng nhận được sp thì vô cùng hài lòng. yenvn đóng gói hàng cực kỳ cẩn thận. túi xách đi kèm yến xào cũng rất sang',
            'Nhìn hộp rất là sang luôn, đóng gói thì khỏi chê rồi nhưng mình thích là có miếng mút được lót ở dưới để giảm sốc, có 1 lớp quấn khi bóp nổ bụp bụp mình rất thích ??.',
            'Mình mua 2 hộp yến. yenvn giao hàng nhanh, đóng gói kỹ. Yến hộp mẫu mã đẹp, được đóng gói theo từng hộp & bên trong có kèm theo 2 túi xách có thể làm quà tặng nhìn rất sang.',
            'giao hàng nhanh, đóng gói cẩn thận, có kèm theo túi giấy. nhìn rất lịch sự.',
            'Vị ngọt vừa phải, độ sâm cũng vừa, không quá lạnh. Mẹ mình không thích sâm vì sợ lạnh cũng thấy hài lòng là mừng rồi. Điểm trừ duy nhất là ông shipper hẹn giờ giấc rồi biến mất. 2 tiếng sau mới có mặt. Mất thời gian của mình, lỡ cả việc.',
            'Mình mua 2 hộp tặng người thân. Cảm nhận của họ là uống ngon, thơm mùi sâm. Mình rất hài lòng nhưng sản phẩm k có túi đựng kèm theo, lúc tặng cho họ đựng bằng bịch xốp nhìn k lịch sự và chanh sả cho lắm, mong nhà cung cấp chú ý tới vấn đề này hơn',
            'Sản phẩm dễ uống, có mùi sâm, không quá ngọt. Tuy nhiên, nên thay đổi nắp sản phẩm vì nắp hiệ. tại khó mở và dễ gây đứt tay khách hàng. Mình đã thử mở nắp hộp và bị cứa 1 đường rất dài. Mọi người nên cẩn thận khi mở sản phẩm.',
            'yến ngon , thơm mùi sâm.bà mình ăn khen ngon, khỏe k còn mệt nữa. hàng đóng gói rất kỉ, nhân viên nhiệt tình, giao hàng nhanh. Mình mua 3 lần rồi lần nào cũng có túi xách rất đẹp. mua hàng yenvn là ăn tâm nhất.',
            'Giao hàng nhanh, tuy nhiên sản phẩm giao không kèm túi giấy đựng hộp yến, muốn dùng đi biếu tặng nhưng không có túi. Tem bên ngoài hộp bị bóc ra dán lại, cảm giác hàng không được an toàn...',
            'hàng nguyên seal. Tuy nhiên, giao giờ cơm trưa, không giao tận tay khách hàng mà gởi chỗ bảo vệ. Nếu người khác lấy hàng thì sao',
            'mình hay mua sản phẩm này Cho gia đình dùng nhiều rồi. mà đề nghị sốp bán hàng nhớ kèm túi xách để đựng nhiều lúc muốn biếu quà tặng cũng ko có mà xách',
            'Hộp khá đẹp, mình tính mua để tặng quà nhưng yenvn giao thiếu túi giấy. Không biết yenvn có hổ trợ giao thêm túi giấy cho mình ko',
            'Mình mua vào đợt khuyến mãi nên thấy giá khá là rẻ, sản phẩm có chất lượng tốt, mình thì lại thích nước yến đường phèn hơn vì có độ ngọt hơn chút xíu, và hạn sử dụng cũng còn rất là dài. bỏ vào tủ lạnh thì uống lạnh lạnh rất là ngon và bổ dưỡng.',
            'Giao hàng rất nhanh ( đặt giao 4h mà 3h đã giao đến ) sản phẩm như hình , không móp méo , có túi đựng riêng có thể mang biếu , tặng người thân bạn bè',
            'sản phẩm sử dụng tốt đóng gói cẩn thận, giao hàng nhanh. rất yên tâm khi mua hàng của yenvn',
            'Mình mua vào đợt được giảm giá nên thấy giá rẻ hơn ở bên ngoài tiệm. Yến này uống ngon, có thương hiệu lâu nên an tâm. Tks yenvn',
            'Đóng gói sản phẩm kỹ. Giao hàng đúng dự kiến. Chất lượng sản phẩm khá ok',
            'Sản phẩm đóng gói cẩn thận và khá bắt mắt, dùng làm quà biếu rất ok',
            'sp tốt, đạt chất lượng, có cả túi để đựng, rất hài lòng',
            'Mua set này đem đi biếu tặng là ok rồi, nhìn lịch sự.',
            'Sản phẩm chất lượng, yến đã được tính chế và chia sẵn theo khẩu phần, rất dễ sử dụng.',
            'Tôi cảm thấy cơ thể mình thật sung mãn và mạnh mẽ sau mỗi lần uống...! Từng múi cơ thớ thịt của tôi như được tiếp thêm nguồn năng lượng tràn trê, khiến tôi cảm thầy mình là nhà vô địch. Cảm ơn yenvn đã sản xuất ra loại nước uống này!',
            'sp chính hãng, đóng gói cẩn thận, shop gửi kèm cả túi để biếu/tặng, mua về 2 thằng cu tranh nhau ăn, sẽ ủng hộ shop những lần tiếp theo.'
        ]);

        foreach ($categories as $index => $category) {
            if (\App\Models\Category::find($category['id']) === null) {
                $category['order'] = $index + 1;
                unset($category['parent_id']);
                \App\Models\Category::create($category);
            }
        }

        foreach ($products as $product) {
            if (\App\Models\Product::find($product['id']) === null) {
                $product['discount'] = rand(0, 30);
                $product['thumbnail'] = str_replace('http://admin.chutdt.io/', '', $product['thumbnail']);
                $product['trending'] = rand(0,1);
                $product = \App\Models\Product::create($product);
                $customerTmp = $customers->random(5);

                $category = collect($category_product)->first(function ($item) use ($product) {
                    return $item['product_id'] == $product->id;
                });

                $category = \App\Models\Category::find($category['category_id']);

                if ($category->type != \App\Models\Category::TYPE_EQUIPMENT) {
                    foreach ($customerTmp as $customer) {
                        $createdAt = \Carbon\Carbon::now()->subMinutes(rand(200, 24 * 60 * 7))->toDateTimeString();
                        $rate = [
                            'product_id' => $product->id,
                            'customer_id' => $customer->id,
                            'rate' => rand(3, 5),
                            'comment' => $comments->random(),
                            'created_at' => $createdAt,
                            'updated_at' => $createdAt,
                            'status' => 10,
                            'user_id' => $user->random()->id
                        ];
                        \App\Models\ProductRate::create($rate);
                    }
                }

            }
        }

        foreach ($product_images as $image) {
            if (\App\Models\ProductImage::find($image['id']) === null) {
                $image['image'] = str_replace('http://admin.chutdt.io/', '', $image['image']);
                \App\Models\ProductImage::create($image);
            }
        }

        foreach ($category_product as $item) {
            unset($item['created_at']);
            unset($item['updated_at']);
            if (\App\Models\Category::find($item['category_id'])) {
                DB::table('category_product')->insert($item);
            }
        }
    }
}
