<?php


class UserTableSeeder extends \Illuminate\Database\Seeder
{

    public function run()
    {
        \App\Models\User::create([
            'username' => 'admin',
            'password' => Hash::make('password'),
            'email' => 'giangnguyen.neko.130@gmail.com',
            'name' => 'Admin'
        ]);

        \App\Models\User::create([
            'username' => 'thuyle',
            'password' => Hash::make('password'),
            'email' => 'thuylekm16gts@gmail.com',
            'name' => 'Lê Thủy'
        ]);

        \App\Models\User::create([
            'username' => 'admin2',
            'password' => Hash::make('password'),
            'email' => 'admin2@gmail.com',
            'name' => 'Admin 2'
        ]);
    }
}
