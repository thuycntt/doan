<?php


class CustomerSeeder extends \Illuminate\Database\Seeder
{
    public function run()
    {
        $customers = [
            [
                'username' => 'giangnguyen',
                'phone' => '0971381894',
                'address' => 'Phường Tân Chánh Hiệp, Quận 12, Hồ Chí Minh',
                'name' => 'Giang Nguyễn',
                'email' => 'mrcatbro97@gmail.com',
                'password' => Hash::make('password'),
                'dob' => '1997-04-15',
                'avatar' => 'vendor/media/user-default.jpg',
                'about' => 'Coding is my life'
            ],
            [
                'username' => 'thuyle',
                'phone' => '0971381136',
                'address' => '205 Phan Xích Long, Phường 2, Phú Nhuận, Thành phố Hồ Chí Minh',
                'name' => 'Lê Thuỷ',
                'email' => 'thuylekm16gts@gmail.com',
                'password' => Hash::make('password'),
                'dob' => '1998-02-19',
                'avatar' => 'vendor/media/user-default.jpg',
                'about' => 'Chút chút'
            ],
            [
                'username' => 'hoanguyen',
                'phone' => '0971381146',
                'address' => '833 Lê Hồng Phong, Phường 12, Quận 10, Thành phố Hồ Chí Minh',
                'name' => 'Hòa Nguyễn',
                'email' => 'hoanguyenmbvfjkg99@gmail.com',
                'password' => Hash::make('password'),
                'dob' => '1995-09-11',
                'avatar' => 'vendor/media/user-default.jpg',
                'about' => 'Hòa Nguyễn'
            ],
            [
                'username' => 'xuanbao',
                'phone' => '0971381146',
                'address' => '833 Lê Hồng Phong, Phường 12, Quận 10, Thành phố Hồ Chí Minh',
                'name' => 'Xuân Bảo',
                'email' => 'luongxuanbao@gmail.com',
                'password' => Hash::make('password'),
                'dob' => '1998-11-11',
                'avatar' => 'vendor/media/user-default.jpg',
                'about' => 'Xuân Bảo'
            ],
            [
                'username' => 'vinhnguyen',
                'phone' => '0971381591',
                'address' => '22 Nguyễn Đình Khơi, Phường 4, Quận Tân Bình, Thành phố Hồ Chí Minh',
                'name' => 'Vinh Nguyễn',
                'email' => 'vinhnguyen2982348ffxcss@gmail.com',
                'password' => Hash::make('password'),
                'dob' => '1991-10-19',
                'avatar' => 'vendor/media/user-default.jpg',
                'about' => 'Vinh Nguyễn'
            ],
            [
                'username' => 'phamtien',
                'phone' => '0979811591',
                'address' => '61/69 Đường số 19, Phường 8, Quận Gò Vấp, Thành phố Hồ Chí Minh',
                'name' => 'Phạm Tiến',
                'email' => 'phamtien2982348ffxcss@gmail.com',
                'password' => Hash::make('password'),
                'dob' => '1989-04-21',
                'avatar' => 'vendor/media/user-default.jpg',
                'about' => 'Phạm Tiến'
            ],
            [
                'username' => 'hoangduy',
                'phone' => '0997811591',
                'address' => 'Số 48 Đường Tân Lập 2, Khu phố 3, Phường Hiệp Phú, Quận 9, Thành phố Hồ Chí Minh',
                'name' => 'Hoàng Văn Duy',
                'email' => 'hoanhduy2982348ffxcss@gmail.com',
                'password' => Hash::make('password'),
                'dob' => '1997-11-11',
                'avatar' => 'vendor/media/user-default.jpg',
                'about' => 'Hoàng Văn Duy'
            ],
            [
                'username' => 'quocdai',
                'phone' => '0997811591',
                'address' => '61/69 Đường số 19, Phường 8, Quận Gò Vấp, Thành phố Hồ Chí Minh',
                'name' => 'Nguyễn Quốc Đại',
                'email' => 'quocdai2982348ffxcss@gmail.com',
                'password' => Hash::make('password'),
                'dob' => '1997-12-12',
                'avatar' => 'vendor/media/user-default.jpg',
                'about' => 'Nguyễn Quốc Đại'
            ]
        ];

        foreach ($customers as $customer) {
            $createdAt = \Carbon\Carbon::now()->subMinutes(rand(1, 24 * 60 * 7))->toDateTimeString();
            $customer['created_at'] = $createdAt;
            $customer['updated_at'] = $createdAt;
            \App\Models\Customer::create($customer);
        }
    }
}
