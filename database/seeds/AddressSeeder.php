<?php


class AddressSeeder extends \Illuminate\Database\Seeder
{
    public function run()
    {
        require __DIR__ . '/data/address.php';

        $shipfee = collect([
            '20000', '15000', '12500', '30000', '10000', '1000', '5000', '18000'
        ]);

        foreach ($provinces as $province) {
            unset($province['ship_fee']);
            \App\Models\Province::create($province);
        }

        foreach ($districts as $district) {
            \App\Models\District::insert($district);
        }

        foreach ($ward1 as $ward) {
            $ward['ship_fee'] = $shipfee->random();
            $ward['delivery_date'] = rand(0, 7);
            \App\Models\Ward::create($ward);
        }

        foreach ($ward2 as $ward) {
            $ward['ship_fee'] = $shipfee->random();
            $ward['delivery_date'] = rand(0, 7);
            \App\Models\Ward::create($ward);
        }

    }
}
