<?php


class SlideSeeder extends \Illuminate\Database\Seeder
{
    public function run()
    {
        require __DIR__ . '/data/chutdt.php';

        foreach ($slides as $slide) {
            if (\App\Models\Slide::find($slide['id']) == null) {
                \App\Models\Slide::create($slide);
            }
        }

        foreach ($slide_items as $item) {
            if (\App\Models\SlideItem::find($item['id']) === null) {
                \App\Models\SlideItem::create($item);
            }
        }
    }
}
