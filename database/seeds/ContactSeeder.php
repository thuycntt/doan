<?php


class ContactSeeder extends \Illuminate\Database\Seeder
{
    public function run()
    {
        $contact = [
            [
                'full_name' => 'Đào Thương',
                'phone' => '0981486015',
                'email' => 'daothuong.11@gmail.com',
                'content' => 'Làm sao để đăng ký làm thành viên ạ?'
            ],
            [
                'full_name' => 'Mai Châu',
                'phone' => null,
                'email' => 'chaumai123@gmail.com',
                'content' => 'Làm sao để tạo nhật ký vậy ad ơi'
            ],
            [
                'full_name' => 'Duy Nguyễn',
                'phone' => null,
                'email' => 'since12147@gmail.com',
                'content' => 'Sao tôi không đăng ký được tài khoản trên web này'
            ],
            [
                'full_name' => 'Giang Nguyễn',
                'phone' => '0916582912',
                'email' => 'giangnguyen.neko.130@gmail.com',
                'content' => 'Ơn giời admin đây rồi !!'
            ]
        ];

        foreach ($contact as $item){
            $createdAt = \Carbon\Carbon::now()->subMinutes(rand(200, 24 * 60 * 7))->toDateTimeString();
            $item['created_at'] = $createdAt;
            \App\Models\Contact::create($item);
        }
    }
}
