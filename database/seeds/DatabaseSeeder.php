<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AddressSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(CustomerSeeder::class);
        $this->call(ArticleSeeder::class);
        $this->call(ProductSeeder::class);
        $this->call(SlideSeeder::class);
        $this->call(MenuTableSeeder::class);
        $this->call(OrderSeeder::class);
        $this->call(QuestionSeeder::class);
        $this->call(CustomerDiarySeeder::class);
        $this->call(ContactSeeder::class);
    }
}
