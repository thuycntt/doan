<?php


class QuestionSeeder extends \Illuminate\Database\Seeder
{
    public function run()
    {
        $customers = \App\Models\Customer::all();

        $users = \App\Models\User::all();

        $questions = [
            [
                'q' => 'Chim yến có thể nuôi được ở khu vực tỉnh thành nào trong nước?',
                'an' => [
                    'u' => 'Ở nước ta hiện nay tại bất cứ tỉnh thành nào cũng đều có thể nuôi được chim yến vì chim yến sinh sống và phân bố ở khu vực Đông Nam Á. Tuy nhiên ở mỗi tỉnh, thành phố thì sẽ có một số khu vực có thể nuôi được chim yến tốt thôi, chứ không phải là tất cả.'
                ]
            ],
            [
                'q' => 'Làm thế nào để có thể dẫn dụ được chim yến vào nhà yến mình dựng sẵn để làm tổ?',
                'an' => [
                    'u' => 'Để có thể dẫn dụ được chim yến tìm tới và vào trong nhà yến mà mình dựng sẵn để làm tổ cần phải có âm thanh dụ yến hay. Âm thanh dụ yến càng hay càng thu hút được nhiều chim yến tìm tới và định cư lâu dài trong nhà yến.'
                ]
            ],
            [
                'q' => 'Chim yến một khi đã làm tổ trong nhà yến dựng sẵn liệu có rời đi hay không?',
                'an' => [
                    'u' => 'Câu trả lời là có nếu như nhà yến không đảm bảo đầy đủ các yếu tố để chim yến cảm thấy an toàn ví dụ không có mùi bầy đàn, điều kiện độ ẩm, nhiệt độ không đảm bảo hay âm thanh trong nhà yến quá lớn…'
                ]
            ],
            [
                'q' => 'Có thể nuôi yến từ chim con?',
                'an' => [
                    'u' => 'Điều này hoàn toàn sai lầm. Vì không chỉ ở Việt Nam mà ở các nước trên thế giới chưa hề áp dụng công nghệ nuôi yến từ chim con và thành công. Phương pháp nuôi yến phổ biến hiện nay đó là dẫn dụ chim yến từ tự nhiên vào trong nhà.'
                ]
            ],
            [
                'q' => 'Chim yến thường làm tổ mấy lần một năm?',
                'an' => [
                    'u' => 'Đối với chim yến đảo thì 2 lần/ một năm, tuy nhiên khả năng dẫn dụ yến đảo về nhà mang đến cơ hội thành công rất thấp.
                    Đối với yến tổ trắng thì 3- 4 lần/ một năm tùy thuộc vào nguồn thức ăn.'
                ]
            ],
            [
                'q' => 'Thời điểm nào nên thu hoạch chim yến?',
                'an' => [
                    'u' => 'ấn đề này tùy thuộc vào mùa sinh sản và số lượng chim yến đang được nuôi trong nhà yến của bạn.

Nếu số lượng chim yến ít, chưa đến 300 con thì không nên thu hoạch mà nên nuôi dưỡng thêm chim yến.

Nếu đúng vào mùa sinh sản khoan hãy thu hoạch, hãy chờ cho đến khi chim yến non có thể bay. Khi thu hoạch hãy căn chừng và thu hoạch ngay khi chim non vừa bay ra khỏi tổ.'
                ]
            ],
            [
                'q' => 'Nên làm gì để đảm bảo an ninh trong nhà yến?',
                'an' => [
                    'u' => 'Nuôi yến với mục tiêu là thu được lợi nhuận, vì vậy cần phải hạn chế được tối đa rủi ro nhất là việc mất cắp chim yến hay bị cú, thằn lằn..ăn chim yến. Để bảo vệ chim yến cần lắp đặt hệ thống camera giám sát, lắp cửa sắt kiên cố và cần phải có người bảo vệ nhà yến.'
                ]
            ],
            [
                'q' => 'Các tỉnh nào có thể nuôi Yến được?',
                'an' => [
                    'u' => 'Hải Phòng, Thanh Hóa, Nghệ An, Hà Tĩnh, Quảng Bình, Đà Nẵng, Quảng Ngãi, Phú Yên, Bình Định, Khánh Hòa, Bình Thuận, Ninh Thuận, Bình Phước, Daklak, Tây Ninh, Lâm Đồng, Bình Dương, Đồng Nai, Bà Rịa – Vũng Tàu, TP HCM, Long An, Bến Tre, Tiền Giang, Bạc Liêu, Cà Mau, An Giang, Trà Vinh và Kiên Giang.
Tuy nhiên không phải mọi địa điểm tại các Tỉnh nêu trên đều có thể nuôi chim Yến. Thường mỗi tỉnh có một vài điểm trong bán kính 5km có thể nuôi tốt. Qua quá trình tư vấn nhiều năm, hiện chúng tôi đã nắm được hầu hết các địa điểm tiến hành xây dựng được nhà nuôi Yến tới đơn vị cấp xã. Xin liên hệ trực tiếp để được tư vấn cho trường hợp của quý khách.

Thêm một điểm cần lưu ý là các tỉnh từ bắc đèo hải vân chim yến thường chết hàng loạt do ko chịu được thời tiết lạnh kéo dài khi nhiệt độ xuống dưới 16 độ C. Hiện Hoàng yến eka đã  nghiên cứu và thử nghiệm rất thành công quy trình xây dựng nhà yến tại miền Bắc.'
                ]
            ],
            [
                'q' => 'Chim yến có thể ấp trứng công nghiệp? gây giống và mua giống về nuôi được không?',
                'an' => [
                    'u' => 'Đến thời điểm hiện tại vẫn chưa gặt hái thành công nào cho việc can thiệp của con người vào việc ấp nở, gây giống loài yến. phương pháp duy nhất để chim yến ở lại nhà vẫn là dẫn dụ tự nhiên.'
                ]
            ],
            [
                'q' => 'Chim yến ăn gì? Nguồn thức ăn ở đâu?',
                'an' => [
                    'u' => '- Nguồn thức ăn của chim yến chủ yếu là côn trùng có kích thước nhỏ như : Bộ cánh màng (kiến), bộ cánh mối (mối), bộ cánh ruồi, ruồi dấm , rầy nâu, rầy xanh, ong nhỏ, phù du, ruồi, muỗi, các bọ cánh nhỏ…Yến còn giúp bà con tiêu diệt thiên địch sâu bệnh thông qua các nguồn thức ăn bảo vệ mùa màng giúp cân bằng sinh thái.

- Vào thời điểm mùa sinh sản, ấp nở của chim yến. Người nuôi yến có thể tạo thêm thức ăn trong nhà yến đảm bảo cung cấp đủ năng lượng cho chim yến nuôi con và phát triển bầy đàn.'
                ]
            ],
            [
                'q' => 'Vùng của tôi rất nhiều chim bay lượn, không biết là yến hay Én ? , yến cỏ ?',
                'an' => [
                    'u' => 'Đặc điểm của 2 loài yến và én này tương đồng nhau, không phải kẻ thù của nhau, hay kiếm ăn chung trên một cánh đồng hay một khoảng không gian nhất định.

Chim yến : Đuôi ngắn không chẻ, vùng lông dưới bụng màu xám, không đậu, quỹ đạo bay không thẳng ( vòng vo) , không đậu chỉ bám vào thanh gỗ, đá khi quay về chỗ trú ngụ và nằm trong tổ khi đang ấp trứng.
Chim Én : Đuôi chẻ hình V, vùng lông bụng màu trắng, quỹ đạo bay thẳng, có khả năng đậu trên cây hay đậu rất nhiều trên dây điện
Yến cỏ : Loài này xuất hiện nhiều ở vùng lạnh : Đà lạt , gia lai…quỹ đạo bay giống chim yến. Đặc điểm có bộ long đen tuyền,mình thon, cánh dài, làm tổ bằng rác.'
                ]
            ],
        ];

        $questions = collect($questions);

        for ($i = 0; $i < 20; $i++) {
            $customer = $customers->random();
            $user = $users->random();
            $questionTmp = $questions->random();
            $createdAt = \Carbon\Carbon::now()->subMinutes(rand(200, 24 * 60 * 7))->toDateTimeString();
            $question = [
                'question' => $questionTmp['q'],
                'customer_id' => $customer->id,
                'status' => rand(0, 1),
                'created_at' => $createdAt,
                'updated_at' => $createdAt,
                'user_id' => $user->id
            ];

            $question = \App\Models\Question::create($question);

            if (!empty($questionTmp['an'])) {
                $replyCreated = \Carbon\Carbon::parse($createdAt);
                foreach ($questionTmp['an'] as $key => $item) {
                    $createdAt = $replyCreated->addMinutes(rand(1, 60))->toDateString();
                    $reply = [
                        'reply' => $item,
                        'question_id' => $question->id,
                        'created_at' => $createdAt,
                        'updated_at' => $createdAt,
                    ];
                    if ($key == 'u') {
                        $reply['user_id'] = $user->id;
                    } else {
                        $reply['customer_id'] = $customer->id;
                    }
                    \App\Models\Reply::create($reply);
                    $replyCreated = \Carbon\Carbon::parse($createdAt);
                }
            }
        }
    }
}
