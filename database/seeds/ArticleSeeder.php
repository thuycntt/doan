<?php


class ArticleSeeder extends \Illuminate\Database\Seeder
{
    public function run()
    {
        require __DIR__ . '/data/chutdt.php';

        foreach ($tags as $tag) {
            if (\App\Models\Tag::find($tag['id']) === null){
                $tag['name'] = mb_strtolower($tag['name']);
                \App\Models\Tag::create($tag);
            }

        }

        foreach ($articles as $article) {
            if (\App\Models\Article::find($article['id']) === null)
                \App\Models\Article::create($article);
        }

        foreach ($article_tag as $pivot) {
            if (DB::table('article_tag')->where(function ($query) use ($pivot) {
                    $query->where('article_id', $pivot['article_id'])
                        ->where('tag_id', $pivot['tag_id']);
                })->first() === null)
                DB::table('article_tag')->insert($pivot);
        }

    }
}
