<?php


class MenuTableSeeder extends \Illuminate\Database\Seeder
{

    public function run()
    {
        require __DIR__ . '/data/chutdt.php';

        foreach ($menus as $menu) {
            if (\App\Models\Menu::find($menu['id']) == null) {
                \App\Models\Menu::create($menu);
            }
        }

        foreach ($menu_items as $item) {
            if (\App\Models\MenuItem::find($item['id']) === null) {
                \App\Models\MenuItem::create($item);
            }
        }
    }
}
