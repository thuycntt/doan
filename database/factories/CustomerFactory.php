<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(\App\Models\Customer::class, function (Faker $faker) {
    return [
        'username' => $faker->unique()->userName,
        'phone' => $faker->phoneNumber,
        'address' => $faker->address,
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => Hash::make('password'),
        'dob' => $faker->dateTimeBetween('1990-01-01', '1998-01-01'),
        'avatar' => 'vendor/media/user-default.jpg',
        'about' => $faker->text(300)
    ];
});
