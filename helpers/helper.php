<?php

if (!function_exists('set_admin_title')) {

    /**
     * @param $title
     */
    function set_admin_title($title)
    {
        \App\Facades\AdminPageTitleFacade::setPageTitle($title);
    }
}

if (!function_exists('get_admin_title')) {

    /**
     * @param $full
     * @return mixed
     */
    function get_admin_title($full = true)
    {
        return \App\Facades\AdminPageTitleFacade::getPageTitle($full);
    }
}

if (!function_exists('user_full_name')) {

    /**
     * @return mixed
     */
    function user_full_name()
    {
        return Auth::guard('web')->user()->name;
    }
}
