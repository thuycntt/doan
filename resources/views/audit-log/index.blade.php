@extends('layouts.master')

{{--@section('contact','menu-item-active')--}}

@section('content')
    <div class="card">
        <div class="card-body">
            <table class="table table-bordered" id="table-audit"></table>
        </div>
    </div>
@stop

@push('page-js')
    <script>
        $(document).ready(function () {
            $('#table-audit').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    method: 'GET',
                    url: '',
                    headers: {'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')}
                },
                drawCallback: function (settings) {
                    const scrollBody = $(settings.nScrollBody);
                    scrollBody.attr('style', 'min-height: 180px;' + scrollBody.attr('style'));
                },
                scrollX: true,
                columns: [
                    {
                        title: 'Nội dung',
                        data: 'info',
                        name: 'info',
                        orderable: true,
                        searchable: true
                    },
                    {
                        title: 'User agent',
                        data: 'user_agent',
                        name: 'user_agent',
                        orderable: true,
                        searchable: true
                    }
                ],
            });
        });
    </script>
@endpush
