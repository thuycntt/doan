@extends('layouts.master')

@section('breadcrumb')

@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <form action="{{route('user.postUpdateInfo')}}" method="post">
                @csrf
                <div class="form-group">
                    <label for="name">Tên</label>
                    <input type="text" class="form-control" placeholder="Tên" name="name" id="name" value="{{ old('name', auth()->user()->name) }}">
                    @error('name')
                    <div class="fv-plugins-message-container"><div class="fv-help-block">{{ $message }}</div></div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="phone">Số điện thoại</label>
                    <input type="text" class="form-control" placeholder="Số điện thoại" name="phone" id="phone" value="{{ old('phone',auth()->user()->phone) }}">
                    @error('phone')
                    <div class="fv-plugins-message-container"><div class="fv-help-block">{{ $message }}</div></div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="dob">Ngày sinh</label>
                    <input type="text" class="form-control" placeholder="Ngày sinh" name="dob" id="dob" value="{{ old('dob',auth()->user()->dob) }}">
                    @error('dob')
                    <div class="fv-plugins-message-container"><div class="fv-help-block">{{ $message }}</div></div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="avatar">Ảnh đại diện</label>
                    <input type="file" class="form-control" name="avatar" id="avatar" value="">
                    @error('avatar')
                    <div class="fv-plugins-message-container"><div class="fv-help-block">{{ $message }}</div></div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="about">Giới thiệu</label>
                    <textarea class="form-control" placeholder="Giới thiệu" name="about" id="about">{{ old('about',auth()->user()->about) }}</textarea>
                    @error('about')
                    <div class="fv-plugins-message-container"><div class="fv-help-block">{{ $message }}</div></div>
                    @enderror
                </div>
                <div class="card-footer d-flex justify-content-end">
                    <button type="submit" class="btn btn-primary mr-2"><i class="fa fa-save"></i>Lưu</button>
                </div>
            </form>
        </div>
    </div>
@stop

