@extends('layouts.master')


@section('breadcrumb')

@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <form action="{{ route('user.postUpdatePassword') }}" method="post">
                @csrf
                <div class="form-group">
                    <label for="old_password">Mật khẩu cũ</label>
                    <input type="password" class="form-control" placeholder="Mật khẩu cũ" name="old_password" id="old_password" value="">
                    @error('old_password')
                    <div class="fv-plugins-message-container"><div class="fv-help-block">{{ $message }}</div></div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="password">Mật khẩu mới</label>
                    <input type="password" class="form-control" placeholder="Mật khẩu mới" name="password" id="password" value="">
                    @error('password')
                    <div class="fv-plugins-message-container"><div class="fv-help-block">{{ $message }}</div></div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="password_confirmation">Nhập lại mật khẩu mới</label>
                    <input type="password" class="form-control" placeholder="Nhập lại mật khẩu mới" name="password_confirmation" id="password_confirmation" value="">
                    @error('password_confirmation')
                    <div class="fv-plugins-message-container"><div class="fv-help-block">{{ $message }}</div></div>
                    @enderror
                </div>
                <div class="card-footer d-flex justify-content-end">
                    <button type="submit" class="btn btn-primary mr-2"><i class="fa fa-save"></i>Lưu</button>
                </div>
            </form>
        </div>
    </div>
@stop
