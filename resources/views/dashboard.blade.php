@extends('layouts.master')

@section('dashboard','menu-item-active')

@section('content')
 <form action="{{route('dashboard')}}" method="get">
     <div class="row">
         <div class="col-3">
             <div class="input-group date mb-2">
                 <input type="text" class="form-control"  name="fromDate" placeholder="Từ ngày" id="from_date" value="{{$fromDate}}">
                 <div class="input-group-append">
            <span class="input-group-text">
                <i class="la la-check"></i>
            </span>
                 </div>
             </div>
         </div>
         <div class="col-3">
             <div class="input-group date mb-2">
                 <input type="text" class="form-control" name="toDate" placeholder="Đến ngày" id="to_date"  value="{{$toDate}}">
                 <div class="input-group-append">
            <span class="input-group-text">
                <i class="la la-check"></i>
            </span>
                 </div>
             </div>
         </div>
         <div class="col-1">
             <button class="btn btn-info" type="submit">Xem</button>
         </div>
     </div>
 </form>
    <div class="card" id="main" data-url="{{ route('dashboard.get_data') }}">
    </div>
    <div class="row mt-5">
        <div class="col-xl-3">
            <div class="card card-custom bgi-no-repeat card-stretch gutter-b"
                 style="background-position: right top; background-size: 30% auto; background-image: url(https://preview.keenthemes.com/metronic/theme/html/demo1/dist/assets/media/svg/shapes/abstract-1.svg)">
                <div class="card-body">
                        <span class="svg-icon svg-icon-primary svg-icon-2x">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                 width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <rect x="0" y="0" width="24" height="24"/>
                                    <path
                                        d="M5.84026576,8 L18.1597342,8 C19.1999115,8 20.0664437,8.79732479 20.1528258,9.83390904 L20.8194924,17.833909 C20.9112219,18.9346631 20.0932459,19.901362 18.9924919,19.9930915 C18.9372479,19.9976952 18.8818364,20 18.8264009,20 L5.1735991,20 C4.0690296,20 3.1735991,19.1045695 3.1735991,18 C3.1735991,17.9445645 3.17590391,17.889153 3.18050758,17.833909 L3.84717425,9.83390904 C3.93355627,8.79732479 4.80008849,8 5.84026576,8 Z M10.5,10 C10.2238576,10 10,10.2238576 10,10.5 L10,11.5 C10,11.7761424 10.2238576,12 10.5,12 L13.5,12 C13.7761424,12 14,11.7761424 14,11.5 L14,10.5 C14,10.2238576 13.7761424,10 13.5,10 L10.5,10 Z"
                                        fill="#000000"/>
                                    <path
                                        d="M10,8 L8,8 L8,7 C8,5.34314575 9.34314575,4 11,4 L13,4 C14.6568542,4 16,5.34314575 16,7 L16,8 L14,8 L14,7 C14,6.44771525 13.5522847,6 13,6 L11,6 C10.4477153,6 10,6.44771525 10,7 L10,8 Z"
                                        fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                                </g>
                            </svg>
                        </span>
                    <span
                        class="card-title font-weight-bolder text-dark-75 font-size-h2 mb-0 mt-6 d-block">{{ $totalProduct }}</span>
                    <span class="font-weight-bold text-muted font-size-h5">Tổng số sản phẩm</span>
                </div>
            </div>
        </div>
        <div class="col-xl-3">
            <div class="card card-custom bg-info card-stretch gutter-b">
                <div class="card-body">
                            <span class="svg-icon svg-icon-white svg-icon-2x">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                     width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect x="0" y="0" width="24" height="24"/>
                                        <path
                                            d="M12,4.56204994 L7.76822128,9.6401844 C7.4146572,10.0644613 6.7840925,10.1217854 6.3598156,9.76822128 C5.9355387,9.4146572 5.87821464,8.7840925 6.23177872,8.3598156 L11.2317787,2.3598156 C11.6315738,1.88006147 12.3684262,1.88006147 12.7682213,2.3598156 L17.7682213,8.3598156 C18.1217854,8.7840925 18.0644613,9.4146572 17.6401844,9.76822128 C17.2159075,10.1217854 16.5853428,10.0644613 16.2317787,9.6401844 L12,4.56204994 Z"
                                            fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                                        <path
                                            d="M3.5,9 L20.5,9 C21.0522847,9 21.5,9.44771525 21.5,10 C21.5,10.132026 21.4738562,10.2627452 21.4230769,10.3846154 L17.7692308,19.1538462 C17.3034221,20.271787 16.2111026,21 15,21 L9,21 C7.78889745,21 6.6965779,20.271787 6.23076923,19.1538462 L2.57692308,10.3846154 C2.36450587,9.87481408 2.60558331,9.28934029 3.11538462,9.07692308 C3.23725479,9.02614384 3.36797398,9 3.5,9 Z M12,17 C13.1045695,17 14,16.1045695 14,15 C14,13.8954305 13.1045695,13 12,13 C10.8954305,13 10,13.8954305 10,15 C10,16.1045695 10.8954305,17 12,17 Z"
                                            fill="#000000"/>
                                    </g>
                                </svg>
                            </span>
                    <span
                        class="card-title font-weight-bolder text-white font-size-h2 mb-0 mt-6 d-block">{{ $totalOrder }}</span>
                    <span class="font-weight-bold text-white font-size-h5">Tổng số đơn hàng</span>
                </div>
            </div>
        </div>
        <div class="col-xl-3">
            <div class="card card-custom bg-danger card-stretch gutter-b">
                <div class="card-body">
                            <span class="svg-icon svg-icon-white svg-icon-2x">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                     width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <polygon points="0 0 24 0 24 24 0 24"/>
                                        <path
                                            d="M5.85714286,2 L13.7364114,2 C14.0910962,2 14.4343066,2.12568431 14.7051108,2.35473959 L19.4686994,6.3839416 C19.8056532,6.66894833 20,7.08787823 20,7.52920201 L20,20.0833333 C20,21.8738751 19.9795521,22 18.1428571,22 L5.85714286,22 C4.02044787,22 4,21.8738751 4,20.0833333 L4,3.91666667 C4,2.12612489 4.02044787,2 5.85714286,2 Z"
                                            fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                                        <rect fill="#000000" opacity="0.3"
                                              transform="translate(8.984240, 12.127098) rotate(-45.000000) translate(-8.984240, -12.127098) "
                                              x="7.41281179" y="10.5556689" width="3.14285714" height="3.14285714"
                                              rx="0.75"/>
                                        <rect fill="#000000" opacity="0.3"
                                              transform="translate(15.269955, 12.127098) rotate(-45.000000) translate(-15.269955, -12.127098) "
                                              x="13.6985261" y="10.5556689" width="3.14285714" height="3.14285714"
                                              rx="0.75"/>
                                        <rect fill="#000000"
                                              transform="translate(12.127098, 15.269955) rotate(-45.000000) translate(-12.127098, -15.269955) "
                                              x="10.5556689" y="13.6985261" width="3.14285714" height="3.14285714"
                                              rx="0.75"/>
                                        <rect fill="#000000"
                                              transform="translate(12.127098, 8.984240) rotate(-45.000000) translate(-12.127098, -8.984240) "
                                              x="10.5556689" y="7.41281179" width="3.14285714" height="3.14285714"
                                              rx="0.75"/>
                                    </g>
                                </svg>
                            </span>
                    <span
                        class="card-title font-weight-bolder text-white font-size-h2 mb-0 mt-6 d-block">{{ $totalArticle }}</span>
                    <span class="font-weight-bold text-white font-size-h5">Tổng số bài viết</span>
                </div>
            </div>
        </div>
        <div class="col-xl-3">
            <div class="card card-custom bg-dark card-stretch gutter-b">
                <div class="card-body">
                            <span class="svg-icon svg-icon-white svg-icon-2x">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                     width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <polygon points="0 0 24 0 24 24 0 24"/>
                                    <path
                                        d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z"
                                        fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                                    <path
                                        d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z"
                                        fill="#000000" fill-rule="nonzero"/>
                                </g>
                            </svg>
                            </span>
                    <span
                        class="card-title font-weight-bolder text-white font-size-h2 mb-0 mt-6 text-hover-primary d-block">{{ $totalCustomer }}</span>
                    <span class="font-weight-bold text-white font-size-h5">Tổng số khách hàng</span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-4">
            <div class="card card-custom bg-light-danger card-stretch gutter-b">
                <div class="card-body">
                    <span class="svg-icon svg-icon-2x svg-icon-success">
                        <i class="fas fa-dollar-sign font-size-h1"></i>
                    </span>
                    <span class="card-title font-weight-bolder text-dark-75 font-size-h2 mb-0 mt-6 d-block profit">{{ number_format($revenueOfMonth) }} VNĐ</span>
                    <span class="font-weight-bold text-muted font-size-h4">Doanh thu trong tháng {{date('m')}}</span>
                </div>
            </div>
        </div>
        <div class="col-xl-4">
            <div class="card card-custom bg-light-primary card-stretch gutter-b">
                <div class="card-body">
                    <span class="svg-icon svg-icon-2x svg-icon-success">
                        <i class="fas fa-dollar-sign font-size-h1"></i>
                    </span>
                    <span class="card-title font-weight-bolder text-dark-75 font-size-h2 mb-0 mt-6 d-block profit">{{ number_format($revenueOf7Days) }} VNĐ</span>
                    <span class="font-weight-bold text-muted font-size-h4">Doanh thu trong 7 ngày gần đây</span>
                </div>
            </div>
        </div>
        <div class="col-xl-4">
            <div class="card card-custom bg-light-primary card-stretch gutter-b">
                <div class="card-body">
                    <span class="svg-icon svg-icon-2x svg-icon-success">
                        <i class="fas fa-dollar-sign font-size-h1"></i>
                    </span>
                    <span class="card-title font-weight-bolder text-dark-75 font-size-h2 mb-0 mt-6 d-block profit">{{ number_format($revenueOfToDay) }} VNĐ</span>
                    <span class="font-weight-bold text-muted font-size-h4">Doanh thu trong hôm nay</span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-6">
            <div class="card card-custom card-stretch gutter-b">
                <div class="card-header h-auto border-0">
                    <div class="card-title py-5">
                        <h3 class="card-label">
                            <span class="d-block text-dark font-weight-bolder">Đơn hàng từ {{ $fromDate }} đến {{ $toDate }}</span>
                        </h3>
                    </div>
                </div>
                <div class="card-body">
                    <div id="chart_order_7_day"></div>
                </div>
            </div>
        </div>
        <div class="col-xl-6">
            <div class="card card-custom card-stretch gutter-b">
                <div class="card-header h-auto border-0">
                    <div class="card-title py-5">
                        <h3 class="card-label">
                            <span class="d-block text-dark font-weight-bolder">Khách hàng đăng ký từ {{ $fromDate }} đến {{ $toDate }}</span>
                        </h3>
                    </div>
                </div>
                <div class="card-body">
                    <div id="chart_customer_7_day"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-6">
            <div class="card card-custom card-stretch gutter-b">
                <div class="card-header h-auto border-0">
                    <div class="card-title py-5">
                        <h3 class="card-label">
                            <span class="d-block text-dark font-weight-bolder">Tỉ lệ thanh toán từ {{ $fromDate }} đến {{ $toDate }}</span>
                        </h3>
                    </div>
                </div>
                <div class="card-body">
                    <div id="transaction_ratio"></div>
                </div>
            </div>
        </div>
        <div class="col-xl-6">
            <div class="card card-custom card-stretch gutter-b">
                <div class="card-header h-auto border-0">
                    <div class="card-title py-5">
                        <h3 class="card-label">
                            <span
                                class="d-block text-dark font-weight-bolder">Đánh giá sản phẩm từ {{ $fromDate }} đến {{ $toDate }}</span>
                        </h3>
                    </div>
                </div>
                <div class="card-body">
                    <div id="chart_rate_7_day"></div>
                </div>
            </div>
        </div>
    </div>
@stop

@push('page-js')
    <script>
        $(document).ready(() => {
            let arrows = {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>'
            }
            $('#from_date').datepicker({
                format:'dd/mm/yyyy',
                rtl: KTUtil.isRTL(),
                autoclose: true,
                orientation: "bottom left",
                todayHighlight: true,
                templates: arrows,
                endDate: new Date()
            });
            $('#to_date').datepicker({
                format:'dd/mm/yyyy',
                rtl: KTUtil.isRTL(),
                autoclose: true,
                orientation: "bottom left",
                todayHighlight: true,
                templates: arrows,
                endDate: new Date()
            });
        })
    </script>
    <script src="{{ asset('vendor/js/dashboard.js') }}"></script>
@endpush
