@extends('layouts.master')

@section('question','menu-item-active')

@section('breadcrumb')

@endsection

@section('content')
    <div class="d-flex flex-row">
        <!--begin::Aside-->
        <div class="flex-row-auto offcanvas-mobile w-350px w-xl-400px offcanvas-mobile-on" id="kt_chat_aside">
            <!--begin::Card-->
            <div class="card card-custom">
                <div class="card-body">
                    <form action="{{ route('question.update_status', $question->id) }}" method="post">
                        @csrf
                        <input type="hidden" name="status" value="{{ $question->status === 1 ? 0 : 1 }}">
                        <button type="submit" class="btn btn-md btn-{{ $question->status === 1 ? 'warning' : 'success' }}"><i class="fa fa-{{ $question->status === 1 ? 'times' : 'check' }}"></i>{{ $question->status === 1 ? 'Đóng' : 'Mở' }} câu hỏi</button>
                    </form>
                    <div class="mt-7 scroll scroll-pull ps ps--active-y" style="height: 40px; overflow: hidden;">
                        <div class="d-flex align-items-center justify-content-between mb-5">
                            <p class="font-size-h5">
                                {{ $question->question }}
                            </p>
                        </div>
                        <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; height: 40px; right: -2px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 40px;"></div></div></div>
                </div>
            </div>
        </div><div class="offcanvas-mobile-overlay"></div>
        <div class="flex-row-fluid ml-lg-8" id="kt_chat_content">
            <!--begin::Card-->
            <div class="card card-custom">
                <!--begin::Header-->
                <div class="card-header align-items-center px-4 py-3">
                    <div class="text-left flex-grow-1">
                    </div>
                    <div class="text-center flex-grow-1">
                        <div class="text-dark-75 font-weight-bold font-size-h5">{{ $question->customer->name }}</div>
                    </div>
                    <div class="text-right flex-grow-1">
                    </div>
                </div>
                <!--end::Header-->
                <!--begin::Body-->
                <div class="card-body">
                    <!--begin::Scroll-->
                    <div class="scroll scroll-pull ps ps--active-y" style="height: 251px; overflow: hidden;" id="scroll">
                        <!--begin::Messages-->
                        <div class="messages" id="message_container">
                            @foreach($question->replies as $reply)
                                @if($reply->customer !== null)
                                    <div class="d-flex flex-column mb-5 align-items-start">
                                        <div class="d-flex align-items-center">
                                            <div class="symbol symbol-circle symbol-40 mr-3">
                                                <img alt="Pic" src="{{ asset('vendor/media/users/300_12.jpg') }}">
                                            </div>
                                            <div>
                                                <a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">{{ $reply->customer->name }}</a>
                                                <span class="text-muted font-size-sm">{{ $reply->created_at->diffForHumans() }}</span>
                                            </div>
                                        </div>
                                        <div class="mt-2 rounded p-5 bg-light-success text-dark-50 font-weight-bold font-size-lg text-left max-w-400px">{{ $reply->reply }}</div>
                                    </div>
                                @else
                                    <div class="d-flex flex-column mb-5 align-items-end">
                                        <div class="d-flex align-items-center">
                                            <div>
                                                <span class="text-muted font-size-sm">{{ $reply->created_at->diffForHumans() }}</span>
                                                <a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">Bạn</a>
                                            </div>
                                            <div class="symbol symbol-circle symbol-40 ml-3">
                                                <img alt="Pic" src="{{ asset('vendor/media/users/300_21.jpg') }}">
                                            </div>
                                        </div>
                                        <div class="mt-2 rounded p-5 bg-light-primary text-dark-50 font-weight-bold font-size-lg text-left max-w-400px">{{ $reply->reply }}</div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                        <!--end::Messages-->
                        <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; height: 451px; right: -2px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 196px;"></div></div></div>
                        <!--end::Scroll-->
                </div>
                <!--end::Body-->
                <!--begin::Footer-->
                <div class="card-footer align-items-center">
                    <!--begin::Compose-->
                    <textarea class="form-control border-0 p-0" rows="2" placeholder="Nhập tin nhắn" id="message-input"></textarea>

                    <div class="d-flex align-items-center justify-content-between mt-5">
                        <div class="mr-3">
                            <a href="#" class="btn btn-clean btn-icon btn-md mr-1">
                                <i class="flaticon2-photograph icon-lg"></i>
                            </a>
                        </div>
                        <div>
                            <button
                                data-user="{{ Auth::user()->id }}"
                                type="button"
                                id="btn-send"
                                class="btn btn-primary btn-md text-uppercase font-weight-bold chat-send py-2 px-6"
                                data-url="{{ route('question.reply_to_question',$question->id) }}">Gửi</button>
                        </div>
                    </div>
                    <!--begin::Compose-->
                </div>
                <!--end::Footer-->
            </div>
            <!--end::Card-->
        </div>
        <!--end::Content-->
    </div>
@stop

@push('page-js')
    <script>
        "use strict";

        // Class definition
        var KTAppChat = function () {
            var _chatAsideEl;
            var _chatContentEl;

            // Private functions
            var _initAside = function () {
                // User listing
                var cardScrollEl = KTUtil.find(_chatAsideEl, '.scroll');
                var cardBodyEl = KTUtil.find(_chatAsideEl, '.card-body');
                var searchEl = KTUtil.find(_chatAsideEl, '.input-group');

                if (cardScrollEl) {
                    // Initialize perfect scrollbar(see:  https://github.com/utatti/perfect-scrollbar)
                    KTUtil.scrollInit(cardScrollEl, {
                        mobileNativeScroll: true,  // Enable native scroll for mobile
                        desktopNativeScroll: false, // Disable native scroll and use custom scroll for desktop
                        resetHeightOnDestroy: true,  // Reset css height on scroll feature destroyed
                        handleWindowResize: true, // Recalculate hight on window resize
                        rememberPosition: true, // Remember scroll position in cookie
                        height: function() {  // Calculate height
                            var height;

                            if (KTUtil.isBreakpointUp('lg')) {
                                height = KTLayoutContent.getHeight();
                            } else {
                                height = KTUtil.getViewPort().height;
                            }

                            if (_chatAsideEl) {
                                height = height - parseInt(KTUtil.css(_chatAsideEl, 'margin-top')) - parseInt(KTUtil.css(_chatAsideEl, 'margin-bottom'));
                                height = height - parseInt(KTUtil.css(_chatAsideEl, 'padding-top')) - parseInt(KTUtil.css(_chatAsideEl, 'padding-bottom'));
                            }

                            if (cardScrollEl) {
                                height = height - parseInt(KTUtil.css(cardScrollEl, 'margin-top')) - parseInt(KTUtil.css(cardScrollEl, 'margin-bottom'));
                            }

                            if (cardBodyEl) {
                                height = height - parseInt(KTUtil.css(cardBodyEl, 'padding-top')) - parseInt(KTUtil.css(cardBodyEl, 'padding-bottom'));
                            }

                            if (searchEl) {
                                height = height - parseInt(KTUtil.css(searchEl, 'height'));
                                height = height - parseInt(KTUtil.css(searchEl, 'margin-top')) - parseInt(KTUtil.css(searchEl, 'margin-bottom'));
                            }

                            // Remove additional space
                            height = height - 2;

                            return height;
                        }
                    });
                }
            }

            return {
                // Public functions
                init: function() {
                    // Elements
                    _chatAsideEl = KTUtil.getById('kt_chat_aside');
                    _chatContentEl = KTUtil.getById('kt_chat_content');

                    // Init aside and user list
                    _initAside();

                    // Init inline chat example
                    KTLayoutChat.setup(KTUtil.getById('kt_chat_content'));
                }
            };
        }();

        jQuery(document).ready(function() {
            KTAppChat.init();
        });
    </script>
    <script src="{{ asset('vendor/js/question.js') }}"></script>
@endpush
