@extends('layouts.master')

@section('question','menu-item-active')

@section('breadcrumb')

@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <table class="table table-bordered" id="table-slide"></table>
        </div>
    </div>
@stop

@push('page-js')
    <script>
        $(document).ready(function () {
            $('#table-slide').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    method: 'GET',
                    url: '',
                    headers: {'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')}
                },
                drawCallback: function (settings) {
                    const scrollBody = $(settings.nScrollBody);
                    scrollBody.attr('style', 'min-height: 180px;' + scrollBody.attr('style'));
                },
                scrollX: true,
                columns: [
                    {
                        title: 'Câu hỏi',
                        data: 'question',
                        name: 'question',
                        orderable: true,
                        searchable: true,
                        width: 700
                    },
                    {
                        title: 'Người hỏi',
                        data: 'customer',
                        name: 'customer',
                        class: 'text-center',
                        orderable: true,
                        searchable: true
                    },
                    {
                        title: 'Trạng thái',
                        data: 'status',
                        name: 'status',
                        class: 'text-center',
                        orderable: true,
                        searchable: true
                    },
                    {
                        title: 'Người phụ trách',
                        data: 'user',
                        name: 'user',
                        class: 'text-center',
                        orderable: true,
                        searchable: true
                    },
                    {
                        title: 'Action',
                        data: 'actions',
                        name: 'actions',
                        class: 'text-center',
                        orderable: true,
                        searchable: true,
                        width: 100
                    },
                ],
            });
        });

        $(document).on('click', '.close-question', function (event) {
            event.preventDefault();
            Swal.fire({
                title: "Xác nhận đóng",
                text: 'Bạn có muốn đóng câu hỏi này ?',
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "Đóng"
            }).then(function (result) {
                if (result.value) {
                    $(event.target).closest('.close-question').find('form').submit();
                }
            });
        });

        $(document).on('click', '.open-question', function (event) {
            event.preventDefault();
            Swal.fire({
                title: "Xác nhận mở",
                text: 'Bạn có muốn mở câu hỏi này ?',
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "Mở"
            }).then(function (result) {
                if (result.value) {
                    $(event.target).closest('.open-question').find('form').submit();
                }
            });
        });

        $(document).on('click', '.unset-question', function (event) {
            event.preventDefault();
            Swal.fire({
                title: "Xác nhận bỏ phụ trách",
                text: 'Bạn có muốn bỏ phụ trách cho câu hỏi này ?',
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "Thực hiện"
            }).then(function (result) {
                if (result.value) {
                    $(event.target).closest('.unset-question').find('form').submit();
                }
            });
        });
    </script>
@endpush
