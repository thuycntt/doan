@extends('layouts.master')

@section('slide','menu-item-active')

@section('breadcrumb')

@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="d-flex justify-content-end mb-5">
                <a href="{{ route('slide.create')}}" class="btn btn-primary font-weight-bolder">
                    <i class="fa fa-plus"></i>
                    Tạo mới
                </a>
            </div>
            <table class="table table-bordered" id="table-slide"></table>
        </div>
    </div>
@stop

@push('page-js')
    <script>
        $(document).ready(function () {
            $('#table-slide').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    method: 'GET',
                    url: '',
                    headers: {'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')}
                },
                drawCallback: function (settings) {
                    const scrollBody = $(settings.nScrollBody);
                    scrollBody.attr('style', 'min-height: 180px;' + scrollBody.attr('style'));
                },
                scrollX: true,
                columns: [
                    {
                        title: 'Tiêu đề',
                        data: 'name',
                        name: 'name',
                        class: 'text-center',
                        orderable: true,
                        searchable: true
                    },
                    {
                        title: 'Vị trí',
                        data: 'position',
                        name: 'position',
                        class: 'text-center',
                        orderable: true,
                        searchable: true
                    },
                    {
                        title: 'Mô tả',
                        data: 'description',
                        name: 'description',
                        class: 'text-center',
                        orderable: true,
                        searchable: true
                    },
                    {
                        title: 'Tổng số slide con',
                        data: 'total',
                        name: 'total',
                        class: 'text-center',
                        orderable: true,
                        searchable: true
                    },
                    {
                        title: 'Action',
                        data: 'actions',
                        name: 'actions',
                        class: 'text-center',
                        orderable: true,
                        searchable: true,
                        width: 100
                    },
                ],
            });
        });

        $(document).on('click','.delete-slide', function (event) {
            event.preventDefault();
            Swal.fire({
                title: "Xoá",
                text: 'Bạn có muốn xoá slide này ?',
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "Xoá"
            }).then(function (result) {
                if (result.value) {
                    $(event.target).closest('.delete-slide').find('form').submit()
                }
            });
        });
    </script>
@endpush
