@extends('layouts.master')

@section('slide','menu-item-active')
@section('breadcrumb')

@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <form action="{{ route('slide.update',$slide->id) }}" method="post">
                @csrf
                <div class="form-group">
                    <label for="name">Tên</label>
                    <input type="text" class="form-control" placeholder="Tên" name="name" id="name" value="{{$slide->name}}">
                    @error('name')
                    <div class="fv-plugins-message-container"><div class="fv-help-block">{{ $message }}</div></div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="position">Vị trí</label>
                    <select class="form-control" name="position" id="position">
                        <option @if($slide->position==1) selected @endif value="1">Top</option>
                        <option @if($slide->position==2) selected @endif value="2">Middle</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="description">Mô tả</label>
                    <textarea class="form-control" placeholder="Mô tả" rows="5" name="description" id="description">{{$slide->description}}</textarea>
                </div>
                <div class="card-footer d-flex justify-content-end">
                    <button type="submit" class="btn btn-primary mr-2"><i class="fa fa-save"></i>Lưu</button>
                </div>
            </form>
        </div>
    </div>
@stop
