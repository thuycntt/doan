@extends('layouts.master')

@section('product','menu-item-active')

@section('breadcrumb')

@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="d-flex justify-content-end mb-5">
                <a href="{{ route('product.create')}}" class="btn btn-primary font-weight-bolder">
                    <i class="fa fa-plus"></i>
                    Tạo mới
                </a>
            </div>
            <form action="" method="get">
            <div class="mb-7">
                <div class="row align-items-start">
                        <div class="col-3">
                            <div class="row align-items-center">
                                <div class="col-4">
                                    <label class="mr-3 mb-0 d-none d-md-block" for="datatable_status">Trạng thái:</label>
                                </div>
                                <div class="col-8">
                                    <select class="form-control" name="status">
                                        <option @if($status=='show') selected @endif value="show">Đang hiển thị</option>
                                        <option @if($status=='deleted') selected @endif value="deleted">Đã xoá</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="row align-items-center">
                                <div class="col-3">
                                    <label class="mr-3 mb-0 d-none d-md-block" for="datatable_status">Loại:</label>
                                </div>
                                <div class="col-8">
                                    <select class="form-control" name="type">
                                        <option @if($type=='equipment') selected @endif value="equipment">Thiết bị yến</option>
                                        <option @if($type=='product') selected @endif value="product">Thực phẩm yến</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-3"><button type="submit" class="btn btn-info">Xem</button></div>
                </div>
            </div>
            </form>
            <table class="table table-bordered" id="table-product"></table>
        </div>
    </div>
@stop

@push('page-js')
    <script>
        $(document).ready(function () {
            $('#table-product').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    method: 'GET',
                    url: '',
                    headers: {'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')}
                },
                drawCallback: function (settings) {
                    const scrollBody = $(settings.nScrollBody);
                    scrollBody.attr('style', 'min-height: 180px;' + scrollBody.attr('style'));
                },
                scrollX: true,
                columns: [
                    {
                        title: 'Tên sản phẩm',
                        data: 'name',
                        name: 'name',
                        class: 'text-center',
                        orderable: true,
                        searchable: true
                    },
                    {
                        title: 'Hình',
                        data: 'thumbnail',
                        name: 'thumbnail',
                        class: 'text-center',
                        orderable: true,
                        searchable: true
                    },
                    {
                        title: 'Giá',
                        data: 'price',
                        name: 'price',
                        class: 'text-center',
                        orderable: true,
                        searchable: true
                    },
                    {
                        title: 'Số lượng',
                        data: 'quantity',
                        name: 'quantity',
                        class: 'text-center',
                        orderable: true,
                        searchable: true
                    }, {
                        title: 'Trạng thái',
                        data: 'status',
                        name: 'status',
                        class: 'text-center',
                        orderable: true,
                        searchable: true
                    },
                    {
                        title: '!!!',
                        data: 'actions',
                        name: 'actions',
                        class: 'text-center',
                        orderable: false,
                        searchable: false
                    },
                ],
            });
            $(document).on('click', '.delete-product', function (event) {
                event.preventDefault();
                Swal.fire({
                    title: "Xoá",
                    text: 'Bạn có muốn xoá sản phẩm này ?',
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Xoá"
                }).then(function (result) {
                    if (result.value) {
                        $(event.target).closest('.delete-product').find('form').submit()
                    }
                });
            });

            $(document).on('click', '.restore-product', function (event) {
                event.preventDefault();
                Swal.fire({
                    title: "Khôi phục",
                    text: 'Bạn có muốn khôi phục sản phẩm này ?',
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Khôi phục"
                }).then(function (result) {
                    if (result.value) {
                        $(event.target).closest('.restore-product').find('form').submit()
                    }
                });
            });

            // $('#datatable_status').change(event => {
            //     let url = window.location.href.split('?')[0];
            //     window.location.href = url + '?status=' + $(event.target).val()
            // })
        });
    </script>
@endpush
