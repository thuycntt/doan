@extends('layouts.master')

@section('product','menu-item-active')
@section('breadcrumb')

@endsection

@section('content')
    <form action="{{ route('product.store') }}" method="post">
        @csrf

        <div class="row">
            <div class="col-9">
                <div class="card">
                    <div class="card-header" style="padding: 1rem 1rem">
                        {{ get_admin_title(false) }}
                    </div>
                    <div class="card-body">
                        @csrf
                        <div class="form-group" id="abc">
                            <label for="name">Tên</label>
                            <input type="text" class="form-control @error('name') is-invalid @enderror"
                                   placeholder="Tên" id="name" name="name" value="{{ old('name') }}">
                            @error('name')
                            <div class="fv-plugins-message-container">
                                <div class="fv-help-block">{{ $message }}</div>
                            </div>
                            @enderror
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="sku">SKU</label>
                                    <input type="text" class="form-control @error('sku') is-invalid @enderror"
                                           placeholder="SKU"
                                           name="sku" id="sku" value="{{ old('sku') }}">
                                    @error('sku')
                                    <div class="fv-plugins-message-container">
                                        <div class="fv-help-block">{{ $message }}</div>
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="discount">Giảm giá</label>
                                    <input type="text" class="form-control @error('discount') is-invalid @enderror"
                                           placeholder="0"
                                           name="discount" id="sku" value="{{ old('discount') }}">
                                    @error('discount')
                                    <div class="fv-plugins-message-container">
                                        <div class="fv-help-block">{{ $message }}</div>
                                    </div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-1" style="max-width: 4.33333%;">
                                <span class="switch switch-sm switch-icon">
                                    <label>
                                        <input type="checkbox" name="auto_sku" id="auto_sku" value="1">
                                        <span></span>
                                    </label>
                                </span>
                            </div>
                            <label class="col-5 col-form-label" style="padding-top: calc(0.35rem + 1px);">Tự động
                                tạo </label>
                        </div>
                        <div class="form-group row">
                            <div class="col-1" style="max-width: 4.33333%;">
                                <span class="switch switch-sm switch-icon">
                                    <label>
                                        <input type="checkbox" name="trending" id="trending" value="1">
                                        <span></span>
                                    </label>
                                </span>
                            </div>
                            <label class="col-5 col-form-label" style="padding-top: calc(0.35rem + 1px);">Sản phẩm nổi bật</label>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="quantity">Số lượng</label>
                                    <input type="text" class="form-control @error('quantity') is-invalid @enderror"
                                           placeholder="Số lượng" name="quantity" id="quantity"
                                           value="{{ old('quantity') }}">
                                    @error('quantity')
                                    <div class="fv-plugins-message-container">
                                        <div class="fv-help-block">{{ $message }}</div>
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="sku">Giá</label>
                                    <input type="text" class="form-control @error('price') is-invalid @enderror"
                                           placeholder="Giá" name="price" id="price" value="{{ old('price') }}">
                                    @error('price')
                                    <div class="fv-plugins-message-container">
                                        <div class="fv-help-block">{{ $message }}</div>
                                    </div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="description" class="col-12">Mô tả</label>
                            <textarea class="form-control @error('description') is-invalid @enderror"
                                      placeholder="Mô tả" rows="5" name="description"
                                      id="description">{{ old('description') }}</textarea>
                            @error('description')
                            <div class="fv-plugins-message-container">
                                <div class="fv-help-block">{{ $message }}</div>
                            </div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="card mt-5">
                    <div class="card-header" style="padding: 1rem 1rem">
                        Chọn hình sản phẩm
                    </div>
                    <input type="hidden" name="images" id="images">
                    <div class="card-body" id="choose_images_container">
                        <div class="image-box" data-success="false" style="width: max-content;display: inline-block;">
                            <input type="hidden" value="{{ asset('vendor/media/placeholder.png') }}" class="image-data"
                                   data-default="{{ asset('vendor/media/placeholder.png') }}">
                            <div class="preview-image-wrapper ">
                                <img src="{{ asset('vendor/media/placeholder.png') }}" alt="preview image"
                                     class="preview_image" width="150">
                                <a class="btn_remove_choose_image" title="Remove image" href="#">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                            <div class="image-box-actions">
                                <a href="#" class="btn_choose_images">
                                    Chọn hình
                                </a>
                            </div>
                        </div>
                        @error('images')
                        <div class="fv-plugins-message-container">
                            <div class="fv-help-block">{{ $message }}</div>
                        </div>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="col-3">
                <div class="card">
                    <div class="card-header" style="padding: 1rem 1rem;min-height: unset">
                        Hành động
                    </div>
                    <div class="card-body">
                        <button class="btn btn-primary font-weight-bold mr-2 submit-form"><i class="fa fa-save"></i>
                            Lưu
                        </button>
                        <button type="reset" class="btn btn-light-primary font-weight-bold">Huỷ</button>
                    </div>
                </div>
                <div class="card mt-5">
                    <div class="card-header" style="padding: 1rem 1rem">
                        Hình đại diện
                    </div>
                    <div class="card-body">
                        <div class="image-box" id="thumbnail-box">
                            <input type="hidden" name="thumbnail" value="{{ asset('vendor/media/placeholder.png') }}"
                                   class="image-data" data-default="{{ asset('vendor/media/placeholder.png') }}">
                            <div class="preview-image-wrapper ">
                                <img src="{{ asset('vendor/media/placeholder.png') }}" alt="preview image"
                                     class="preview_image" width="150">
                                <a class="btn_remove_image" title="Remove image" href="#">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                            <div class="image-box-actions">
                                <a href="#" class="btn_gallery">
                                    Choose image
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card mt-5">
                    <div class="card-header" style="padding: 1rem 1rem;min-height: unset">
                        Chọn chuyên mục
                    </div>
                    <div class="card-body pb-5" >
                        <div style="height: 500px; overflow-y: scroll">
                            <div class="checkbox-list">
                                @foreach($categories as $category)
                                    <label class="checkbox" data-id="{{ $category->id }}">
                                        <input type="checkbox" categories data-cat="{{$category->type}}"  name="categories[]" value="{{ $category->id }}">
                                        <span></span>{{ $category->type == \App\Models\Category::TYPE_FOOD ? 'Thực phẩm yến' : 'Thiết bị yến' }} | {{ $category->name }}
                                    </label>
                                @endforeach
                            </div>
                        </div>
                        @error('category_id')
                        <div class="fv-plugins-message-container">
                            <div class="fv-help-block">{{ $message }}</div>
                        </div>
                        @enderror
                    </div>
                </div>
            </div>
        </div>
    </form>
@stop

@push('plugin-js')
    <script src="{{ asset('vendor/libraries/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('vendor/libraries/ckfinder/ckfinder.js') }}"></script>
@endpush

@push('page-js')
    <script src="{{ asset('vendor/js/product.js') }}"></script>
@endpush
