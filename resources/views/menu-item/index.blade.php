@extends('layouts.master')

@section('menu','menu-item-active')

@section('breadcrumb')

@endsection

@section('content')
    <div class="row">
        <div class="col-8">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Sắp xếp menu con</h4>
                    <form action="{{ route('menu_item.update_nestable',$menu->id) }}" method="post">
                        @csrf
                        <div class="dd" data-url="{{ route('menu_item.get_data_nestable',$menu->id) }}" style="width: 120%" data-delete="{{ route('category.delete') }}">
                        </div>
                        <input type="hidden" name="data" id="input-nestable" >
                        <input type="hidden" name="menu_id" value="{{$menu->id}}" >
                        <div class="card-footer d-flex justify-content-end">
                            <button type="submit" class="btn btn-primary mr-2"><i class="fa fa-save"></i>Lưu</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-4">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Tạo mới</h4>
                    <form action="{{ route('menu_item.store_or_update') }}"
                          data-delete="{{ route('menu_item.delete') }}"
                          data-restore="{{ route('menu_item.restore') }}"
                          data-soft-delete="{{ route('menu_item.soft_delete') }}"
                          method="post" id="form-cat">
                        @csrf
                        <input type="hidden" name="id" id="id">
                        <input type="hidden" name="menu_id" id="menu_id" value="{{$menu->id}}">
                        <div class="form-group">
                            <label for="name">Tên</label>
                            <input type="text" class="form-control" placeholder="Tên" name="name" id="name">
                            @error('name')
                            <div class="fv-plugins-message-container"><div class="fv-help-block">{{ $message }}</div></div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="url">Liên kết</label>
                            <input type="text" class="form-control" placeholder="Liên kết" name="url" id="url">
                            @error('url')
                            <div class="fv-plugins-message-container"><div class="fv-help-block">{{ $message }}</div></div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="description">Mô tả</label>
                            <textarea class="form-control" placeholder="Mô tả" rows="5" name="description" id="description"></textarea>
                        </div>
                        <div class="card-footer d-flex justify-content-end">
                            <button type="submit" class="btn btn-primary mr-2"><i class="fa fa-save"></i>Lưu</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
@push('plugin-css')
    <link href="{{ asset('vendor/libraries/nestable/nestable.css') }}" rel="stylesheet" type="text/css"/>
@endpush
@push('plugin-js')
    <script type="text/javascript" src="{{ asset('vendor/libraries/nestable/jquery.nestable.js') }}"></script>
@endpush
@push('page-js')
    <script src="{{ asset('vendor/js/menuitem.js') }}"></script>
@endpush
