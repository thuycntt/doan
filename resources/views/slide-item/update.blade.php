@extends('layouts.master')

@section('slide','menu-item-active')
@section('breadcrumb')

@endsection

@section('content')
    <form action="{{ route('slide_item.update',$slideItem->id) }}" method="post">
        <div class="row">
            <div class="col-9">
                <div class="card">
                    <div class="card-body">
                        @csrf
                        <div class="form-group">
                            <label for="url">Liên kết</label>
                            <input type="text" class="form-control" placeholder="Liên kết" name="url" id="url" value="{{ old('url', $slideItem->url) }}">
                            @error('url')
                            <div class="fv-plugins-message-container"><div class="fv-help-block">{{ $message }}</div></div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="status">Trạng thái</label>
                            <select class="form-control" name="status" id="status">
                                <option value="1" @if($slideItem->status == 1) selected @endif >Hiện</option>
                                <option value="2" @if($slideItem->status == 2) selected @endif>Ẩn</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="description">Mô tả</label>
                            <textarea class="form-control" placeholder="Mô tả" rows="5" name="description" id="description">{{ old('description', $slideItem->description) }}</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-3">
                <div class="card">
                    <div class="card-header" style="padding: 1rem 1rem;min-height: unset">
                        Hành động
                    </div>
                    <div class="card-body">
                        <button type="submit" class="btn btn-primary font-weight-bold mr-2"><i class="fa fa-save"></i>
                            Lưu
                        </button>
                        <button type="reset" class="btn btn-light-primary font-weight-bold">Huỷ</button>
                    </div>
                </div>
                <div class="card mt-5">
                    <div class="card-header" style="padding: 1rem 1rem">
                        Hình đại diện
                    </div>
                    <div class="card-body">
                        <div class="image-box">
                            <input type="hidden" name="image" value="{{ asset($slideItem->image) }}" class="image-data" data-default="{{ asset('vendor/media/placeholder.png') }}">
                            <div class="preview-image-wrapper ">
                                <img src="{{ asset($slideItem->image) }}" alt="preview image" class="preview_image" width="150">
                                <a class="btn_remove_image" title="Remove image" href="#">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                            <div class="image-box-actions">
                                <a href="#" class="btn_gallery">
                                    Choose image
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@stop
@push('plugin-js')
    <script src="{{ asset('vendor/libraries/ckfinder/ckfinder.js') }}"></script>
@endpush
@push('page-js')
    <script>
        $(()=>{
            $('.btn_gallery').click(event => {
                event.preventDefault();
                CKFinder.popup({
                    chooseFiles: true,
                    width: 1000,
                    height: 700,
                    onInit: function (finder) {
                        finder.on('files:choose', function (evt) {
                            let file = evt.data.files.first();
                            $('.image-box').find('input').val(file.getUrl());
                            $('.image-box').find('img').attr('src', file.getUrl());
                        });

                        finder.on('file:choose:resizedImage', function (evt) {
                            $('.image-box').find('input').val(evt.data.resizedUrl);
                            $('.image-box').find('img').attr('src', evt.data.resizedUrl);
                        });
                    }
                });
            })
        })
    </script>
@endpush
