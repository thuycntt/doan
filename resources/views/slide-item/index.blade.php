@extends('layouts.master')

@section('slide','menu-item-active')

@section('breadcrumb')

@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="d-flex justify-content-end mb-5">
                <a href="{{ route('slide_item.create', ['slide' => $slide->id])}}"
                   class="btn btn-primary font-weight-bolder">
                    <i class="fa fa-plus"></i>
                    Tạo mới
                </a>
            </div>
            <div id="simple-list" class="row">
                <div id="example1" class="list-group col">
                    @foreach($slide->slideItems as $item)
                        <div class="list-group-item d-flex justify-content-around align-items-center"
                             data-id="{{$item->id}}">
                            <img style="width: 200px" src="{{$item->image}}">
                            <p>{{ $item->url }}</p>
                            <div class="row">
                                <div class="col-1 edit-item">
                                    <a href="{{route("slide_item.edit", $item->id)}}">
                                        <i class="fa fa-pencil-alt text-info"></i>
                                    </a>
                                </div>
                                <div class="col-1 delete-item">
                                    <i class="fa fa-times text-danger"></i>
                                    <form action="{{route("slide_item.delete",$item->id)}}" method="post">@csrf @method('delete')</form>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <form action="{{route('slide_item.update_sort')}}" method="post">
                <div class="row mt-5">
                    @csrf
                    <input type="hidden" name="sort" id="sort">
                    <div class="col-12 text-right">
                        <button type="submit" class="btn btn-info">Lưu</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop
@push('plugin-js')
    <script src="{{asset('vendor\libraries\sortablejs\Sortable.min.js')}}"></script>
@endpush
@push('page-js')
    <script>
        $(document).ready(function () {
            var el = document.getElementById("example1");
            var sortable = Sortable.create(el, {
                onSort: function (/**Event*/ evt) {
                    var order = sortable.toArray();
                    console.log(order)
                    $('#sort').val(JSON.stringify(order));
                },
            });
        });

        $(document).on('click', '.delete-item', function (event) {
            event.preventDefault();
            Swal.fire({
                title: "Xoá",
                text: 'Bạn có muốn xoá slide con này ?',
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "Xoá"
            }).then(function (result) {
                if (result.value) {
                    $(event.target).closest('.delete-item').find('form').submit()
                }
            });
        });
    </script>
@endpush

