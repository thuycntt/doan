@extends('layouts.master')

@section('order','menu-item-active')

@section('breadcrumb')

@endsection

@section('content')
    <div class="card">
        <div class="card-body">
           <form action="" method="get">
               <div class="mb-7">
                   <div class="row align-items-center">
                       <div class="col-3">
                           <div class="row align-items-center">
                               <div class="col-5">
                                   <label class="mr-3 mb-0 d-none d-md-block" for="datatable_status">Trạng thái:</label>
                               </div>
                               <div class="col-7">
                                   <select class="form-control" id="status" name="status">
                                       <option @if($status==\App\Models\Order::STATUS_VERIFY) selected
                                               @endif value="{{ \App\Models\Order::STATUS_VERIFY }}">Chờ xác nhận
                                       </option>
                                       <option @if($status==\App\Models\Order::STATUS_GETTING_PRODUCT) selected
                                               @endif value="{{ \App\Models\Order::STATUS_GETTING_PRODUCT }}">Đang lấy hàng
                                       </option>
                                       <option @if($status==\App\Models\Order::STATUS_PENDING) selected
                                               @endif value="{{ \App\Models\Order::STATUS_PENDING }}">Chờ giao hàng
                                       </option>
                                       <option @if($status==\App\Models\Order::STATUS_SHIPPING) selected
                                               @endif value="{{ \App\Models\Order::STATUS_SHIPPING }}">Đang giao hàng
                                       </option>
                                       <option @if($status==\App\Models\Order::STATUS_COMPLETE) selected
                                               @endif value="{{ \App\Models\Order::STATUS_COMPLETE }}">Đã giao hàng
                                       </option>
                                       <option @if($status==\App\Models\Order::STATUS_CANCEL) selected
                                               @endif value="{{ \App\Models\Order::STATUS_CANCEL }}">Đã bị huỷ
                                       </option>
                                   </select>
                               </div>
                           </div>
                       </div>

                   </div>
                   <div class="row align-items-center mt-5">
                       <div class="col-3">
                           <div class="row align-items-center">
                               <div class="col-5">
                                   <label class="mr-3 mb-0 d-none d-md-block" for="datatable_status">Tỉnh thành:</label>
                               </div>
                               <div class="col-7">
                                   <select style="width:100%" name="province" id="province"
                                           data-url="{{ route('order.get_province') }}" @if(!empty($province)) data-id="{{ $province->id }}" @endif>
                                       @if(!empty($province))
                                       <option value="{{ $province->id  }}" selected>{{  $province->name }}</option>
                                       @endif
                                   </select>
                               </div>
                           </div>
                       </div>
                       <div class="col-3">
                           <div class="row align-items-center">
                               <div class="col-5">
                                   <label class="mr-3 mb-0 d-none d-md-block" for="datatable_status">Quận/Huyện:</label>
                               </div>
                               <div class="col-7">
                                   <select style="width:100%" name="district" id="district"
                                           data-url="{{ route('order.get_district') }}" @if(!empty($district)) data-id="{{ $district->id }}" @endif @if(empty($province))  disabled @endif>
                                       @if(!empty($district))
                                       <option value="{{ $district->id  }}" selected>{{  $district->name }}</option>
                                       @endif
                                   </select>
                               </div>
                           </div>
                       </div>
                       <div class="col-3">
                           <div class="row align-items-center">
                               <div class="col-5">
                                   <label class="mr-3 mb-0 d-none d-md-block" for="datatable_status">Phường/Xã:</label>
                               </div>
                               <div class="col-7">
                                   <select style="width:100%" name="ward" id="ward"
                                           data-url="{{ route('order.get_ward') }}" @if(empty($district))  disabled @endif>
                                       @if(!empty($ward))
                                       <option value="{{ $ward->id  }}" selected>{{  $ward->name }}</option>
                                       @endif
                                   </select>
                               </div>
                           </div>
                       </div>
                       <div class="col-3"><button type="submit" class="btn btn-info">Xem</button></div>
                   </div>
               </div>
           </form>
            <table class="table table-bordered" id="table-order"></table>
        </div>
    </div>
    <div class="modal fade" id="modal-date" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Cập nhật ngày giao hàng</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <form action="" data-url="{{ route('order.update_delivery') }}" method="post">
                    <div class="modal-body">
                        @csrf
                        <div class="form-group">
                            <div class="input-group date mb-2">
                                <input type="text" class="form-control" name="delivery_date"
                                       id="delivery_date">
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="la la-check"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Đóng
                        </button>
                        <button type="submit" class="btn btn-primary font-weight-bold">Lưu</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@push('page-js')
    <script>
        $(document).ready(function () {
            $('#table-order').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    method: 'GET',
                    url: '',
                    headers: {'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')}
                },
                drawCallback: function (settings) {
                    const scrollBody = $(settings.nScrollBody);
                    scrollBody.attr('style', 'min-height: 180px;' + scrollBody.attr('style'));
                },
                scrollX: true,
                columns: [
                    {
                        title: 'Mã đặt hàng',
                        data: 'uuid',
                        name: 'uuid',
                        class: 'text-center',
                        orderable: true,
                        searchable: true
                    },
                    {
                        title: 'Người đặt',
                        data: 'customer',
                        name: 'customer',
                        class: 'text-center',
                        orderable: true,
                        searchable: true
                    },
                    {
                        title: 'Số tiền',
                        data: 'amount',
                        name: 'amount',
                        class: 'text-center',
                        orderable: true,
                        searchable: true
                    },
                    {
                        title: 'Loại thanh toán',
                        data: 'transaction',
                        name: 'transaction',
                        class: 'text-center',
                        orderable: true,
                        searchable: true
                    },
                    {
                        title: 'Trạng thái',
                        data: 'status',
                        name: 'status',
                        class: 'text-center',
                        orderable: true,
                        searchable: true
                    },
                    {
                        title: 'Ngày đặt',
                        data: 'created_at',
                        name: 'created_at',
                        class: 'text-center',
                        orderable: true,
                        searchable: true
                    },
                    {
                        title: '!!!',
                        data: 'actions',
                        name: 'actions',
                        class: 'text-center',
                        orderable: true,
                        searchable: true,
                        width: 100
                    },
                ],
            });
            let arrows = {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>'
            }
            $('#delivery_date').datepicker({
                format: 'dd/mm/yyyy',
                rtl: KTUtil.isRTL(),
                autoclose: true,
                orientation: "bottom left",
                todayHighlight: true,
                templates: arrows,
                startDate: new Date()
            });

            $('#modal-date').on('show.bs.modal', event => {
                if(event.namespace == 'bs.modal'){
                    let id = $(event.relatedTarget).data('id');
                    let form =$('#modal-date').find('form');
                    let url = form.data('url') + '/' + id;
                    form.attr('action', url)
                }

            })
            let province = $('#province').data('id') != undefined ? parseInt($('#province').data('id')) : null;
            let district = $('#district').data('id') != undefined ? parseInt($('#district').data('id')) : null;

            $('#province').select2({
                multiple: false,
                placeholder: 'Chọn Tỉnh thành',
                ajax: {
                    url: $('#province').data('url'),
                    headers: {'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')},
                    method: 'GET',
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            term: params.term,
                            page: params.page,
                        };
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;
                        return {
                            results: $.map(data.data, function (item) {
                                return {
                                    text: item.name,
                                    id: item.id,
                                    title: item.name
                                }
                            }),
                            pagination: {
                                more: (params.page * 10) < data.total
                            }
                        };
                    },
                    cache: true
                },
                escapeMarkup: markup => markup
            });
            $('#district').select2({
                multiple: false,
                placeholder: 'Chọn Quận/huyện',
                ajax: {
                    url: $('#district').data('url'),
                    headers: {'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')},
                    method: 'GET',
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            term: params.term,
                            page: params.page,
                            province_id: province
                        };
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;
                        return {
                            results: $.map(data.data, function (item) {
                                return {
                                    text: item.name,
                                    id: item.id,
                                    title: item.name
                                }
                            }),
                            pagination: {
                                more: (params.page * 10) < data.total
                            }
                        };
                    },
                    cache: true
                },
                escapeMarkup: markup => markup
            });
            $('#ward').select2({
                multiple: false,
                placeholder: 'Chọn Phường/Xã',
                ajax: {
                    url: $('#ward').data('url'),
                    headers: {'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')},
                    method: 'GET',
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            term: params.term,
                            page: params.page,
                            district_id:district
                        };
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;
                        return {
                            results: $.map(data.data, function (item) {
                                return {
                                    text: item.name,
                                    id: item.id,
                                    title: item.name
                                }
                            }),
                            pagination: {
                                more: (params.page * 10) < data.total
                            }
                        };
                    },
                    cache: true
                },
                escapeMarkup: markup => markup
            });

            $('#province').on('select2:select',(event)=>{
                province = event.params.data.id;
                $('#district').prop('disabled',false);
                $('#district').val(null).trigger('change');
                $('#ward').val(null).trigger('change');
                $('#ward').prop('disabled',true);
            })
            $('#district').on('select2:select',(event)=>{
                district = event.params.data.id;
                $('#ward').prop('disabled',false);
                $('#ward').val(null).trigger('change');
            })
        });

        // $('#datatable_status').change(event => {
        //     window.location.href = window.location.href.split('?')[0] + '?status=' + $(event.target).val()
        // });

        $(document).on('click', '.order-verify', event => {
            event.preventDefault();
            Swal.fire({
                title: "Xác nhận đơn hàng",
                text: 'Bạn có muốn xác nhận đơn hàng này ?',
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "Xác nhận"
            }).then(function (result) {
                if (result.value) {
                    $(event.target).closest('.order-verify').find('form').submit()
                }
            });
        })

        $(document).on('click', '.order-pending', event => {
            event.preventDefault();
            Swal.fire({
                title: "Xác nhận đã lấy hàng",
                text: 'Bạn có muốn xác nhận đã lấy hàng cho đơn hàng này ?',
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "Xác nhận"
            }).then(function (result) {
                if (result.value) {
                    $(event.target).closest('.order-pending').find('form').submit()
                }
            });
        })

        $(document).on('click', '.order-shipping', event => {
            event.preventDefault();
            Swal.fire({
                title: "Xác nhận đang vận chuyển",
                text: 'Bạn có muốn xác nhận đang vận chuyển cho đơn hàng này ?',
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "Xác nhận"
            }).then(function (result) {
                if (result.value) {
                    $(event.target).closest('.order-shipping').find('form').submit()
                }
            });
        })

        $(document).on('click', '.order-complete', event => {
            event.preventDefault();
            Swal.fire({
                title: "Xác nhận giao hàng thành công",
                text: 'Bạn có muốn xác nhận giao hàng thành công cho đơn hàng này ?',
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "Xác nhận"
            }).then(function (result) {
                if (result.value) {
                    $(event.target).closest('.order-complete').find('form').submit()
                }
            });
        })

        $(document).on('click', '.order-cancel', event => {
            event.preventDefault();
            Swal.fire({
                title: "Xác nhận huỷ đơn hàng",
                text: 'Bạn có muốn xác nhận huỷ đơn hàng này ?',
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "Xác nhận"
            }).then(function (result) {
                if (result.value) {
                    $(event.target).closest('.order-cancel').find('form').submit()
                }
            });
        })

    </script>
@endpush
