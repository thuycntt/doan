@extends('layouts.master')

@section('order','menu-item-active')

@section('breadcrumb')

@endsection

@section('content')
    <div class="card card-custom overflow-hidden">
        <div class="card-body p-0">
            <!-- begin: Invoice-->
            <!-- begin: Invoice header-->
            <div class="row justify-content-center py-8 px-8 py-md-27 px-md-0">
                <div class="col-md-9">
                    <div class="d-flex justify-content-between pb-10 pb-md-20 flex-column flex-md-row">
                        <h1 class="display-4 font-weight-boldest mb-10">Đơn hàng: {{ $order->uuid }} {!! $status !!}</h1>
                        <div class="d-flex flex-column align-items-md-end px-0">
                            <span class="d-flex flex-column align-items-md-end opacity-70">
                                <h3>{{ $order->created_at->format('d/m/Y H:i') }}</h3>
                            </span>
                        </div>
                    </div>
                    <div class="border-bottom w-100"></div>
                    <div class="d-flex justify-content-between pt-6" style="padding-right: 20px">
                        <div class="d-flex flex-column flex-root">
                            <span class="font-weight-bolder mb-2">Khách hàng</span>
                            <span >{{ $order->customer->name }}</span>
                            <span class="mt-4">{{ $order->customer->phone }}</span>
                            <span >{{ $order->customer->address }}</span>
                        </div>
                        <div class="d-flex flex-column flex-root" style="padding-right: 20px;padding-left: 20px">
                            <span class="font-weight-bolder mb-2">Địa chỉ giao hàng</span>
                            <span>{{ $order->full_name }}</span>
                            <span class="mt-4">{{ $order->phone }}</span>
                            <span>{{ $order->address }}</span>
                            <span class="mt-4">Ghi chú: {{ $order->note }}</span>
                        </div>
                        <div class="d-flex flex-column flex-root">
                            <span class="font-weight-bolder mb-2">Hình thức thanh toán</span>
                            <span>{!! $transactionType !!}</span>

                            <p class="font-weight-bolder mb-2 mt-2">Mã thanh toán</p>
                            <span>{!! $order->transaction->uuid !!}</span>
                            <p class="font-weight-bolder mb-2 mt-2">Ngày dự kiến giao hàng</p>
                            <span>{!! Carbon\Carbon::parse($order->delivery_date)->format('d/m/Y') !!}</span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end: Invoice header-->
            <!-- begin: Invoice body-->
            <div class="row justify-content-center py-8 px-8 py-md-10 px-md-0">
                <div class="col-md-9">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th class="pl-0 font-weight-bold text-muted text-uppercase">Tên</th>
                                <th class="text-right font-weight-bold text-muted text-uppercase">Số lượng</th>
                                <th class="text-right font-weight-bold text-muted text-uppercase">Giá đơn</th>
                                <th class="text-right pr-0 font-weight-bold text-muted text-uppercase">Giá</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php($total = 0)
                            @foreach($order->products as $product)
                                @php($price =($product->pivot->product_price-$product->pivot->product_price*($product->pivot->product_discount/100))*$product->pivot->quantity)
                                <tr class="font-weight-boldest">
                                    <td class="pl-0 pt-7">{{ $product->name }}</td>
                                    <td class="text-right pt-7">{{ $product->pivot->quantity }}</td>
                                    <td class="text-right pt-7">{{ number_format($product->pivot->product_price-$product->pivot->product_price*($product->pivot->product_discount/100)) }} VND</td>
                                    <td class="text-danger pr-0 pt-7 text-right">{{ number_format($price) }} VND</td>
                                </tr>
                                @php($total = $total + $price)
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- end: Invoice body-->
            <!-- begin: Invoice footer-->
            <div class="row justify-content-center bg-gray-100 py-8 px-8 py-md-10 px-md-0">
                <div class="col-md-9">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            </thead>
                            <tbody>
                            <tr class="font-weight-bolder">
                                <td class="font-size-h4 font-weight-boldest">Phí giao hàng: <span class="text-danger font-size-h3 font-weight-boldest">{{ number_format($order->ship_fee) }} VND</span></td>
                            </tr>
                            <tr class="font-weight-bolder">
                                <td class="font-size-h3 font-weight-boldest">Tổng đơn hàng: <span class="text-danger font-size-h3 font-weight-boldest">{{ number_format($total + $order->ship_fee) }} VND</span></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- end: Invoice footer-->
            <!-- end: Invoice-->
        </div>
    </div>
@stop

@push('page-js')
@endpush
