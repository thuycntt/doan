<div class="dropdown dropdown-inline">
    <a href="javascript:;" class="btn btn-sm btn-clean btn-icon mr-2" data-toggle="dropdown" aria-expanded="false">
        <span class="svg-icon svg-icon-md">
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                 height="24px" viewBox="0 0 24 24" version="1.1">
                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <rect x="0" y="0" width="24" height="24"></rect>
                    <path
                        d="M5,8.6862915 L5,5 L8.6862915,5 L11.5857864,2.10050506 L14.4852814,5 L19,5 L19,9.51471863 L21.4852814,12 L19,14.4852814 L19,19 L14.4852814,19 L11.5857864,21.8994949 L8.6862915,19 L5,19 L5,15.3137085 L1.6862915,12 L5,8.6862915 Z M12,15 C13.6568542,15 15,13.6568542 15,12 C15,10.3431458 13.6568542,9 12,9 C10.3431458,9 9,10.3431458 9,12 C9,13.6568542 10.3431458,15 12,15 Z"
                        fill="#000000"></path>
                </g>
            </svg>
        </span>
    </a>
    <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right" style="display: none;">
        <ul class="navi flex-column navi-hover py-2">
            <li class="navi-item order-view">
                <a href="{{ route('order.detail', $order->id) }}" class="navi-link" target="_blank">
                    <span class="navi-icon">
                        <i class="fa fa-eye"></i>
                    </span>
                    <span class="navi-text">Xem</span>
                </a>
            </li>
            @if(!($order->status === \App\Models\Order::STATUS_COMPLETE ||$order->status === \App\Models\Order::STATUS_CANCEL) )
            <li class="navi-item order-view" >
                <a href="#" class="navi-link" data-toggle="modal" data-target="#modal-date" data-id="{{$order->id}}">
                    <span class="navi-icon">
                        <i class="fa fa-pencil-alt"></i>
                    </span>
                    <span class="navi-text">Cập nhật ngày giao hàng</span>
                </a>
            </li>
            @endif
            @if($order->status === \App\Models\Order::STATUS_VERIFY)
                <li class="navi-item order-verify">
                    <a href="" class="navi-link">
                    <span class="navi-icon">
                        <i class="fa fa-check"></i>
                    </span>
                        <span class="navi-text">Xác nhận đơn hàng</span>
                    </a>
                    <form action="{{ route('order.update_status') }}" method="POST">
                        @csrf
                        <input type="hidden" name="order_uuid" value="{{ $order->uuid }}">
                        <input type="hidden" name="status" value="{{ \App\Models\Order::STATUS_GETTING_PRODUCT }}">
                    </form>
                </li>
                <li class="navi-item order-cancel">
                    <a href="" class="navi-link">
                <span class="navi-icon">
                    <i class="fa fa-times"></i>
                </span>
                        <span class="navi-text">Huỷ đơn hàng</span>
                    </a>
                    <form action="{{ route('order.update_status') }}" method="POST">
                        @csrf
                        <input type="hidden" name="order_uuid" value="{{ $order->uuid }}">
                        <input type="hidden" name="status" value="{{ \App\Models\Order::STATUS_CANCEL }}">
                    </form>
                </li>
            @endif
            @if($order->status === \App\Models\Order::STATUS_GETTING_PRODUCT)
                <li class="navi-item order-pending">
                    <a href="" class="navi-link">
                <span class="navi-icon">
                    <i class="fa fa-check"></i>
                </span>
                        <span class="navi-text">Đã lấy hàng</span>
                    </a>
                    <form action="{{ route('order.update_status') }}" method="POST">
                        @csrf
                        <input type="hidden" name="order_uuid" value="{{ $order->uuid }}">
                        <input type="hidden" name="status" value="{{ \App\Models\Order::STATUS_PENDING }}">
                    </form>
                </li>
                <li class="navi-item order-cancel">
                    <a href="" class="navi-link">
                <span class="navi-icon">
                    <i class="fa fa-times"></i>
                </span>
                        <span class="navi-text">Huỷ đơn hàng</span>
                    </a>
                    <form action="{{ route('order.update_status') }}" method="POST">
                        @csrf
                        <input type="hidden" name="order_uuid" value="{{ $order->uuid }}">
                        <input type="hidden" name="status" value="{{ \App\Models\Order::STATUS_CANCEL }}">
                    </form>
                </li>
            @endif
            @if($order->status === \App\Models\Order::STATUS_PENDING)
                <li class="navi-item order-shipping">
                    <a href="" class="navi-link">
                        <span class="navi-icon">
                            <i class="fa fa-check"></i>
                        </span>
                        <span class="navi-text">Đang vận chuyển</span>
                    </a>
                    <form action="{{ route('order.update_status') }}" method="POST">
                        @csrf
                        <input type="hidden" name="order_uuid" value="{{ $order->uuid }}">
                        <input type="hidden" name="status" value="{{ \App\Models\Order::STATUS_SHIPPING }}">
                    </form>
                </li>
            @endif
            @if($order->status === \App\Models\Order::STATUS_SHIPPING)
                <li class="navi-item order-complete">
                    <a href="" class="navi-link">
                    <span class="navi-icon">
                        <i class="fa fa-check"></i>
                    </span>
                        <span class="navi-text">Giao hàng thành công</span>
                    </a>
                    <form action="{{ route('order.update_status') }}" method="POST">
                        @csrf
                        <input type="hidden" name="order_uuid" value="{{ $order->uuid }}">
                        <input type="hidden" name="status" value="{{ \App\Models\Order::STATUS_COMPLETE }}">
                    </form>
                </li>
            @endif
        </ul>
    </div>
</div>

