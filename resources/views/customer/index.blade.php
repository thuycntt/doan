@extends('layouts.master')

@section('customer','menu-item-active')

@section('breadcrumb')

@endsection

@section('content')
    <div class="card">
        <div class="card-body">
{{--            <div class="d-flex justify-content-end mb-5">--}}
{{--                <a href="{{ route('menu.create')}}" class="btn btn-primary font-weight-bolder">--}}
{{--                    <i class="fa fa-plus"></i>--}}
{{--                    Tạo mới--}}
{{--                </a>--}}
{{--            </div>--}}
            <table class="table table-bordered" id="table-customer"></table>
        </div>
    </div>
@stop

@push('page-js')
    <script>
        $(document).ready(function () {
            $('#table-customer').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    method: 'GET',
                    url: '',
                    headers: {'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')}
                },
                drawCallback: function (settings) {
                    const scrollBody = $(settings.nScrollBody);
                    scrollBody.attr('style', 'min-height: 180px;' + scrollBody.attr('style'));
                },
                scrollX: true,
                columns: [
                    {
                        title: 'Tên',
                        data: 'name',
                        name: 'name',
                        orderable: true,
                        searchable: true
                    },
                    {
                        title: 'Email',
                        data: 'email',
                        name: 'email',
                        class: 'text-center',
                        orderable: true,
                        searchable: true
                    },
                    {
                        title: 'SDT',
                        data: 'phone',
                        name: 'phone',
                        class: 'text-center',
                        orderable: true,
                        searchable: true
                    },
                    {
                        title: 'Ngày đăng ký',
                        data: 'created_at',
                        name: 'created_at',
                        class: 'text-center',
                        orderable: true,
                        searchable: true
                    },
                    {
                        title: '!!!',
                        data: 'actions',
                        name: 'actions',
                        class: 'text-center',
                        orderable: true,
                        searchable: true
                    },
                ],
            });
        });
    </script>
@endpush
