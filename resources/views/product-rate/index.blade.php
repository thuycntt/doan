@extends('layouts.master')

@section('product-rate','menu-item-active')

@section('breadcrumb')

@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <table class="table table-bordered" id="table-product-rate"></table>
        </div>
    </div>
@stop

@push('page-js')
    <script>
        $(document).ready(function () {
            $('#table-product-rate').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    method: 'GET',
                    url: '',
                    headers: {'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')}
                },
                drawCallback: function (settings) {
                    const scrollBody = $(settings.nScrollBody);
                    scrollBody.attr('style', 'min-height: 180px;' + scrollBody.attr('style'));
                },
                scrollX: true,
                columns: [
                    {
                        title: 'Bình luận',
                        data: 'comment',
                        name: 'comment',
                        orderable: true,
                        width:500,
                        searchable: true
                    },{
                        title: 'Sản phẩm',
                        data: 'product',
                        name: 'product',
                    },
                    {
                        title: 'Đánh giá',
                        data: 'rate',
                        name: 'rate',
                        class: 'text-center',
                        width:50,
                    },
                    {
                        title: 'Trạng thái',
                        data: 'status',
                        name: 'status',
                        class: 'text-center',
                        orderable: true,
                        searchable: true
                    },
                    {
                        title: 'Khách hàng',
                        data: 'customer',
                        name: 'customer',
                        class: 'text-center',
                        orderable: false,
                        searchable: false
                    },{
                        title: 'Người duyệt',
                        data: 'user',
                        name: 'user',
                        class: 'text-center',
                        orderable: false,
                        searchable: false
                    },
                    {
                        title: '!!!',
                        data: 'actions',
                        name: 'actions',
                        class: 'text-center',
                        orderable: false,
                        searchable: false
                    },
                ],
            });
            $(document).on('click', '.accept-rate', function (event) {
                event.preventDefault();
                Swal.fire({
                    title: "Duyệt đánh giá",
                    text: 'Bạn có muốn duyệt đánh giá này ?',
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonText: "OK"
                }).then(function (result) {
                    if (result.value) {
                        $(event.target).closest('.accept-rate').find('form').submit()
                    }
                });
            });

            $(document).on('click', '.reject-rate', function (event) {
                event.preventDefault();
                Swal.fire({
                    title: "Từ chối đánh giá",
                    text: 'Bạn có muốn từ chối đánh giá này ?',
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonText: "OK"
                }).then(function (result) {
                    if (result.value) {
                        $(event.target).closest('.reject-rate').find('form').submit()
                    }
                });
            });
        });
    </script>
@endpush
