@extends('layouts.master')

@section('transaction','menu-item-active')

@section('breadcrumb')

@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <table class="table table-bordered" id="table-transaction"></table>
        </div>
    </div>
@stop

@push('page-js')
    <script>
        $(document).ready(function () {
            $('#table-transaction').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    method: 'GET',
                    url: '',
                    headers: {'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')}
                },
                drawCallback: function (settings) {
                    const scrollBody = $(settings.nScrollBody);
                    scrollBody.attr('style', 'min-height: 180px;' + scrollBody.attr('style'));
                },
                scrollX: true,
                columns: [
                    {
                        title: 'Mã thanh toán ',
                        data: 'uuid',
                        name: 'uuid',
                        class: 'text-center',
                        orderable: true,
                        searchable: true
                    },
                    {
                        title: 'Loại thanh toán',
                        data: 'type',
                        name: 'type',
                        class: 'text-center',
                        orderable: true,
                        searchable: true
                    },
                    {
                        title: 'Khách hàng',
                        data: 'customer',
                        name: 'customer',
                        class: 'text-center',
                        orderable: true,
                        searchable: true
                    },
                    {
                        title: 'Đơn hàng',
                        data: 'order',
                        name: 'order',
                        class: 'text-center',
                        orderable: true,
                        searchable: true
                    },
                    {
                        title: 'Trạng thái',
                        data: 'status',
                        name: 'status',
                        class: 'text-center',
                        orderable: true,
                        searchable: true
                    },
                    {
                        title: 'Tổng tiền',
                        data: 'amount',
                        name: 'amount',
                        class: 'text-center',
                        orderable: true,
                        searchable: true
                    }
                ],
            });
        });

        $(document).on('click', '.delete-article', function (event) {
            event.preventDefault();
            Swal.fire({
                title: "Xoá",
                text: 'Bạn có muốn xoá bài viết này ?',
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "Xoá"
            }).then(function (result) {
                if (result.value) {
                    $(event.currentTarget).find('form').submit()
                }
            });
        });
    </script>
@endpush
