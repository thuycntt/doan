<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title> {{ get_admin_title() }}</title>
    <meta name="description" content="Page with empty content"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon shortcut" href="{{ config('core.base.general.admin_favicon') }}">

    <!--begin::Fonts-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700"/>
    <!--end::Fonts-->

    <!--begin::Global Theme Styles(used by all pages)-->
    <link href="{{ asset('vendor/css/plugins.bundle.css?v=7.0.6') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('vendor/libraries/datatables/datatables.bundle.css?v=7.0.6') }}" rel="stylesheet" type="text/css"/>
    @stack('plugin-css')
    <link href="{{ asset('vendor/css/style.bundle.css?v=7.0.6') }}" rel="stylesheet" type="text/css"/>
    @stack('page-css')
    <!--end::Global Theme Styles-->

    <!--begin::Layout Themes(used by all pages)-->
    <link href="{{ asset('vendor/css/themes/header/base/dark.css?v=7.0.6') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('vendor/css/themes/header/menu/dark.css?v=7.0.6') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('vendor/css/themes/brand/dark.css?v=7.0.6') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('vendor/css/themes/aside/dark.css?v=7.0.6') }}" rel="stylesheet" type="text/css"/>
</head>
<!--end::Head-->
<!--begin::Body-->
<body id="kt_body" class="@yield('body-class')">
@yield('page')
<script>let KTAppSettings = { "breakpoints": { "sm": 576, "md": 768, "lg": 992, "xl": 1200, "xxl": 1400 }, "colors": { "theme": { "base": { "white": "#ffffff", "primary": "#3699FF", "secondary": "#E5EAEE", "success": "#1BC5BD", "info": "#8950FC", "warning": "#FFA800", "danger": "#F64E60", "light": "#E4E6EF", "dark": "#181C32" }, "light": { "white": "#ffffff", "primary": "#E1F0FF", "secondary": "#EBEDF3", "success": "#C9F7F5", "info": "#EEE5FF", "warning": "#FFF4DE", "danger": "#FFE2E5", "light": "#F3F6F9", "dark": "#D6D6E0" }, "inverse": { "white": "#ffffff", "primary": "#ffffff", "secondary": "#3F4254", "success": "#ffffff", "info": "#ffffff", "warning": "#ffffff", "danger": "#ffffff", "light": "#464E5F", "dark": "#ffffff" } }, "gray": { "gray-100": "#F3F6F9", "gray-200": "#EBEDF3", "gray-300": "#E4E6EF", "gray-400": "#D1D3E0", "gray-500": "#B5B5C3", "gray-600": "#7E8299", "gray-700": "#5E6278", "gray-800": "#3F4254", "gray-900": "#181C32" } }, "font-family": "Poppins" };</script>
<script src="{{ asset('vendor/js/plugins.bundle.js?v=7.0.6') }}"></script>
<script src="{{ asset('vendor/js/scripts.bundle.js?v=7.0.6') }}"></script>
<script src="{{ asset('vendor/libraries/datatables/datatables.bundle.js?v=7.0.6') }}"></script>
@stack('plugin-js')
<script>
    $(()=>{
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "positionClass": "toast-bottom-right",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "0",
            "hideDuration": "200",
            "timeOut": "3000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
    })
</script>
@stack('page-js')
@if(session()->has('noties'))
    <script>$(()=>{let heading = "{{ \Arr::get(session()->get('noties'), 'heading', 'Notify') }}";let message = "{{ \Arr::get(session()->get('noties'), 'message', 'Message') }}";toastr.{{ \Arr::get(session()->get('noties'), 'type', 'info') }}(heading, message);})</script>
@endif
</body>
<!--end::Body-->
</html>
