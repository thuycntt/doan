<div id="kt_header" class="header header-fixed">
    <!--begin::Container-->
    <div class="container-fluid d-flex align-items-stretch justify-content-between">
        <!--begin::Header Menu Wrapper-->
        <div class="header-menu-wrapper header-menu-wrapper-left" id="kt_header_menu_wrapper">
            <!--begin::Header Menu-->
            <div id="kt_header_menu" class="header-menu header-menu-mobile header-menu-layout-default">
            </div>
            <!--end::Header Menu-->
        </div>
        <!--end::Header Menu Wrapper-->
        <!--begin::Topbar-->
        <div class="topbar">
            <!--begin::User-->
            <div class="dropdown">
                <!--begin::Toggle-->
                <div class="topbar-item" data-toggle="dropdown" data-offset="10px,0px" aria-expanded="false">
                    <div class="btn btn-icon btn-clean btn-dropdown btn-lg mr-1 pulse pulse-primary">
                        <span class="svg-icon svg-icon-xl svg-icon-primary">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <rect x="0" y="0" width="24" height="24"></rect>
                                    <path d="M2.56066017,10.6819805 L4.68198052,8.56066017 C5.26776695,7.97487373 6.21751442,7.97487373 6.80330086,8.56066017 L8.9246212,10.6819805 C9.51040764,11.267767 9.51040764,12.2175144 8.9246212,12.8033009 L6.80330086,14.9246212 C6.21751442,15.5104076 5.26776695,15.5104076 4.68198052,14.9246212 L2.56066017,12.8033009 C1.97487373,12.2175144 1.97487373,11.267767 2.56066017,10.6819805 Z M14.5606602,10.6819805 L16.6819805,8.56066017 C17.267767,7.97487373 18.2175144,7.97487373 18.8033009,8.56066017 L20.9246212,10.6819805 C21.5104076,11.267767 21.5104076,12.2175144 20.9246212,12.8033009 L18.8033009,14.9246212 C18.2175144,15.5104076 17.267767,15.5104076 16.6819805,14.9246212 L14.5606602,12.8033009 C13.9748737,12.2175144 13.9748737,11.267767 14.5606602,10.6819805 Z" fill="#000000" opacity="0.3"></path>
                                    <path d="M8.56066017,16.6819805 L10.6819805,14.5606602 C11.267767,13.9748737 12.2175144,13.9748737 12.8033009,14.5606602 L14.9246212,16.6819805 C15.5104076,17.267767 15.5104076,18.2175144 14.9246212,18.8033009 L12.8033009,20.9246212 C12.2175144,21.5104076 11.267767,21.5104076 10.6819805,20.9246212 L8.56066017,18.8033009 C7.97487373,18.2175144 7.97487373,17.267767 8.56066017,16.6819805 Z M8.56066017,4.68198052 L10.6819805,2.56066017 C11.267767,1.97487373 12.2175144,1.97487373 12.8033009,2.56066017 L14.9246212,4.68198052 C15.5104076,5.26776695 15.5104076,6.21751442 14.9246212,6.80330086 L12.8033009,8.9246212 C12.2175144,9.51040764 11.267767,9.51040764 10.6819805,8.9246212 L8.56066017,6.80330086 C7.97487373,6.21751442 7.97487373,5.26776695 8.56066017,4.68198052 Z" fill="#000000"></path>
                                </g>
                            </svg>
                        </span>
                        @if(auth()->user()->unReadNotifications->count() > 0)
                            <span class="pulse-ring"></span>
                        @endif
                    </div>
                </div>
                <!--end::Toggle-->
                <!--begin::Dropdown-->
                <div class="dropdown-menu p-0 m-0 dropdown-menu-right dropdown-menu-anim-up dropdown-menu-lg" style="">
                    <!--begin::Header-->
                    <div class="d-flex flex-column pt-12 bgi-size-cover bgi-no-repeat rounded-top" style="background-color: #0a90eb">
                        <!--begin::Title-->
                        <h4 class="d-flex flex-center rounded-top">
                            <span class="text-white">Thông báo</span>
                            <span class="btn btn-text btn-success btn-sm font-weight-bold btn-font-md ml-2">{{ auth()->user()->unReadNotifications->count() }}</span>
                        </h4>
                    </div>
                    <!--end::Header-->
                    <!--begin::Content-->
                    <div class="tab-content">
                        <div class="tab-pane active show p-8" id="topbar_notifications_notifications" role="tabpanel">
                            <!--begin::Scroll-->
                            <div class="scroll pr-7 mr-n7 ps" data-scroll="true" data-height="300" data-mobile-height="200" style="height: 300px; overflow: hidden;">
                                @foreach(auth()->user()->unReadNotifications as $notify)
                                    <div class="d-flex align-items-center mb-6">
                                        <div class="d-flex flex-column font-weight-bold notification-item">
                                            <form action="{{ route('user.mask_as_read_notify', $notify->id) }}" method="post">@csrf <input type="hidden" name="redirect" value="{{ route($notify->type === 'App\Notifications\CustomerReplyQuestionNotification' ? 'question.detail' : 'diary.detail',$notify->data['data']['id']) }}"></form>
                                            <a href="#" class="text-dark text-hover-primary mb-1 font-size-lg">
                                                @if($notify->type === 'App\Notifications\CustomerReplyQuestionNotification')
                                                    Phản hồi câu hỏi
                                                @else
                                                    Phản hồi nhật kí
                                                @endif
                                            </a>
                                            <span class="text-muted">Khách hàng <strong>{{ $notify->data['data']['customer_name'] }}</strong> đã phản hồi về 1 {{ $notify->type === 'App\Notifications\CustomerReplyQuestionNotification' ? 'câu hỏi' : 'nhật ký' }}</span>
                                        </div>
                                    </div>
                                @endforeach
                                <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div></div></div>
                            <!--end::Scroll-->
                            <!--begin::Action-->
                            <div class="d-flex flex-center pt-7">
                                <a href="#" class="btn btn-light-primary font-weight-bold text-center">See All</a>
                            </div>
                            <!--end::Action-->
                        </div>
                    </div>
                    <!--end::Content-->
                </div>
                <!--end::Dropdown-->
            </div>
            <div class="topbar-item ml-5">
                <div class="btn btn-icon btn-icon-mobile w-auto btn-clean d-flex align-items-center btn-lg px-2" id="kt_quick_user_toggle">
                    <span class="text-muted font-weight-bold font-size-base d-none d-md-inline mr-1">Xin chào,</span>
                    <span class="text-dark-50 font-weight-bolder font-size-base d-none d-md-inline mr-3">{{ user_full_name() }}</span>
                    <span class="symbol symbol-lg-35 symbol-25 symbol-light-success"><span class="symbol-label font-size-h5 font-weight-bold">S</span></span>
                </div>
            </div>
            <!--end::User-->
        </div>
        <!--end::Topbar-->
    </div>
    <!--end::Container-->
</div>
