@if (count($breadcrumbs))
    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
        <li class="breadcrumb-item">
            <i class="flaticon2-shelter mr-3 "></i>
            <a href="#" class="text-muted">Dashboard</a>
        </li>
        @foreach ($breadcrumbs as $breadcrumb)
            @if ($breadcrumb->url && !$loop->last)
                <li class="breadcrumb-item">
                    <a href="{{ 'javascript:;' }}" class="text-muted">{{ $breadcrumb->title }}</a>
                </li>
            @else
                <li class="breadcrumb-item">
                    <a href="#" class="text-muted">{{ $breadcrumb->title }}</a>
                </li>
            @endif
        @endforeach
    </ul>
@endif

