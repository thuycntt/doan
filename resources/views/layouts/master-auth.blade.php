@extends('layouts.base')

@section('body-class','kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading')
@section('page')
    <div class="d-flex flex-column flex-root">
        <!--begin::Login-->
        @yield('content')
        <!--end::Login-->
    </div>
@stop

@push('page-css')
    <link href="{{ asset('vendor/css/login.css') }}" rel="stylesheet" type="text/css"/>
@endpush
