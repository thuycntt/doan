@extends('layouts.master')

@section('article-parent','menu-item-open menu-item-here')
@section('article','menu-item-active')
@section('breadcrumb')

@endsection

@section('content')
    <form action="{{ route('article.store') }}" method="post">
        @csrf
        <div class="row">
            <div class="col-9">
                <div class="card">
                    <div class="card-header" style="padding: 1rem 1rem">
                        {{ get_admin_title(false) }}
                    </div>
                    <div class="card-body">
                        @csrf
                        <div class="form-group">
                            <label>Tên</label>
                            <input type="text" class="form-control @error('title') is-invalid @enderror"
                                   placeholder="Tên" name="title" value="{{ old('title') }}">
                            @error('title')
                            <div class="fv-plugins-message-container">
                                <div class="fv-help-block">{{ $message }}</div>
                            </div>
                            @enderror
                        </div>
                        <div class="form-group row">
                            <label class="col-1 col-form-label">Bài nổi bật</label>
                            <div class="col-3">
                                <span class="switch switch-sm switch-icon">
                                    <label>
                                        <input type="checkbox" checked="checked" name="is_feature" value="1">
                                        <span></span>
                                    </label>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="short_content">Mô tả ngắn</label>
                            <textarea class="form-control @error('short_content') is-invalid @enderror"
                                      placeholder="Mô tả ngắn" rows="5" id="short_content"
                                      name="short_content">{{ old('short_content') }}</textarea>
                            @error('short_content')
                            <div class="fv-plugins-message-container">
                                <div class="fv-help-block">{{ $message }}</div>
                            </div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="content" class="col-12">Nội dung</label>
{{--                            <button class="btn btn-outline-dark mb-2 mt-2" data-action="destroy-editor">Ẩn/hiện editor--}}
{{--                            </button>--}}
                            <textarea class="form-control @error('content') is-invalid @enderror" placeholder="Nội dung"
                                      rows="5" name="content" id="content">{{ old('content') }}</textarea>
                            @error('content')
                            <div class="fv-plugins-message-container">
                                <div class="fv-help-block">{{ $message }}</div>
                            </div>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-3">
                <div class="card">
                    <div class="card-header" style="padding: 1rem 1rem;min-height: unset">
                        Hành động
                    </div>
                    <div class="card-body">
                        <button type="submit" class="btn btn-primary font-weight-bold mr-2"><i class="fa fa-save"></i>
                            Lưu
                        </button>
                        <button type="reset" class="btn btn-light-primary font-weight-bold">Huỷ</button>
                    </div>
                </div>
                <div class="card mt-5">
                    <div class="card-header" style="padding: 1rem 1rem">
                        Hình đại diện
                    </div>
                    <div class="card-body">
                        <div class="image-box">
                            <input type="hidden" name="thumbnail" value="{{ asset('vendor/media/placeholder.png') }}"
                                   class="image-data" data-default="{{ asset('vendor/media/placeholder.png') }}">
                            <div class="preview-image-wrapper ">
                                <img src="{{ asset('vendor/media/placeholder.png') }}" alt="preview image"
                                     class="preview_image" width="150">
                                <a class="btn_remove_image" title="Remove image" href="#">

                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                            <div class="image-box-actions">
                                <a href="#" class="btn_gallery">
                                    Choose image
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card mt-5">
                    <div class="card-header" style="padding: 1rem 1rem;min-height: unset">
                        Trạng thái
                    </div>
                    <div class="card-body">
                        <select class="form-control" name="status" id="status">
                            <option value="1">Xuất bản</option>
                            <option value="2" selected>Nháp</option>
                        </select>
                        @error('status')
                        <div class="fv-plugins-message-container">
                            <div class="fv-help-block">{{ $message }}</div>
                        </div>
                        @enderror
                    </div>
                </div>
                <div class="card mt-5">
                    <div class="card-header" style="padding: 1rem 1rem;min-height: unset">
                        Chọn Tag
                    </div>
                    <div class="card-body">
                        <select style="width:100%" name="tags[]" id="tags"
                                data-url="{{ route('tag.select2') }}"></select>
                        @error('tags')
                        <div class="fv-plugins-message-container">
                            <div class="fv-help-block">{{ $message }}</div>
                        </div>
                        @enderror
                    </div>
                </div>
            </div>
        </div>
    </form>
@stop

@push('plugin-js')
    <script src="{{ asset('vendor/libraries/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('vendor/libraries/ckfinder/ckfinder.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.20.0/axios.min.js"></script>
@endpush

@push('page-js')
    <script>

        $(() => {
            if (window.CKEDITOR !== undefined) {
                let config = {
                    filebrowserImageBrowseUrl: '/ckfinder/browser',
                    filebrowserImageUploadUrl: $('meta[name="csrf-token"]').attr('content'),
                    filebrowserWindowWidth: '1200',
                    filebrowserWindowHeight: '750',
                    height: 500,
                    allowedContent: true
                };
                CKEDITOR.replace(document.getElementById('content'), config);

                $('[data-action=destroy-editor]')
                    .off('click')
                    .on('click', event => {
                        event.preventDefault();
                        if (CKEDITOR.instances['content']) {
                            CKEDITOR.instances['content'].destroy();
                        } else {
                            CKEDITOR.replace(document.getElementById('content'), config);
                        }
                    })
            }

            $('.btn_gallery').click(event => {
                event.preventDefault();
                CKFinder.popup({
                    chooseFiles: true,
                    width: 1000,
                    height: 700,
                    onInit: function (finder) {
                        finder.on('files:choose', function (evt) {
                            let file = evt.data.files.first();
                            $('.image-box').find('input').val(file.getUrl());
                            $('.image-box').find('img').attr('src', file.getUrl());
                        });

                        finder.on('file:choose:resizedImage', function (evt) {
                            $('.image-box').find('input').val(evt.data.resizedUrl);
                            $('.image-box').find('img').attr('src', evt.data.resizedUrl);
                        });
                    }
                });
            })

            $('#tags').select2({
                multiple: true,
                placeholder: 'Chọn Tag',
                ajax: {
                    url: $('#tags').data('url'),
                    headers: {'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')},
                    method: 'POST',
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            term: params.term,
                            page: params.page,
                        };
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;
                        return {
                            results: $.map(data.data, function (item) {
                                return {
                                    text: item.name,
                                    id: item.id,
                                    title: item.name
                                }
                            }),
                            pagination: {
                                more: (params.page * 10) < data.total
                            }
                        };
                    },
                    cache: true
                },
                escapeMarkup: markup => markup
            });
        })
    </script>
@endpush
