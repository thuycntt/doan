<div class="dropdown dropdown-inline">
    <a href="javascript:;" class="btn btn-sm btn-clean btn-icon mr-2" data-toggle="dropdown" aria-expanded="false">
        <span class="svg-icon svg-icon-md">
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <rect x="0" y="0" width="24" height="24"></rect>
                    <path d="M5,8.6862915 L5,5 L8.6862915,5 L11.5857864,2.10050506 L14.4852814,5 L19,5 L19,9.51471863 L21.4852814,12 L19,14.4852814 L19,19 L14.4852814,19 L11.5857864,21.8994949 L8.6862915,19 L5,19 L5,15.3137085 L1.6862915,12 L5,8.6862915 Z M12,15 C13.6568542,15 15,13.6568542 15,12 C15,10.3431458 13.6568542,9 12,9 C10.3431458,9 9,10.3431458 9,12 C9,13.6568542 10.3431458,15 12,15 Z" fill="#000000"></path>
                </g>
            </svg>
        </span>
    </a>
    <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right" style="display: none;">
        <ul class="navi flex-column navi-hover py-2">
            <li class="navi-item">
                <a href="{{ route('article.edit',$article->id) }}" class="navi-link">
                    <span class="navi-icon">
                        <i class="fa fa-pencil-alt"></i>
                    </span>
                    <span class="navi-text">Sửa</span>
                </a>
            </li>
            <li class="navi-item">
                <a href="#" class="navi-link" onclick="document.querySelector('.article-{{ $article->id }}').submit()">
                    <span class="navi-icon">
                        <i class="la la-print"></i>
                    </span>
                    <span class="navi-text">{{ $article->status == 1 ? 'Gỡ xuống' : 'xuất bản' }}</span>
                </a>
                <form method="post" class="article-{{ $article->id }}" action="{{ route('article.update_status',$article->id) }}">@csrf</form>
            </li>
            <li class="navi-item delete-article">
                <a href="#" class="navi-link">
                    <span class="navi-icon">
                        <i class="fa fa-times"></i>
                    </span>
                    <span class="navi-text">Xoá</span>
                    <form action="{{ route('article.destroy', $article->id) }}" method="post">@csrf @method('delete')</form>
                </a>
            </li>
        </ul>
    </div>
</div>
