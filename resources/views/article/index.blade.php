@extends('layouts.master')

@section('article-parent','menu-item-open menu-item-here')
@section('article','menu-item-active')

@section('breadcrumb')

@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="d-flex justify-content-end mb-5">
                <a href="{{ route('article.create')}}" class="btn btn-primary font-weight-bolder">
                    <i class="fa fa-plus"></i>
                    Tạo mới
                </a>
            </div>
            <table class="table table-bordered" id="table-article"></table>
        </div>
    </div>
@stop

@push('page-js')
    <script>
        $(document).ready(function () {
            $('#table-article').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    method: 'GET',
                    url: '',
                    headers: {'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')}
                },
                drawCallback: function (settings) {
                    const scrollBody = $(settings.nScrollBody);
                    scrollBody.attr('style', 'min-height: 180px;' + scrollBody.attr('style'));
                },
                scrollX: true,
                columns: [
                    {
                        title: 'Tiêu đề',
                        data: 'title',
                        name: 'title',
                        orderable: true,
                        searchable: true
                    },
                    {
                        title: 'Ảnh đại diện',
                        data: 'thumbnail',
                        name: 'thumbnail',
                        class: 'text-center',
                        orderable: true,
                        searchable: true
                    },
                    {
                        title: 'Trạng thái',
                        data: 'status',
                        name: 'status',
                        class: 'text-center',
                        orderable: true,
                        searchable: true
                    },
                    {
                        title: 'Actions',
                        data: 'actions',
                        name: 'actions',
                        class: 'text-center',
                        orderable: false,
                        searchable: false,
                        width: 100
                    },
                ],
            });
        });

        $(document).on('click', '.delete-article', function (event) {
            event.preventDefault();
            Swal.fire({
                title: "Xoá",
                text: 'Bạn có muốn xoá bài viết này ?',
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "Xoá"
            }).then(function (result) {
                if (result.value) {
                    $(event.target).closest('.delete-article').find('form').submit()
                }
            });
        });
    </script>
@endpush
