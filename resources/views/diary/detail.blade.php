@extends('layouts.master')

@section('diary','menu-item-active')

@section('breadcrumb')

@endsection

@section('content')
    <div class="d-flex flex-row">
        <!--begin::Aside-->
        <div class="flex-row-auto offcanvas-mobile w-550px w-xl-600px offcanvas-mobile-on" id="kt_chat_aside">
            <!--begin::Card-->
            <div class="card card-custom">
                <!--begin::Body-->
                <div class="card-body">
                    <h3>{{ $diary->date }}</h3>
                    <!--begin:Users-->
                    <div class="mt-7 scroll scroll-pull ps ps--active-y" style="height: 40px; overflow: hidden;">
                        <!--begin:User-->
                        <div class="d-flex align-items-center justify-content-between mb-5">
                            <p class="font-size-h5">
                                {!! $diary->note !!}
                            </p>
                        </div>
                        <div>
                            <ul id="images">
                                @if($diary->images)
                                @foreach($diary->images as $image)
                                    <div class="symbol symbol-50 symbol-lg-120" data-src="{{ $image }}">
                                        <img alt="Pic" src="{{ $image }}" data-src="{{ $image }}">
                                    </div>
                                @endforeach
                                @endif
                            </ul>
                        </div>
                        <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; height: 40px; right: -2px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 40px;"></div></div></div>
                    <!--end:Users-->
                </div>
                <!--end::Body-->
            </div>
            <!--end::Card-->
        </div><div class="offcanvas-mobile-overlay"></div>
        <!--end::Aside-->
        <!--begin::Content-->
        <div class="flex-row-fluid ml-lg-8" id="kt_chat_content">
            <!--begin::Card-->
            <div class="card card-custom">
                <!--begin::Header-->
                <div class="card-header align-items-center px-4 py-3">
                    <div class="text-left flex-grow-1">
                    </div>
                    <div class="text-center flex-grow-1">
                        <div class="text-dark-75 font-weight-bold font-size-h5">{{ $diary->customer->name }}</div>
                    </div>
                    <div class="text-right flex-grow-1">
                    </div>
                </div>
                <!--end::Header-->
                <!--begin::Body-->
                <div class="card-body">
                    <!--begin::Scroll-->
                    <div class="scroll scroll-pull ps ps--active-y" style="height: 451px; overflow: hidden;" id="scroll">
                        <!--begin::Messages-->
                        <div class="messages" id="message_container">
                            @foreach($diary->replies as $reply)
                                @if($reply->customer !== null)
                                    <div class="d-flex flex-column mb-5 align-items-start">
                                        <div class="d-flex align-items-center">
                                            <div class="symbol symbol-circle symbol-40 mr-3">
                                                <img alt="Pic" src="{{ asset('vendor/media/users/300_12.jpg') }}">
                                            </div>
                                            <div>
                                                <a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">{{ $reply->customer->name }}</a>
                                                <span class="text-muted font-size-sm">{{ $reply->created_at->diffForHumans() }}</span>
                                            </div>
                                        </div>
                                        <div class="mt-2 rounded p-5 bg-light-success text-dark-50 font-weight-bold font-size-lg text-left max-w-400px">{{ $reply->reply }}</div>
                                    </div>
                                @else
                                    <div class="d-flex flex-column mb-5 align-items-end">
                                        <div class="d-flex align-items-center">
                                            <div>
                                                <span class="text-muted font-size-sm">{{ $reply->created_at->diffForHumans() }}</span>
                                                <a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">Bạn</a>
                                            </div>
                                            <div class="symbol symbol-circle symbol-40 ml-3">
                                                <img alt="Pic" src="{{ asset('vendor/media/users/300_21.jpg') }}">
                                            </div>
                                        </div>
                                        <div class="mt-2 rounded p-5 bg-light-primary text-dark-50 font-weight-bold font-size-lg text-right max-w-400px">{{ $reply->reply }}</div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                        <!--end::Messages-->
                        <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; height: 451px; right: -2px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 196px;"></div></div></div>
                        <!--end::Scroll-->
                </div>
                <!--end::Body-->
                <!--begin::Footer-->
                <div class="card-footer align-items-center">
                    <!--begin::Compose-->
                    <textarea class="form-control border-0 p-0" rows="5" placeholder="Nhập tin nhắn" id="message-input"></textarea>

                    <div class="d-flex align-items-center justify-content-between mt-5">
                        <div class="mr-3">
                        </div>
                        <div>
                            <button
                                type="button"
                                id="btn-send"
                                class="btn btn-primary btn-md text-uppercase font-weight-bold chat-send py-2 px-6"
                                data-url="{{ route('diary.reply',$diary->id) }}">Gửi</button>
                        </div>
                    </div>
                    <!--begin::Compose-->
                </div>
                <!--end::Footer-->
            </div>
            <!--end::Card-->
        </div>
        <!--end::Content-->
    </div>
@stop

@push('plugin-css')
    <link href="{{ asset('vendor/libraries/viewerjs/dist/viewer.min.css') }}" rel="stylesheet" type="text/css"/>
@endpush
@push('plugin-js')
    <script src="{{ asset('vendor/libraries/viewerjs/dist/viewer.min.js') }}"></script>
@endpush
@push('page-js')
    <script src="{{ asset('vendor/js/diary.js') }}"></script>
@endpush
