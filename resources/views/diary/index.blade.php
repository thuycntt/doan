@extends('layouts.master')

@section('diary','menu-item-active')

@section('breadcrumb')

@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <table class="table table-bordered" id="table-diary"></table>
        </div>
    </div>
@stop

@push('page-js')
    <script>
        $(document).ready(function () {
            $('#table-diary').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    method: 'GET',
                    url: '',
                    headers: {'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')}
                },
                drawCallback: function (settings) {
                    const scrollBody = $(settings.nScrollBody);
                    scrollBody.attr('style', 'min-height: 180px;' + scrollBody.attr('style'));
                },
                scrollX: true,
                columns: [
                    {
                        title: 'Khách hàng',
                        data: 'customer',
                        name: 'customer',
                        class: 'text-center',
                        orderable: true,
                        searchable: true
                    },
                    {
                        title: 'Xem',
                        data: 'seen',
                        name: 'seen',
                        class: 'text-center',
                        orderable: true,
                        searchable: true
                    },
                    {
                        title: 'Trạng thái',
                        data: 'status',
                        name: 'status',
                        class: 'text-center',
                        orderable: true,
                        searchable: true
                    },
                    {
                        title: 'Ngày ghi',
                        data: 'date',
                        name: 'date',
                        class: 'text-center',
                        orderable: true,
                        searchable: true
                    },
                    {
                        title: '!!!',
                        data: 'actions',
                        name: 'actions',
                        class: 'text-center',
                        orderable: true,
                        searchable: true,
                        width: 100
                    },
                ],
            });
            $(document).on('click', '.close-diary', function (event) {
                event.preventDefault();
                Swal.fire({
                    title: "Xác nhận đóng",
                    text: 'Bạn có muốn đóng nhật ký này ?',
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Đóng"
                }).then(function (result) {
                    if (result.value) {
                        $(event.target).closest('.close-diary').find('form').submit();
                    }
                });
            });

            $(document).on('click', '.open-diary', function (event) {
                event.preventDefault();
                Swal.fire({
                    title: "Xác nhận mở",
                    text: 'Bạn có muốn mở nhật ký này ?',
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Mở"
                }).then(function (result) {
                    if (result.value) {
                        $(event.target).closest('.open-diary').find('form').submit();
                    }
                });
            });
        });
    </script>
@endpush
