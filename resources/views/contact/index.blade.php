@extends('layouts.master')

@section('contact','menu-item-active')

@section('content')
    <div class="card">
        <div class="card-body">
            <table class="table table-bordered" id="table-contact"></table>
        </div>
        <div class="modal fade" id="modal-reply" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Phản hồi liên hệ</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <i aria-hidden="true" class="ki ki-close"></i>
                        </button>
                    </div>
                    <form action="{{ route('contact.reply') }}" method="post">
                    <div class="modal-body">
                            @csrf
                            <div class="form-group">
                                <input type="hidden" id="contact-id" name="id">
                                <textarea class="form-control" placeholder="Nhập phản hồi" name="content" rows="3" required></textarea>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Đóng</button>
                        <button type="submit" class="btn btn-primary font-weight-bold">Lưu</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop

@push('page-js')
    <script>
        $(document).ready(function () {
            $('#table-contact').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    method: 'GET',
                    url: '',
                    headers: {'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')}
                },
                drawCallback: function (settings) {
                    const scrollBody = $(settings.nScrollBody);
                    scrollBody.attr('style', 'min-height: 180px;' + scrollBody.attr('style'));
                },
                scrollX: true,
                columns: [
                    {
                        title: 'Nội dung',
                        data: 'content',
                        name: 'content',
                        orderable: true,
                        searchable: true
                    },
                    {
                        title: 'Người gửi',
                        data: 'full_name',
                        name: 'full_name',
                        orderable: true,
                        searchable: true
                    },
                    {
                        title: 'Trạng thái',
                        data: 'status',
                        name: 'status',
                        class: 'text-center',
                        orderable: true,
                        searchable: true
                    },
                    {
                        title: 'Phản hồi',
                        data: 'user',
                        name: 'user',
                        class: 'text-center',
                        orderable: true,
                        searchable: true
                    },
                    {
                        title: 'Ngày gửi',
                        data: 'created_at',
                        name: 'created_at',
                        class: 'text-center',
                        orderable: true,
                        searchable: true
                    },
                    {
                        title: '!!!',
                        data: 'actions',
                        name: 'actions',
                        class: 'text-center',
                    }
                ],
            });

            $('#modal-reply').on('show.bs.modal',event=>{
                let id = $(event.relatedTarget).data('id');
                $('#modal-reply').find('input[name=id]').val(id)
            })
        });
    </script>
@endpush
