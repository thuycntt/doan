@extends('layouts.master')

@section('article-parent','menu-item-open menu-item-here')
@section('tag','menu-item-active')
@section('breadcrumb')

@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <form action="{{ route('tag.store') }}" method="post">
                @csrf
                <div class="form-group">
                    <label for="name">Tên</label>
                    <input type="text" class="form-control" placeholder="Tên" name="name" id="name">
                    @error('name')
                    <div class="fv-plugins-message-container"><div class="fv-help-block">{{ $message }}</div></div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="description">Mô tả</label>
                    <textarea class="form-control" placeholder="Mô tả" rows="5" name="description" id="description"></textarea>
                </div>
                <div class="card-footer d-flex justify-content-end">
                    <button type="submit" class="btn btn-primary mr-2"><i class="fa fa-save"></i>Lưu</button>
                </div>
            </form>
        </div>
    </div>
@stop
