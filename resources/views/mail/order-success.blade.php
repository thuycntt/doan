<!doctype html>
<html>
<head>
    <meta name="viewport" content="width=device-width"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <style>
        /* -------------------------------------
            GLOBAL RESETS
        ------------------------------------- */
        img {
            border: none;
            -ms-interpolation-mode: bicubic;
            max-width: 100%;
        }

        body {
            background-color: #f6f6f6;
            font-family: sans-serif;
            -webkit-font-smoothing: antialiased;
            font-size: 14px;
            line-height: 1.4;
            margin: 0;
            padding: 0;
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }

        table {
            border-collapse: separate;
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
            width: 100%;
        }

        table td {
            font-family: sans-serif;
            font-size: 14px;
            vertical-align: top;
        }

        /* -------------------------------------
            BODY & CONTAINER
        ------------------------------------- */

        .body {
            background-color: #f6f6f6;
            width: 100%;
        }

        /* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
        .container {
            display: block;
            margin: 0 auto !important;
            /* makes it centered */
            max-width: 580px;
            padding: 10px;
            width: 580px;
        }

        /* This should also be a block element, so that it will fill 100% of the .container */
        .content {
            box-sizing: border-box;
            display: block;
            margin: 0 auto;
            max-width: 580px;
            padding: 10px;
        }

        /* -------------------------------------
            HEADER, FOOTER, MAIN
        ------------------------------------- */
        .main {
            background: #ffffff;
            border-radius: 3px;
            width: 100%;
        }

        .wrapper {
            box-sizing: border-box;
            padding: 20px;
        }

        .content-block {
            padding-bottom: 10px;
            padding-top: 10px;
        }

        .footer {
            clear: both;
            margin-top: 10px;
            text-align: center;
            width: 100%;
        }

        .footer td,
        .footer p,
        .footer span,
        .footer a {
            color: #999999;
            font-size: 12px;
            text-align: center;
        }

        /* -------------------------------------
            TYPOGRAPHY
        ------------------------------------- */
        h1,
        h2,
        h3,
        h4 {
            color: #000000;
            font-family: sans-serif;
            font-weight: 400;
            line-height: 1.4;
            margin: 0;
            margin-bottom: 30px;
        }

        h1 {
            font-size: 35px;
            font-weight: 300;
            text-align: center;
            text-transform: capitalize;
        }

        p,
        ul,
        ol {
            font-family: sans-serif;
            font-size: 14px;
            font-weight: normal;
            margin: 0;
            margin-bottom: 15px;
        }

        p li,
        ul li,
        ol li {
            list-style-position: inside;
            margin-left: 5px;
        }

        a {
            color: #3498db;
            text-decoration: underline;
        }

        /* -------------------------------------
            BUTTONS
        ------------------------------------- */
        .btn {
            box-sizing: border-box;
            width: 100%;
        }

        .btn > tbody > tr > td {
            padding-bottom: 15px;
        }

        .btn table {
            width: auto;
        }

        .btn table td {
            background-color: #ffffff;
            border-radius: 5px;
            text-align: center;
        }

        .btn a {
            background-color: #ffffff;
            border: solid 1px #3498db;
            border-radius: 5px;
            box-sizing: border-box;
            color: #3498db;
            cursor: pointer;
            display: inline-block;
            font-size: 14px;
            font-weight: bold;
            margin: 0;
            padding: 12px 25px;
            text-decoration: none;
            text-transform: capitalize;
        }

        .btn-primary table td {
            background-color: #3498db;
        }

        .btn-primary a {
            background-color: #3498db;
            border-color: #3498db;
            color: #ffffff;
        }

        /* -------------------------------------
            OTHER STYLES THAT MIGHT BE USEFUL
        ------------------------------------- */
        .last {
            margin-bottom: 0;
        }

        .first {
            margin-top: 0;
        }

        .align-center {
            text-align: center;
        }

        .align-right {
            text-align: right;
        }

        .align-left {
            text-align: left;
        }

        .clear {
            clear: both;
        }

        .mt0 {
            margin-top: 0;
        }

        .mb0 {
            margin-bottom: 0;
        }

        .preheader {
            color: transparent;
            display: none;
            height: 0;
            max-height: 0;
            max-width: 0;
            opacity: 0;
            overflow: hidden;
            mso-hide: all;
            visibility: hidden;
            width: 0;
        }

        .powered-by a {
            text-decoration: none;
        }

        hr {
            border: 0;
            border-bottom: 1px solid #f6f6f6;
            Margin: 20px 0;
        }

        /* -------------------------------------
            RESPONSIVE AND MOBILE FRIENDLY STYLES
        ------------------------------------- */
        @media only screen and (max-width: 620px) {
            table[class=body] h1 {
                font-size: 28px !important;
                margin-bottom: 10px !important;
            }

            table[class=body] p,
            table[class=body] ul,
            table[class=body] ol,
            table[class=body] td,
            table[class=body] span,
            table[class=body] a {
                font-size: 16px !important;
            }

            table[class=body] .wrapper,
            table[class=body] .article {
                padding: 10px !important;
            }

            table[class=body] .content {
                padding: 0 !important;
            }

            table[class=body] .container {
                padding: 0 !important;
                width: 100% !important;
            }

            table[class=body] .main {
                border-left-width: 0 !important;
                border-radius: 0 !important;
                border-right-width: 0 !important;
            }

            table[class=body] .btn table {
                width: 100% !important;
            }

            table[class=body] .btn a {
                width: 100% !important;
            }

            table[class=body] .img-responsive {
                height: auto !important;
                max-width: 100% !important;
                width: auto !important;
            }
        }

        /* -------------------------------------
            PRESERVE THESE STYLES IN THE HEAD
        ------------------------------------- */
        @media all {
            .ExternalClass {
                width: 100%;
            }

            .ExternalClass,
            .ExternalClass p,
            .ExternalClass span,
            .ExternalClass font,
            .ExternalClass td,
            .ExternalClass div {
                line-height: 100%;
            }

            .apple-link a {
                color: inherit !important;
                font-family: inherit !important;
                font-size: inherit !important;
                font-weight: inherit !important;
                line-height: inherit !important;
                text-decoration: none !important;
            }

            .btn-primary table td:hover {
                background-color: #34495e !important;
            }

            .btn-primary a:hover {
                background-color: #34495e !important;
                border-color: #34495e !important;
            }
        }

        .verify-code {
            padding: 10px;
            background-color: #eee;
            font-weight: bold;
            font-size: 18px;
        }
    </style>
    <style>
        .title {
            text-align: center;
            display: block;
        }

        .container {
            width: 1170px;
            margin: 0 auto;
        }

        .justify-content-center {
            -webkit-box-pack: center !important;
            -ms-flex-pack: center !important;
            justify-content: center !important;
        }

        .align-items-center {
            -webkit-box-align: center !important;
            -ms-flex-align: center !important;
            align-items: center !important;
        }

        .d-flex {
            display: -webkit-box !important;
            display: -ms-flexbox !important;
            display: flex !important;
        }

        .col-1 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 8.33333%;
            flex: 0 0 8.33333%;
            max-width: 8.33333%;
        }

        .col-2 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 16.66667%;
            flex: 0 0 16.66667%;
            max-width: 16.66667%;
        }

        .col-3 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 25%;
            flex: 0 0 25%;
            max-width: 25%;
        }

        .col-4 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 33.33333%;
            flex: 0 0 33.33333%;
            max-width: 33.33333%;
        }

        .col-5 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 41.66667%;
            flex: 0 0 41.66667%;
            max-width: 41.66667%;
        }

        .col-6, .lightGallery .image-tile {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 50%;
            flex: 0 0 50%;
            max-width: 50%;
        }

        .col-7 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 58.33333%;
            flex: 0 0 58.33333%;
            max-width: 58.33333%;
        }

        .col-8 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 66.66667%;
            flex: 0 0 66.66667%;
            max-width: 66.66667%;
        }

        .col-9 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 75%;
            flex: 0 0 75%;
            max-width: 75%;
        }

        .col-10 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 83.33333%;
            flex: 0 0 83.33333%;
            max-width: 83.33333%;
        }

        .col-11 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 91.66667%;
            flex: 0 0 91.66667%;
            max-width: 91.66667%;
        }

        .col-12 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 100%;
            flex: 0 0 100%;
            max-width: 100%;
        }


        .row {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            margin-right: -12.5px;
            margin-left: -12.5px;
        }

        .table, .jsgrid .jsgrid-table {
            width: 100%;
            margin-bottom: 1rem;
            color: #212529;
        }

        .table th, .jsgrid .jsgrid-table th,
        .table td,
        .jsgrid .jsgrid-table td {
            padding: 12px 15px;
            vertical-align: top;
            border-top: 1px solid #dee2e6;
        }

        .table thead th, .jsgrid .jsgrid-table thead th {
            vertical-align: bottom;
            border-bottom: 2px solid #dee2e6;
        }

        .table tbody + tbody, .jsgrid .jsgrid-table tbody + tbody {
            border-top: 2px solid #dee2e6;
        }

        .table-bordered {
            border: 1px solid #dee2e6;
            border-collapse: collapse;
        }

        .table-bordered th,
        .table-bordered td {
            border: 1px solid #dee2e6;
        }

        .table-bordered thead th,
        .table-bordered thead td {
            border-bottom-width: 2px;
        }

        .table-striped tbody tr:nth-of-type(odd) {
            background-color: rgba(0, 0, 0, 0.05);
        }

        .list-group {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            padding-left: 0;
            margin-bottom: 0;
        }

        .list-group-item {
            position: relative;
            display: block;
            padding: 0.75rem 1.25rem;
            margin-bottom: -1px;
            background-color: #fff;
            border: 1px solid rgba(0, 0, 0, 0.125);
        }

        .list-group-item:first-child {
            border-top-left-radius: 0.25rem;
            border-top-right-radius: 0.25rem;
        }

        .list-group-item:last-child {
            margin-bottom: 0;
            border-bottom-right-radius: 0.25rem;
            border-bottom-left-radius: 0.25rem;
        }

        .list-group-item.disabled, .list-group-item:disabled {
            color: #6c757d;
            pointer-events: none;
            background-color: #fff;
        }

        .list-group-item.active {
            z-index: 2;
            color: #fff;
            background-color: #007bff;
            border-color: #007bff;
        }
    </style>
</head>
<body class="">
<table border="0" cellpadding="0" cellspacing="0" class="body"  style="background-color: white; padding: 10px">
    <tr>
        <td>&nbsp;</td>
        <td class="container">
            <div class="content">
                <div style="font-family: Arial,Helvetica Neue,Helvetica,sans-serif;line-height: 14pt;padding: 20px 0px;font-size: 14px;max-width: 580px;margin: 0 auto;"><div class="adM">
                    </div><div style="padding:0 10px;margin-bottom:25px"><div class="adM">
                        </div><p>Xin chào {{ $customer->name }}</p>
                        <p>Cảm ơn Bạn đã đặt hàng tại <strong>YếnVN</strong>!</p>
                        <p>Đơn hàng của Bạn đã được tiếp nhận. Nhân viên xử lý đơn hàng của <strong>YếnVN</strong> sẽ gọi từ số 02477702777 hoặc 02436627666 trong vòng 12h  để xác nhận đơn hàng. Phiền bạn vui lòng chú ý điện thoại trong khoảng thời gian nói trên
                        </p><p>Dưới đây là đơn đặt hàng của bạn</p>
                        <p></p></div>
                    <hr>
                    <div style="padding:0 10px">
                        <table style="width:100%;border-collapse:collapse;margin-top:20px">
                            <thead>
                            <tr>
                                <th style="text-align:left;width:50%;font-size:medium;padding:5px 0"><strong>Thông tin khách hàng</strong></th>
                                <th style="text-align:left;width:50%;font-size:medium;padding:5px 0"><strong>Địa chỉ nhận hàng</strong></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td style="padding-right:15px">
                                    <table style="width:100%">
                                        <tbody>

                                        <tr>
                                            <td>{{ $customer->name }}</td>
                                        </tr>

                                        <tr>
                                            <td>{{ $customer->phone }}</td>
                                        </tr>

                                        <tr>
                                            <td style="word-break:break-word;word-wrap:break-word"><a href="mailto:{{ $customer->email }}" target="_blank">{{ $customer->email }}</a></td>
                                        </tr>


                                        <tr>
                                            <td style="word-break:break-word;word-wrap:break-word">
                                                {{ $customer->address }}
                                            </td>
                                        </tr>

                                        </tbody>
                                    </table>
                                </td>
                                <td>
                                    <table style="width:100%">
                                        <tbody>

                                        <tr>
                                            <td>{{ $order->full_name }}</td>
                                        </tr>

                                        <tr>
                                            <td>{{ $order->phone }}</td>
                                        </tr>


                                        <tr>
                                            <td style="word-break:break-word;word-wrap:break-word">
                                                {{ $order->address }}
                                            </td>
                                        </tr>

                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <table style="width:100%;border-collapse:collapse;margin-top:20px">
                            <thead>
                            <tr>
                                <th style="text-align:left;width:50%;font-size:medium;padding:5px 0">Hình thức thanh toán:</th>
                                <th style="text-align:left;width:50%;font-size:medium;padding:5px 0">Hình thức vận chuyển:</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td style="padding-right:15px">{{ $transaction->type === \App\Models\Transaction::MOMO ? 'Đã thanh toán bằng ví Momo' : 'Thanh toán khi giao hàng (COD)' }}</td>
                                <td>SHIP TẬN NƠI<br></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div style="margin-top:20px;padding:0 10px">
                        <div style="padding-top:10px;font-size:medium"><strong>Thông tin đơn hàng</strong></div>
                        <table style="width:100%;margin:10px 0">
                            <tbody><tr>
                                <td style="width:50%;padding-right:15px">Mã đơn hàng: {{ $order->uuid }}</td>
                                <td style="width:50%">
                                    <p>Ngày đặt hàng: {{ $order->created_at->format('d/m/Y') }}</p>
                                    <p>Ngày giao hàng dự kiến: {{ Carbon\Carbon::parse($order->delivery_date)->format('d/m/Y') }}</p>
                                </td>
                            </tr>
                            </tbody></table>
                        <ul style="padding-left:0;list-style-type:none;margin-bottom:0">
                            @foreach($products as $product)
                            <li>
                                <table style="width:100%;border-bottom:1px solid #e4e9eb">
                                    <tbody>
                                    <tr>
                                        <td style="width:100%;padding:25px 10px 0px 0" colspan="2">
                                            <div style="float:left;width:80px;height:80px;border:1px solid #ebeff2;overflow:hidden">
                                                <img style="max-width:100%;max-height:100%" src="{{ $product['thumbnail'] }}">
                                            </div>
                                            <div style="margin-left:100px">
                                                <a href="https://yenvn.info/san-pham-yen/{{ $product['slug'] }}.{{ $product['sku'] }}" style="color:#357ebd;text-decoration:none" target="_blank" >{{ $product['name'] }}</a>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:70%;padding:5px 0px 25px">
                                            <div style="margin-left:100px">
                                                {{ number_format($product['price']-($product['price']*($product['discount']/100))) }} VND <span style="margin-left:20px">x {{ $product['quantity'] }}</span>
                                            </div>
                                        </td>
                                        <td style="text-align:right;width:30%;padding:5px 0px 25px">{{ number_format(($product['price']-($product['price']*($product['discount']/100))) * (int)$product['quantity']) }} VND</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </li>
                            @endforeach
                        </ul>
                        <table style="width:100%;border-collapse:collapse;margin-bottom:50px;margin-top:10px">
                            <tbody><tr>
                                <td style="width:20%"></td>
                                <td style="width:80%">
                                    <table style="width:100%;float:right">
                                        <tbody><tr>
                                            <td style="padding-bottom:10px">Giảm giá:</td>
                                            <td style="font-weight:bold;text-align:right;padding-bottom:10px">-0 VND</td>
                                        </tr>
                                        <tr>
                                            <td style="padding-bottom:10px">Phí vận chuyển:</td>
                                            <td style="font-weight:bold;text-align:right;padding-bottom:10px">{{ number_format($shipFee) }} VND</td>
                                        </tr>
                                        <tr style="border-top:1px solid #e5e9ec">
                                            <td style="padding-top:10px">Thành tiền</td>
                                            <td style="font-weight:bold;text-align:right;font-size:16px;padding-top:10px">{{ number_format($total + (int)$shipFee) }} VND</td>
                                        </tr>
                                        </tbody></table>
                                </td>
                            </tr>
                            </tbody></table>
                    </div>
                    <div style="clear:both"></div>

                    <div style="padding:0 10px">
                        <p style="margin:30px 0"><span style="font-weight:bold">Ghi chú:</span> {{ $order->note }}</p>
                    </div>

                    <div style="clear:both"></div>
                    <div style="padding:0 10px">

                        <div style="clear:both"></div>
                        <p style="margin:30px 0">Nếu Bạn có bất kỳ câu hỏi nào, đừng ngần ngại liên hệ với <strong>YếnVN</strong> tại <a href="mailto:support@yenvn.com" style="color:#357ebd" target="_blank">support@<span class="il">yenvn</span>.com.vn </a></p>
                        <p style="text-align:right"><i>Love,</i></p>
                        <p style="text-align:right"><strong>YếnVN</strong></p>
                    </div>
                </div>
            </div>
        </td>
        <td>&nbsp;</td>
    </tr>
</table>
</body>
</html>
