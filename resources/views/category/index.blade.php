@extends('layouts.master')

@section('category','menu-item-active')

@section('breadcrumb')

@endsection

@section('content')
    <div class="row">
        <div class="col-8">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Sắp xếp chuyên mục</h4>
                    <form action="{{ route('category.update-nestable') }}" method="post">
                        @csrf
                        <div class="row">
                            <div class="col-10">
                                <div class="dd" data-url="{{ route('category.get_data_nestable') }}" style="width: 100%;max-width: none" data-delete="{{ route('category.delete') }}"></div>
                            </div>
                            <div class="col-2">
                                <div class="card-footer d-flex justify-content-end">
                                    <button type="submit" class="btn btn-primary mr-2"><i class="fa fa-save"></i>Lưu</button>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="data" id="input-nestable">
                    </form>
                </div>
            </div>
        </div>
        <div class="col-4">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title1">Tạo mới</h4>
                    <form action="{{ route('category.store') }}"
                          data-delete="{{ route('category.delete') }}"
                          data-restore="{{ route('category.restore') }}"
                          data-soft-delete="{{ route('category.soft_delete') }}"
                          method="post" id="form-cat">
                        @csrf
                        <input type="hidden" name="id" id="id">
                        <div class="form-group">
                            <label for="name">Tên</label>
                            <input type="text" class="form-control" placeholder="Tên" name="name" id="name">
                            @error('name')
                            <div class="fv-plugins-message-container"><div class="fv-help-block">{{ $message }}</div></div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="type">Loại</label>
                            <select type="text" class="form-control" name="type" id="type">
                                <option value="1">Thực phẩm yến</option>
                                <option value="2">Thiết bị yến</option>
                            </select>
                            @error('type')
                            <div class="fv-plugins-message-container"><div class="fv-help-block">{{ $message }}</div></div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="description">Mô tả</label>
                            <textarea class="form-control" placeholder="Mô tả" rows="5" name="description" id="description"></textarea>
                        </div>
                        <div class="card-footer d-flex justify-content-end">
                            <button type="submit" class="btn btn-primary mr-2"><i class="fa fa-save"></i>Lưu</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
@push('plugin-css')
    <link href="{{ asset('vendor/libraries/nestable/nestable.css') }}" rel="stylesheet" type="text/css"/>
@endpush
@push('plugin-js')
    <script type="text/javascript" src="{{ asset('vendor/libraries/nestable/jquery.nestable.js') }}"></script>
@endpush
@push('page-js')
    <script src="{{ asset('vendor/js/category.js') }}"></script>
@endpush
