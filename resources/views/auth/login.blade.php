@extends('layouts.master-auth')

@section('content')
    <div class="login login-2 login-signin-on d-flex flex-column flex-lg-row flex-column-fluid bg-white" id="kt_login">
        <!--begin::Aside-->
        <div class="login-aside order-2 order-lg-1 d-flex flex-row-auto position-relative overflow-hidden">
            <!--begin: Aside Container-->
            <div class="d-flex flex-column-fluid flex-column justify-content-between py-9 px-7 py-lg-13 px-lg-35">
                <!--begin::Logo-->
                <a href="#" class="text-center pt-2">
                    <img src="" class="max-h-75px" alt="" />
                </a>
                <!--end::Logo-->
                <!--begin::Aside body-->
                <div class="d-flex flex-column-fluid flex-column flex-center">
                    <!--begin::Signin-->
                    <div class="login-form login-signin py-11">
                        <!--begin::Form-->
                        <form class="form" novalidate="novalidate" id="kt_login_signin_form" action="{{ route('login') }}" method="post">
                            @csrf
                            <!--begin::Title-->
                            <div class="text-center pb-8">
                                <h2 class="font-weight-bolder text-dark font-size-h2 font-size-h1-lg">Đăng nhập</h2>
                            </div>
                            <!--end::Title-->
                            <!--begin::Form group-->
                            <div class="form-group @error('username') fv-plugins-icon-container has-danger @enderror">
                                <label class="font-size-h6 font-weight-bolder text-dark">Tên đăng nhập</label>
                                <input class="form-control form-control-solid h-auto py-7 px-6 rounded-lg @error('username') is-invalid @enderror" type="text" name="username" autocomplete="off" />
                                @error('username')
                                    <div class="fv-plugins-message-container"><div class="fv-help-block">{{ $message }}</div></div>
                                @enderror
                            </div>
                            <!--end::Form group-->
                            <!--begin::Form group-->
                            <div class="form-group @error('password') fv-plugins-icon-container has-danger @enderror">
                                <div class="d-flex justify-content-between mt-n5">
                                    <label class="font-size-h6 font-weight-bolder text-dark pt-5">Mật khẩu</label>
                                    <a href="javascript:;" class="text-primary font-size-h6 font-weight-bolder text-hover-primary pt-5" id="kt_login_forgot"> Quên mật khẩu ? </a>
                                </div>
                                <input class="form-control form-control-solid h-auto py-7 px-6 rounded-lg @error('password') is-invalid @enderror" type="password" name="password" autocomplete="off" />
                                @error('password')
                                <div class="fv-plugins-message-container"><div class="fv-help-block">{{ $message }}</div></div>
                                @enderror
                            </div>
                            <!--end::Form group-->
                            <!--begin::Action-->
                            <div class="text-center pt-2">
                                <button id="kt_login_signin_submit" class="btn btn-dark font-weight-bolder font-size-h6 px-8 py-4 my-3">Đăng nhập</button>
                            </div>
                            <!--end::Action-->
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Signin-->
                </div>
                <!--end::Aside body-->
                <!--begin: Aside footer for desktop-->
                <!--end: Aside footer for desktop-->
            </div>
            <!--end: Aside Container-->
        </div>
        <!--begin::Aside-->
        <!--begin::Content-->
        <div class="content order-1 order-lg-2 d-flex flex-column w-100 pb-0" style="background-color: black!important;padding: 0">
            <!--begin::Title-->
            <!--end::Title-->
            <!--begin::Image-->
            <div class="content-img d-flex flex-row-fluid bgi-no-repeat bgi-position-y-bottom bgi-position-x-center" style="background-image: url({{ asset('vendor/media/login-bg/'.rand(1,6).'.jpg') }})"></div>
            <!--end::Image-->
        </div>
        <!--end::Content-->
    </div>
@stop
