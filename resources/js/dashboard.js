import axios from 'axios';

class Dashboard {
    constructor() {
        this.init();
    }

    init() {
        let arrows = {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        }
        $('#from_date').datepicker({
            format: 'dd/mm/yyyy',
            rtl: KTUtil.isRTL(),
            autoclose: true,
            orientation: "bottom left",
            todayHighlight: true,
            templates: arrows,
            endDate: new Date()
        });
        $('#to_date').datepicker({
            format: 'dd/mm/yyyy',
            rtl: KTUtil.isRTL(),
            autoclose: true,
            orientation: "bottom left",
            todayHighlight: true,
            templates: arrows,
            endDate: new Date()
        });

        let fromDate = $('#from_date').val();
        let toDate = $('#to_date').val();
        let url = $('#main').data('url');
        url = url +`?fromDate=${fromDate}&toDate=${toDate}`
        axios.get(url).then(
            res => {
                this.renderChartOrder7Day(res.data.orders_7_day);
                this.renderChartCustomer7Day(res.data.customers_7_day);
                this.renderChartTransactionRatio(res.data.transaction_ratio);
                this.renderChartRate7Day(res.data.rate_7_day);
            }
        )
    }

    renderChartOrder7Day(data) {
        let options = {
            series: [{
                name: 'Số lượt đặt hàng',
                data: Object.values(data)
            }],
            chart: {
                type: 'bar',
                height: 350,
                toolbar: {
                    show: false
                }
            },
            plotOptions: {
                bar: {
                    horizontal: false,
                    columnWidth: ['30%'],
                    endingShape: 'rounded'
                },
            },
            legend: {
                show: false
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                show: true,
                width: 2,
                colors: ['transparent']
            },
            xaxis: {
                categories: Object.keys(data),
                axisBorder: {
                    show: false,
                },
                axisTicks: {
                    show: false
                },
                labels: {
                    style: {
                        colors: 'gray',
                        fontSize: '12px',
                    }
                }
            },
            yaxis: {
                labels: {
                    style: {
                        colors: 'gray',
                        fontSize: '12px',
                    }
                }
            },
            fill: {
                opacity: 1
            },
            states: {
                normal: {
                    filter: {
                        type: 'none',
                        value: 0
                    }
                },
                hover: {
                    filter: {
                        type: 'none',
                        value: 0
                    }
                },
                active: {
                    allowMultipleDataPointsSelection: false,
                    filter: {
                        type: 'none',
                        value: 0
                    }
                }
            },
            tooltip: {
                style: {
                    fontSize: '12px',
                },
                y: {
                    formatter: function (val) {
                        return val
                    }
                }
            },
            colors: [
                KTAppSettings.colors.theme.base.success
            ],
            grid: {
                borderColor: 'gray-200',
                strokeDashArray: 4,
                yaxis: {
                    lines: {
                        show: true
                    }
                }
            }
        };

        let chart = new ApexCharts(document.getElementById('chart_order_7_day'), options);
        chart.render();
    }

    renderChartCustomer7Day(data) {
        let options = {
            series: [{
                name: 'Số lượt khách đăng ký',
                data: Object.values(data)
            }],
            chart: {
                type: 'bar',
                height: 350,
                toolbar: {
                    show: false
                }
            },
            plotOptions: {
                bar: {
                    horizontal: false,
                    columnWidth: ['30%'],
                    endingShape: 'rounded'
                },
            },
            legend: {
                show: false
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                show: true,
                width: 2,
                colors: ['transparent']
            },
            xaxis: {
                categories: Object.keys(data),
                axisBorder: {
                    show: false,
                },
                axisTicks: {
                    show: false
                },
                labels: {
                    style: {
                        colors: 'gray',
                        fontSize: '12px',
                    }
                }
            },
            yaxis: {
                labels: {
                    style: {
                        colors: 'gray',
                        fontSize: '12px',
                    }
                }
            },
            fill: {
                opacity: 1
            },
            states: {
                normal: {
                    filter: {
                        type: 'none',
                        value: 0
                    }
                },
                hover: {
                    filter: {
                        type: 'none',
                        value: 0
                    }
                },
                active: {
                    allowMultipleDataPointsSelection: false,
                    filter: {
                        type: 'none',
                        value: 0
                    }
                }
            },
            tooltip: {
                style: {
                    fontSize: '12px',
                },
                y: {
                    formatter: function (val) {
                        return val
                    }
                }
            },
            colors: [
                KTAppSettings.colors.theme.base.success
            ],
            grid: {
                borderColor: 'gray-200',
                strokeDashArray: 4,
                yaxis: {
                    lines: {
                        show: true
                    }
                }
            }
        };

        let chart = new ApexCharts($('#chart_customer_7_day')[0], options);
        chart.render();
    }

    renderChartTransactionRatio(data) {

        let options = {
            series: Object.values(data),
            chart: {
                width: 450,
                height: 350,
                type: 'pie',
            },
            labels: Object.keys(data),
            responsive: [{
                breakpoint: 480,
                options: {
                    chart: {
                        width: 300
                    },
                    legend: {
                        position: 'bottom'
                    }
                }
            }],
            colors: [
                KTAppSettings.colors.theme.base.danger,
                KTAppSettings.colors.theme.base.info,
                KTAppSettings.colors.theme.base.primary,
                KTAppSettings.colors.theme.base.success,
                KTAppSettings.colors.theme.base.warning
            ]
        };

        (new ApexCharts($('#transaction_ratio')[0], options)).render();
    }

    renderChartRate7Day(data) {
        let options = {
            series: [{
                name: 'Số lượt đánh giá',
                data: Object.values(data)
            }],
            chart: {
                type: 'bar',
                height: 350,
                toolbar: {
                    show: false
                }
            },
            plotOptions: {
                bar: {
                    horizontal: false,
                    columnWidth: ['30%'],
                    endingShape: 'rounded'
                },
            },
            legend: {
                show: false
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                show: true,
                width: 2,
                colors: ['transparent']
            },
            xaxis: {
                categories: Object.keys(data),
                axisBorder: {
                    show: false,
                },
                axisTicks: {
                    show: false
                },
                labels: {
                    style: {
                        colors: 'gray',
                        fontSize: '12px',
                    }
                }
            },
            yaxis: {
                labels: {
                    style: {
                        colors: 'gray',
                        fontSize: '12px',
                    }
                }
            },
            fill: {
                opacity: 1
            },
            states: {
                normal: {
                    filter: {
                        type: 'none',
                        value: 0
                    }
                },
                hover: {
                    filter: {
                        type: 'none',
                        value: 0
                    }
                },
                active: {
                    allowMultipleDataPointsSelection: false,
                    filter: {
                        type: 'none',
                        value: 0
                    }
                }
            },
            tooltip: {
                style: {
                    fontSize: '12px',
                },
                y: {
                    formatter: function (val) {
                        return val
                    }
                }
            },
            colors: [
                KTAppSettings.colors.theme.base.success
            ],
            grid: {
                borderColor: 'gray-200',
                strokeDashArray: 4,
                yaxis: {
                    lines: {
                        show: true
                    }
                }
            }
        };

        let chart = new ApexCharts($('#chart_rate_7_day')[0], options);
        chart.render();
    }
}

$(() => {
    new Dashboard();
})
