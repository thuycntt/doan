import axios from 'axios'

class Category {
    constructor() {
        this.$formCat = $('#form-cat')
        this.$dd = $('.dd');
        this.$inputNestable = $('#input-nestable');
        this.dataAfterChnage = null;
        this.init();
    }

    init() {
        axios.get(this.$dd.data('url')).then(
            res => {
                this.$inputNestable.val(JSON.stringify(res.data));
                this.$dd.html(this.generateHtmlNestable(res.data));
                this.$dd.nestable({maxDepth: 1});
                this.$dd.on('change', () => {
                    this.$inputNestable.val(JSON.stringify(this.$dd.nestable('serialize')));
                    console.log(this.$dd.nestable('serialize'));
                });
            }
        )

        $(document).on('click', '.edit-cat', event => {
            event.preventDefault();
            let id = $(event.currentTarget).data('id');
            let name = $(event.currentTarget).data('name');
            let description = $(event.currentTarget).data('des');
            let type = $(event.currentTarget).data('type');
            $('#id').val(id);
            $('#name').val(name);
            $('#description').val(description);
            $('#type').val(type);
            window.scrollTo(0, 0);
        });

        $(document).on('click', '.hide-cat', event => {
            event.preventDefault();
            let id = $(event.currentTarget).data('id');
            Swal.fire({
                title: "Ẩn chuyên mục",
                text: 'Bạn có muốn ẩn chuyên mục này ?',
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "Ẩn"
            }).then(result => {
                if (result.value) {
                    axios.post(this.$formCat.data('soft-delete'), {id}).then(
                        res => {
                            toastr.success('Thành công');
                            setTimeout(()=>{
                                window.location.reload();
                            }, 1000)
                        },
                        err => {
                            toastr.error('Error')
                        }
                    )
                }
            });
        });

        $(document).on('click', '.show-cat', event => {
            event.preventDefault();
            let id = $(event.currentTarget).data('id');
            Swal.fire({
                title: "Hiện chuyên mục",
                text: 'Bạn có muốn hiện chuyên mục này ?',
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "Hiện"
            }).then(result => {
                if (result.value) {
                    axios.post(this.$formCat.data('restore'), {id}).then(
                        res => {
                            toastr.success('Thành công');
                            setTimeout(()=>{
                                window.location.reload();
                            }, 1000)
                        },
                        err => {
                            toastr.error('Error')
                        }
                    )
                }
            });
        })

    }

    generateHtmlNestable(data) {
        let html = '<ol class="dd-list">';
        for (let item of data) {
            html += `
                <li class="dd-item dd3-item" data-id="${item.id}">
                    <div class="dd-handle dd3-handle"></div>
                    <div class="dd3-content">
                        <div style="display: flex;justify-content: left">
                            <span style="font-weight: 600;font-size:15px;flex: 1;" title="${item.type == 1 ? 'Thực phẩm yến' : 'Thiết bị yến'} | ${item.name}">${item.type == 1 ? 'Thực phẩm yến' : 'Thiết bị yến'} | ${item.name.length > 30 ? item.name.slice(0, 30) + '...' : item.name}</span>
                            <div style="margin-right: 100px">
                                ${item.deleted_at ? 'Ẩn' : 'Hiện'}
                            </div>
                            <div class="row">
                                <div class="col-1 edit-cat" data-id="${item.id}" data-name="${item.name}" data-des="${item.description}" data-type="${item.type}"><i class="fa fa-pencil-alt text-info"></i></div>
                                <div class="col-1 ${item.deleted_at ? 'show-cat' : 'hide-cat'}" data-id="${item.id}"><i class="fa ${item.deleted_at ? 'fa-eye' : 'fa-eye-slash'} ${item.deleted_at ? 'text-success' : 'text-warning'}"></i></div>
                            </div>
                        </div>
                    </div>
                </li>
            `;
        }
        html += '</ol>';
        return html;
    }
}

$(() => {
    new Category();
})
