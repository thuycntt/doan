class Product {
    constructor() {
        this.$autoSku = $('#auto_sku');
        this.$description = $('#description');
        this.$btnGallery = $('.btn_gallery');
        this.$category = $('#category');
        this.$sku = $('#sku');
        this.$chooseImagesContainer = $('#choose_images_container');

        this.init();
    }

    init() {

        this.initCKEditor();

        this.initSelectThumb();

        this.initPriceInput();

        this.$autoSku.on('change',() => {
                if (this.$autoSku.prop('checked')) {
                    this.$sku.val('').prop('disabled', true)
                } else {
                    this.$sku.prop('disabled', false)
                }
            })

        $(document).on('click', '.btn_choose_images', event => {
            event.preventDefault();
            let imageBox = $(event.target).parents('.image-box');
            CKFinder.popup({
                chooseFiles: true,
                width: 1000,
                height: 700,
                connector: '/ckfinder/connector',
                onInit: (finder) => {
                    finder.on('files:choose', (evt) => {
                        let file = evt.data.files.first();
                        imageBox.find('input').val(file.getUrl());
                        imageBox.find('img').attr('src', file.getUrl());

                        if (imageBox.data('success') !== true) {
                            imageBox.attr('data-success', 'true');

                            this.$chooseImagesContainer.append(`<div class="image-box" data-success="false" style="width: max-content;display: inline-block;">
                            <input type="hidden" value="/vendor/media/placeholder.png" class="image-data" data-default="/vendor/media/placeholder.png">
                            <div class="preview-image-wrapper ">
                                <img src="/vendor/media/placeholder.png" alt="preview image" class="preview_image" width="150">
                                <a class="btn_remove_choose_image" title="Remove image" href="#">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                            <div class="image-box-actions">
                                <a href="#" class="btn_choose_images">
                                    Chọn hình
                                </a>
                            </div>
                        </div>`);
                        }

                    });

                }
            });
        })

        $(document).on('click', '.btn_remove_choose_image', event => {
            event.preventDefault();
            let imageBox = $(event.target).parents('.image-box');
            imageBox.remove();
            if (this.$chooseImagesContainer.find('.image-box').length === 0) {
                this.$chooseImagesContainer.html(`<div class="image-box" data-success="false" style="width: max-content;display: inline-block;">
                            <input type="hidden" value="/vendor/media/placeholder.png" class="image-data" data-default="/vendor/media/placeholder.png">
                            <div class="preview-image-wrapper ">
                                <img src="/vendor/media/placeholder.png" alt="preview image" class="preview_image" width="150">
                                <a class="btn_remove_choose_image" title="Remove image" href="#">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                            <div class="image-box-actions">
                                <a href="#" class="btn_choose_images">
                                    Chọn hình
                                </a>
                            </div>
                        </div>`);
            }
        })

        $('.submit-form').click(event => {
            let imageBoxs = $('[data-success=true]')
            if (imageBoxs.length === 0) {
                $('#images').val(JSON.stringify([]));
            } else {
                // let arr = imageBoxs.toArray().reduce((current, next) => {
                //     let input = $(next).find('input');
                //     current.push(input.val())
                //     return current;
                // }, []);
                let arr = [];
                for(let i = 0; i<imageBoxs.length; i++){
                    let input = $(imageBoxs[i]).find('input');
                    arr.push(input.val());
                }
                $('#images').val(JSON.stringify(arr));
            }

            $('form').submit();
        })


        $('input[categories]').on('change',event=>{
            let check = $(event.target).prop('checked');
            let type = $(event.target).data('cat');
            if(check == true){
                if(type == 2){
                    $('input[data-cat=1]').prop('disabled',true).parent().addClass('checkbox-disabled');
                }else{
                    $('input[data-cat=2]').prop('disabled',true).parent().addClass('checkbox-disabled');
                }
            }else{
                let count =  $('input[data-cat='+ type +']:checked');
                if (count.length == 0){
                    $('input[categories]').prop('disabled',false).parent().removeClass('checkbox-disabled');
                }
            }
        })

    }

    initCKEditor() {
        if (window.CKEDITOR !== undefined) {
            let config = {
                filebrowserImageBrowseUrl: '/ckfinder/browser',
                filebrowserImageUploadUrl: $('meta[name="csrf-token"]').attr('content'),
                filebrowserWindowWidth: '1200',
                filebrowserWindowHeight: '750',
                height: 500,
                allowedContent: true
            };
            CKEDITOR.replace(this.$description[0], config);
            // $('[data-action=destroy-editor]')
            //     .off('click')
            //     .on('click', event => {
            //         event.preventDefault();
            //         if (CKEDITOR.instances['content']) {
            //             CKEDITOR.instances['content'].destroy();
            //         } else {
            //             CKEDITOR.replace(document.getElementById('content'), config);
            //         }
            //     })
        }
    }

    initSelectThumb() {
        this.$btnGallery.click(event => {
            event.preventDefault();
            CKFinder.popup({
                chooseFiles: true,
                width: 1000,
                height: 700,
                onInit: function (finder) {
                    finder.on('files:choose', function (evt) {
                        let file = evt.data.files.first();
                        $('#thumbnail-box').find('input').val(file.getUrl());
                        $('#thumbnail-box').find('img').attr('src', file.getUrl());
                    });
                }
            });
        })
    }

    // initSelectCategory() {
    //     this.$category.select2({
    //         placeholder: 'Chọn chuyên mục',
    //         ajax: {
    //             url: $('#category').data('url'),
    //             headers: {'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')},
    //             method: 'POST',
    //             dataType: 'json',
    //             delay: 250,
    //             data: function (params) {
    //                 return {
    //                     term: params.term,
    //                     page: params.page,
    //                 };
    //             },
    //             processResults: function (data, params) {
    //                 params.page = params.page || 1;
    //                 return {
    //                     results: $.map(data.data, function (item) {
    //                         return {
    //                             text: item.name,
    //                             id: item.id,
    //                             title: item.name
    //                         }
    //                     }),
    //                     pagination: {
    //                         more: (params.page * 10) < data.total
    //                     }
    //                 };
    //             },
    //             cache: true
    //         },
    //         escapeMarkup: markup => markup
    //     });
    //
    //     if (this.$category.data('data') !== undefined) {
    //         let data = this.$category.data('data');
    //         let newOption = new Option(data.name, data.id, true, false);
    //         this.$category.append(newOption).trigger('change');
    //     }
    // }

    initPriceInput() {
        $('#price').keydown(event => {
            if (event.keyCode >= 48 && event.keyCode <= 57 || event.keyCode === 8) {
                return true;
            } else {
                event.preventDefault();
            }
        });

        $('#price').keyup(event => {
            let x = $(event.target).val().toString().replaceAll('.', '').replaceAll(',', '');
            if (x !== '') {
                x = parseInt(x).toLocaleString('en-US', {style: 'currency', currency: 'VND'}) + '';
                $(event.target).val(x.replace('₫', ''));
            }
        })
    }
}

$(() => {
    new Product();
})
