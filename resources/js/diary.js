let KTAppChat = function () {
    let _chatAsideEl;
    let _chatContentEl;

    let _initAside = function () {
        let cardScrollEl = KTUtil.find(_chatAsideEl, '.scroll');
        let cardBodyEl = KTUtil.find(_chatAsideEl, '.card-body');
        let searchEl = KTUtil.find(_chatAsideEl, '.input-group');

        KTUtil.scrollInit(cardScrollEl, {
            mobileNativeScroll: true,
            desktopNativeScroll: false,
            resetHeightOnDestroy: true,
            handleWindowResize: true,
            rememberPosition: true,
            height: function () {
                let height;

                if (KTUtil.isBreakpointUp('lg')) {
                    height = KTLayoutContent.getHeight();
                } else {
                    height = KTUtil.getViewPort().height;
                }

                if (_chatAsideEl) {
                    height = height - parseInt(KTUtil.css(_chatAsideEl, 'margin-top')) - parseInt(KTUtil.css(_chatAsideEl, 'margin-bottom'));
                    height = height - parseInt(KTUtil.css(_chatAsideEl, 'padding-top')) - parseInt(KTUtil.css(_chatAsideEl, 'padding-bottom'));
                }

                if (cardScrollEl) {
                    height = height - parseInt(KTUtil.css(cardScrollEl, 'margin-top')) - parseInt(KTUtil.css(cardScrollEl, 'margin-bottom'));
                }

                if (cardBodyEl) {
                    height = height - parseInt(KTUtil.css(cardBodyEl, 'padding-top')) - parseInt(KTUtil.css(cardBodyEl, 'padding-bottom'));
                }

                if (searchEl) {
                    height = height - parseInt(KTUtil.css(searchEl, 'height'));
                    height = height - parseInt(KTUtil.css(searchEl, 'margin-top')) - parseInt(KTUtil.css(searchEl, 'margin-bottom'));
                }

                // Remove additional space
                height = height - 2;

                return height;
            }
        });
    }

    return {
        // Public functions
        init: function () {
            // Elements
            _chatAsideEl = KTUtil.getById('kt_chat_aside');
            _chatContentEl = KTUtil.getById('kt_chat_content');

            // Init aside and user list
            _initAside();

            // Init inline chat example
            KTLayoutChat.setup(KTUtil.getById('kt_chat_content'));
        }
    };
}();

import axios from 'axios';

class Diary {
    constructor() {
        this.init();
    }

    init() {
        new Viewer(document.getElementById('images'), {
            url: 'data-src',
        });

        $('#btn-send').click(event => {
            event.preventDefault();
            let message = $('#message-input').val();
            let url = $('#btn-send').data('url');
            if (message !== '') {
                $('#btn-send').prop('disabled',true);
                axios.post(url, {
                    message
                }).then(
                    res => {
                        $('#btn-send').prop('disabled',false);
                        $('#message-input').val('');
                        $('#message_container').append(`
                                    <div class="d-flex flex-column mb-5 align-items-end">
                                        <div class="d-flex align-items-center">
                                            <div>
                                                <span class="text-muted font-size-sm">${res.data.created}</span>
                                                <a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">Bạn</a>
                                            </div>
                                            <div class="symbol symbol-circle symbol-40 ml-3">
                                                <img alt="Pic" src="/vendor/media/users/300_21.jpg">
                                            </div>
                                        </div>
                                        <div class="mt-2 rounded p-5 bg-light-primary text-dark-50 font-weight-bold font-size-lg text-right max-w-400px">${res.data.reply}</div>
                                    </div>`)
                    }
                )
            }
        })
    }
}

$(() => {
    KTAppChat.init();
    new Diary();
})
