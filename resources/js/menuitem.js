import axios from 'axios';

class MenuItem {
    constructor() {
        this.$formCat = $('#form-cat')
        this.$dd = $('.dd');
        this.$inputNestable = $('#input-nestable');

        this.init();
    }

    init() {
        axios.post(this.$dd.data('url')).then(
            res => {
                this.$dd.html(this.generateHtmlNestable(res.data));
                this.$dd.nestable({maxDepth: 2});
                this.$dd.on('change', () => {
                    this.$inputNestable.val(JSON.stringify(this.$dd.nestable('serialize')));
                });
            }
        )

        $(document).on('click', '.edit-menu', event => {
            event.preventDefault();
            let data = $(event.currentTarget).data('data');

            $('#id').val(data.id);
            $('#name').val(data.name);
            $('#url').val(data.url);
            $('#description').val(data.description);
        });

        // $(document).on('click', '.delete-menu', event => {
        //     event.preventDefault();
        //     let id = $(event.currentTarget).data('id');
        //     Swal.fire({
        //         title: "Xoá menu con",
        //         text: 'Bạn có muốn xoá menu con này ?',
        //         icon: "warning",
        //         showCancelButton: true,
        //         confirmButtonText: "Xoá"
        //     }).then(result => {
        //         if (result.value) {
        //             axios.post(this.$formCat.data('delete'), {id}).then(
        //                 res => {
        //                     window.location.reload();
        //                 },
        //                 err => {
        //                     toastr.error('Error')
        //                 }
        //             )
        //         }
        //     });
        // })

        $(document).on('click', '.hide-menu', event => {
            event.preventDefault();
            let id = $(event.currentTarget).data('id');
            Swal.fire({
                title: "Ẩn menu con",
                text: 'Bạn có muốn ẩn menu con này ?',
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "Ẩn"
            }).then(result => {
                if (result.value) {
                    axios.post(this.$formCat.data('soft-delete'), {id}).then(
                        res => {
                            window.location.reload();
                        },
                        err => {
                            toastr.error('Error')
                        }
                    )
                }
            });
        });

        $(document).on('click', '.show-menu', event => {
            event.preventDefault();
            let id = $(event.currentTarget).data('id');
            Swal.fire({
                title: "Hiện menu con",
                text: 'Bạn có muốn hiện menu này ?',
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "Hiện"
            }).then(result => {
                if (result.value) {
                    axios.post(this.$formCat.data('restore'), {id}).then(
                        res => {
                            window.location.reload();
                        },
                        err => {
                            toastr.error('Error')
                        }
                    )
                }
            });
        })
    }

    generateHtmlNestable(data) {
        let html = '<ol class="dd-list">';
        for (let item of data) {
            html += `
                <li class="dd-item dd3-item" data-id="${item.id}">
                    <div class="dd-handle dd3-handle"></div>
                    <div class="dd3-content">
                        <div style="display: flex;justify-content: left">
                            <span style="font-weight: 600;font-size:15px;flex: 1;" title="${item.name}">${item.name.length > 30 ? item.name.slice(0, 30) + '...' : item.name}</span>
                            <div style="margin-right: 100px">
                                ${item.deleted_at ? 'Ẩn' : 'Hiện'}
                            </div>
                            <div class="row">
                                <div class="col-1 edit-menu" data-data='${JSON.stringify(item)}'><i class="fa fa-pencil-alt text-info"></i></div>
<!--                                <div class="col-1 delete-menu" data-id="${item.id}"><i class="fa fa-times text-danger"></i></div>-->
                                <div class="col-1 ${item.deleted_at ? 'show-menu' : 'hide-menu'}" data-id="${item.id}"><i class="fa ${item.deleted_at ? 'fa-eye' : 'fa-eye-slash'} ${item.deleted_at ? 'text-success' : 'text-warning'}"></i></div>
                            </div>
                        </div>
                    </div>
                    ${item.children && item.children.length > 0 ? this.generateHtmlNestable(item.children) : ''}
                </li>
            `;
        }
        html += '</ol>';
        return html;
    }
}

$(() => {
    new MenuItem();
})




