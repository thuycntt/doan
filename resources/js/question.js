import axios from 'axios';

class Question {
    constructor() {
        this.$messageContainer = $('#message_container')
        this.$btnSend = $('#btn-send');
        this.$messageInput = $('#message-input');

        this.init();
    }

    init() {
        let timeScroll = setTimeout(() => {
            let inner = document.getElementById('scroll');
            inner.scrollTop = inner.scrollHeight;
            clearTimeout(timeScroll);
        }, 300);

        this.$btnSend.click(event => {
            event.preventDefault();
            let message = this.$messageInput.val();
            let url = $(event.currentTarget).data('url');
            let user = $(event.currentTarget).data('user')
            if (message !== '') {
                this.$btnSend.prop('disabled',true);
                axios.post(url, {
                    message,
                    user
                }).then(
                    res => {
                        this.$btnSend.prop('disabled',false);
                        this.$messageInput.val('');
                        this.$messageContainer.append(`
                                    <div class="d-flex flex-column mb-5 align-items-end">
                                        <div class="d-flex align-items-center">
                                            <div>
                                                <span class="text-muted font-size-sm">${res.data.created}</span>
                                                <a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">Bạn</a>
                                            </div>
                                            <div class="symbol symbol-circle symbol-40 ml-3">
                                                <img alt="Pic" src="/vendor/media/users/300_21.jpg">
                                            </div>
                                        </div>
                                        <div class="mt-2 rounded p-5 bg-light-primary text-dark-50 font-weight-bold font-size-lg text-right max-w-400px">${res.data.reply}</div>
                                    </div>`)
                    }
                );
            }
        });
    }
}

$(() => {
    new Question();
})
