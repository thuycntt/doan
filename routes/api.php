<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::domain(env('APP_SUB_URL'))->group(function () {
    Route::group([
        'middleware' => ['api']
    ], function () {

        Route::post('login', 'Api\Auth\LoginController@login');
        Route::post('register', 'Api\Auth\RegisterController@register');


        Route::prefix('article')->group(function () {
            Route::get('list-article', 'Api\ArticleController@listArticle');
            Route::get('article-detail', 'Api\ArticleController@articleDetail');
            Route::get('list-tag', 'Api\ArticleController@listTag');
            Route::get('list-article-by-tag', 'Api\ArticleController@listArticleByTag');
            Route::get('list-article-new', 'Api\ArticleController@listNew');
        });

        Route::prefix('product')->group(function () {
            Route::get('list-product', 'Api\ProductController@listProduct');
            Route::get('product-detail', 'Api\ProductController@productDetail');
            Route::post('save-product-rate', 'Api\ProductRateController@saveProductRate');
            Route::get('get-product-rate', 'Api\ProductRateController@getProductRate');
            Route::get('list-product-new', 'Api\ProductController@listProductNew');
            Route::get('list-filter-product', 'Api\ProductController@listFilterProduct');
            Route::get('list-filter-equipment', 'Api\ProductController@listFilterProductEquipment');
            Route::get('list-trending-product', 'Api\ProductController@listTrendingProducts');
            Route::get('list-random-product', 'Api\ProductController@listProductRandom');
            Route::get('list-discount-product', 'Api\ProductController@listProductDiscount');
            Route::get('list-product-by-category', 'Api\ProductController@listProductByCategory');
            Route::get('list-equipment','Api\ProductController@listEquipmentProduct');
        });


        Route::prefix('address')->group(function () {
            Route::get('list-provinces', 'Api\AddressController@listProvinces');
            Route::get('list-districts', 'Api\AddressController@listDistricts');
            Route::get('list-wards', 'Api\AddressController@listWards');
        });

        Route::prefix('slide')->group(function () {
            Route::get('get-slide', 'Api\SlideController@getSlide');
        });

        Route::prefix('menu')->group(function () {
            Route::get('get-menu', 'Api\MenuController@getMenu');
        });

        Route::prefix('category')->group(function () {
            Route::get('get-category', 'Api\CategoryController@getCategory');
        });
        Route::prefix('contact')->group(function () {
            Route::post('create-contact', 'Api\ContactController@createContact');
        });

        Route::prefix('transaction')->group(function () {
            Route::post('create-momo', 'Api\TransactionController@createMomoTransaction')->name('api.transaction.create_momo');
            Route::post('validate-momo', 'Api\TransactionController@validateMomoTransaction')->name('api.transaction.validate_momo');
        });

            Route::group(['middleware' => ['auth:api']], function () {
            Route::post('logout', 'Api\Auth\LoginController@logout');
            Route::post('me', 'Api\Auth\LoginController@me');
            Route::post('refresh', 'Api\Auth\LoginController@refresh');

            Route::prefix('order')->group(function () {
                Route::post('create', 'Api\OrderController@createOrder');
                Route::post('cancel','Api\OrderController@cancelOrder');
                Route::post('validate', 'Api\OrderController@validateOrder');
                Route::get('list-order', 'Api\OrderController@listOrderByCustomer');
                Route::get('list-transaction', 'Api\OrderController@listTransactionByCustomer');
                Route::post('delivery-date','Api\OrderController@deliveryDate');
            });

            Route::prefix('diary')->group(function () {
                Route::post('save-diary', 'Api\DiaryController@saveDiary');
                Route::get('get-diary', 'Api\DiaryController@getDiary');
                Route::post('save-reply', 'Api\DiaryController@saveReply');
            });

            Route::prefix('question')->group(function () {
                Route::post('save-question', 'Api\QuestionController@saveQuestion');
                Route::post('save-reply', 'Api\QuestionController@saveReply');
                Route::get('list-question', 'Api\QuestionController@listQuestion');
                Route::get('list-reply', 'Api\QuestionController@listReply');
            });
            Route::prefix('customer')->group(function () {
                Route::post('customer-update-info', 'Api\CustomerController@updateInfo');
                Route::post('update-password', 'Api\CustomerController@updatePassword');
            });
        });
    });
});
