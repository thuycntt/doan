<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('', '/login');

Route::domain(env('APP_SUB_URL'))->group(function () {
    Route::group(['middleware' => ['web']], function () {
        Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
        Route::post('login', 'Auth\LoginController@login')->name('login');

        Route::middleware('auth:web')->group(function () {
            Route::get('', 'DashboardController@index')->name('dashboard');
            Route::post('logout', 'Auth\LoginController@logout')->name('logout');

            Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
            Route::get('/dashboard/get-data', 'DashboardController@getData')->name('dashboard.get_data');


            Route::prefix('user')->group(function () {
                Route::post('mask-as-read-notify/{id}', 'UserController@markAsReadNotify')->name('user.mask_as_read_notify');
                Route::get('update-password', 'UserController@updatePassword')->name('user.updatePassword');
                Route::post('update-password', 'UserController@postUpdatePassword')->name('user.postUpdatePassword');
                Route::get('update-info', 'UserController@updateInfo')->name('user.updateInfo');
                Route::post('post-update-info', 'UserController@postUpdateInfo')->name('user.postUpdateInfo');
            });


            Route::prefix('tag')->group(function () {
                Route::get('', 'TagController@index')->name('tag.index');
                Route::get('create', 'TagController@create')->name('tag.create');
                Route::post('store', 'TagController@store')->name('tag.store');
                Route::get('edit/{id}', 'TagController@edit')->name('tag.edit');
                Route::post('update/{id}', 'TagController@update')->name('tag.update');
                Route::delete('delete/{idx}', 'TagController@destroy')->name('tag.destroy');
                Route::post('select2', 'TagController@select2')->name('tag.select2');
            });


            Route::prefix('article')->group(function () {
                Route::get('', 'ArticleController@index')->name('article.index');
                Route::get('create', 'ArticleController@create')->name('article.create');
                Route::post('store', 'ArticleController@store')->name('article.store');
                Route::get('edit/{id}', 'ArticleController@edit')->name('article.edit');
                Route::post('update/{id}', 'ArticleController@update')->name('article.update');
                Route::post('update-status/{id}', 'ArticleController@updateStatus')->name('article.update_status');
                Route::delete('delete/{id}', 'ArticleController@destroy')->name('article.destroy');
            });

            Route::prefix('category')->group(function () {
                Route::get('', 'CategoryController@index')->name('category.index');
                Route::get('get-data-nestable', 'CategoryController@getDataNestable')->name('category.get_data_nestable');
                Route::post('store', 'CategoryController@store')->name('category.store');
                Route::post('update-nestable', 'CategoryController@updateNestable')->name('category.update-nestable');
                Route::post('delete', 'CategoryController@delete')->name('category.delete');
                Route::post('soft-delete', 'CategoryController@softDelete')->name('category.soft_delete');
                Route::post('restore', 'CategoryController@restore')->name('category.restore');
                Route::post('select2', 'CategoryController@select2')->name('category.select2');
            });

            Route::prefix('slide')->group(function () {
                Route::get('', 'SlideController@index')->name('slide.index');
                Route::get('create', 'SlideController@create')->name('slide.create');
                Route::post('store', 'SlideController@store')->name('slide.store');
                Route::get('edit/{id}', 'SlideController@edit')->name('slide.edit');
                Route::post('update/{id}', 'SlideController@update')->name('slide.update');
                Route::post('delete/{id}', 'SlideController@delete')->name('slide.destroy');
            });

            Route::prefix('slide-item')->group(function () {
                Route::get('edit/{id}', 'SlideItemController@edit')->name('slide_item.edit');
                Route::post('update/{id}', 'SlideItemController@update')->name('slide_item.update');
                Route::get('/{slide}/create', 'SlideItemController@create')->name('slide_item.create');
                Route::post('/{slide}/store', 'SlideItemController@store')->name('slide_item.store');
                Route::delete('delete/{id}', 'SlideItemController@delete')->name('slide_item.delete');
               // Route::post('update-status/{id}', 'SlideItemController@updateStatus')->name('slide_item.update_status');
                Route::get('{id}', 'SlideItemController@index')->name('slide_item.index');
                Route::post('update-sort','SlideItemController@updateSort')->name('slide_item.update_sort');
            });

            Route::prefix('product')->group(function () {
                Route::get('', 'ProductController@index')->name('product.index');
                Route::get('create', 'ProductController@create')->name('product.create');
                Route::get('edit/{id}', 'ProductController@edit')->name('product.edit');
                Route::post('store', 'ProductController@store')->name('product.store');
                Route::post('update/{id}', 'ProductController@update')->name('product.update');
                Route::delete('delete/{id}', 'ProductController@delete')->name('product.delete');
                Route::post('restore/{id}', 'ProductController@restore')->name('product.restore');
            });

            Route::prefix('product-rate')->group(function () {
                Route::get('', 'ProductRateController@index')->name('product_rate.index');
                Route::post('update-status', 'ProductRateController@updateStatus')->name('product_rate.update_status');
            });

            Route::prefix('order')->group(function () {
                Route::get('', 'OrderController@index')->name('order.index');
                Route::post('update-order-status', 'OrderController@updateOrderStatus')->name('order.update_status');
                Route::get('order-detail/{id}', 'OrderController@orderDetail')->name('order.detail');
                Route::post('update-delivery/{id?}','OrderController@updateDeliveryDate')->name('order.update_delivery');
                Route::get('get-province','OrderController@getProvince')->name('order.get_province');
                Route::get('get-district','OrderController@getDistrict')->name('order.get_district');
                Route::get('get-ward','OrderController@getWard')->name('order.get_ward');
            });

            Route::prefix('transaction')->group(function () {
                Route::get('', 'TransactionController@index')->name('transaction.index');
            });

            Route::prefix('question')->group(function () {
                Route::get('', 'QuestionController@index')->name('question.index');
                Route::get('detail/{id}', 'QuestionController@questionDetail')->name('question.detail');
                Route::post('reply-to-question/{id}', 'QuestionController@replyToQuestion')->name('question.reply_to_question');
                Route::post('update-status/{id}', 'QuestionController@updateStatus')->name('question.update_status');
                Route::post('unset-user/{id}', 'QuestionController@unsetUser')->name('question.unset_user');
            });

            Route::prefix('menu')->group(function () {
                Route::get('', 'MenuController@index')->name('menu.index');
                Route::get('create', 'MenuController@create')->name('menu.create');
                Route::post('store', 'MenuController@store')->name('menu.store');
                Route::get('edit/{id}', 'MenuController@edit')->name('menu.edit');
                Route::post('update/{id}', 'MenuController@update')->name('menu.update');
                Route::delete('delete/{id}', 'MenuController@delete')->name('menu.destroy');
            });


            Route::prefix('menu-item')->group(function () {
                Route::get('{id}', 'MenuItemController@index')->name('menu_item.index');
                Route::post('get-data-nestable/{id}', 'MenuItemController@getDataNestable')->name('menu_item.get_data_nestable');
                Route::post('store-or-update', 'MenuItemController@storeOrUpdate')->name('menu_item.store_or_update');
                Route::post('update-nestable', 'MenuItemController@updateNestable')->name('menu_item.update_nestable');
                Route::post('soft-delete', 'MenuItemController@softDelete')->name('menu_item.soft_delete');
                Route::post('delete', 'MenuItemController@delete')->name('menu_item.delete');
                Route::post('restore', 'MenuItemController@restore')->name('menu_item.restore');
            });

            Route::prefix('customer')->group(function () {
                Route::get('', 'CustomerController@index')->name('customer.index');
            });

            Route::prefix('diary')->group(function () {
                Route::get('', 'DiaryController@index')->name('diary.index');
                Route::get('detail/{id}', 'DiaryController@diaryDetail')->name('diary.detail');
                Route::post('reply-to-diary/{id}', 'DiaryController@replyToDiary')->name('diary.reply');
                Route::post('update', 'DiaryController@updateStatus')->name('diary.update');
            });

            Route::prefix('contact')->group(function () {
                Route::get('', 'ContactController@index')->name('contact.index');
                Route::post('reply', 'ContactController@reply')->name('contact.reply');
            });

            Route::prefix('audit-log')->group(function () {
                Route::get('', 'AuditLogController@index')->name('audit_log.index');
            });

            Route::any('ckfinder/connector', '\CKSource\CKFinderBridge\Controller\CKFinderController@requestAction')
                ->name('ckfinder_connector');

            Route::any('ckfinder/browser', '\CKSource\CKFinderBridge\Controller\CKFinderController@browserAction')
                ->name('ckfinder_browser');
        });
    });
});

