<?php

namespace App\Providers;

use App\Http\Middleware\ForceJsonMiddleware;
use App\Models\Contact;
use App\Services\AdminPageTitle;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('page-title', function () {
            return new AdminPageTitle;
        });

        if (env('ENABLE_HTTPS', false)) {
            \URL::forceScheme('https');
        }

        $router = $this->app['router'];

        $router->pushMiddlewareToGroup('api', ForceJsonMiddleware::class);

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        if(Schema::hasTable('contacts')){
            \View::share('countContact',Contact::where('status',0)->count());
        } else {
            \View::share('countContact',0);
        }
    }
}
