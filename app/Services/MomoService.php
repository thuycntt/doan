<?php


namespace App\Services;


use App\Http\Requests\Api\Momo\CreateMomoTransactionRequest;
use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Http;
use Ramsey\Uuid\Uuid;

class MomoService
{
    private function generateTransactionId()
    {
        $id = \Str::random(16);
        while (Transaction::where('uuid', $id)->first() !== null) {
            $id = \Str::random(16);
        }
        return $id;
    }
    /**
     * @param CreateMomoTransactionRequest $request
     * @return false|mixed
     */
    public function createTransaction(CreateMomoTransactionRequest $request)
    {
        $data = $request->validated();
        $data['expire_at'] = date('Y-m-d H:i:s', Carbon::now()->timestamp + env('MOMO_TRANSACTION_EXPIRE_TIME'));
        try {
            $partnerCode = env('MOMO_PARTNER_CODE');
            $accessKey = env('MOMO_ACCESS_KEY');
            $secretKey = env('MOMO_SECRET_KEY');
            $orderInfo = 'Thanh toan qua vi Momo';
            $orderId = $this->generateTransactionId();
            $returnUrl = $data['redirect_url'];
            $notifyUrl = 'https://momo.vn';
            $extraData = "merchantName=YenVN";
            $endpoint = env('MOMO_ENDPOINT');
            $requestType = "captureMoMoWallet";
            $amount = strval($data['amount'] + $data['ship_fee']);

           // $rawHash = "partnerCode=%s&accessKey=%s&requestId=%s&amount=%s&orderId=%s&orderInfo=%s&returnUrl=%s&notifyUrl=%s&extraData=%s";
            $rawHash = "partnerCode=".$partnerCode."&accessKey=".$accessKey."&requestId=". $data['expire_at']."&amount=". $amount.
            "&orderId=".$orderId."&orderInfo=".$orderInfo."&returnUrl=".$returnUrl."&notifyUrl=". $notifyUrl."&extraData=". $extraData;


//            $rawHash = sprintf($rawHash,
//                $partnerCode,
//                $accessKey,
//                $data['expire_at'],
//                $data['amount'],
//                $orderId,
//                $orderInfo,
//                $returnUrl,
//                $notifyUrl,
//                $extraData
//            );

            $signature = hash_hmac("sha256", $rawHash, $secretKey);
            $dataSend = [
                'partnerCode' => $partnerCode,
                'accessKey' => $accessKey,
                'requestId' => $data['expire_at'],
                'amount' => $amount,
                'orderId' => $orderId,
                'orderInfo' => $orderInfo,
                'returnUrl' => $returnUrl,
                'notifyUrl' => $notifyUrl,
                'extraData' => $extraData,
                'requestType' => $requestType,
                'signature' => $signature
            ];

            $response = Http::post($endpoint, $dataSend);

            if ($response->ok()) {
                $response = @json_decode($response->body(), true);
                $payUrl = isset($response['payUrl']) ? $response['payUrl'] : false;

                $dataInsert = [
                    'uuid' => $orderId,
                    'type' => Transaction::MOMO,
                    'customer_id' => $data['customer_id'],
                    'transaction_info' => $orderInfo,
                    'data' => json_encode(['pay_url' => $payUrl, 'data_get' => $response]),
                    'status' => Transaction::STATUS_PENDING,
                    'amount' => $data['amount']
                ];

                Transaction::create($dataInsert);

                return [
                    'url' => $payUrl,
                    'transaction_uuid' => $orderId
                ];

            }
            return false;
        } catch (\Exception $exception) {
            \Log::error($exception->getMessage());
            return false;
        }
    }

    /**
     * @param FormRequest $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function validateAfterTransaction(FormRequest $request)
    {
        $data = $request->validated();

        $orderId = $data['orderId'];

        $transaction = Transaction::where('uuid', $orderId)->first();

        if (!$transaction || $transaction->status !== Transaction::STATUS_PENDING) {
            return [
                'status' => Transaction::STATUS_FAILED,
                'message' => 'Transaction is invalid',
                'transaction_uuid' => $orderId
            ];
        }

        $rawHash = "partnerCode=%s&accessKey=%s&requestId=%s&amount=%s&orderId=%s&orderInfo=%s&orderType=%s&transId=%s&message=%s&localMessage=%s&responseTime=%s&errorCode=%s&payType=%s&extraData=%s";

        $rawHash = sprintf(
            $rawHash,
            $data['partnerCode'],
            $data['accessKey'],
            $data['requestId'],
            $data['amount'],
            $data['orderId'],
            $data['orderInfo'],
            $data['orderType'],
            $data['transId'],
            $data['message'],
            $data['localMessage'],
            $data['responseTime'],
            $data['errorCode'],
            $data['payType'],
            $data['extraData']
        );

        $partnerSignature = hash_hmac("sha256", $rawHash, env('MOMO_SECRET_KEY'));

        if ($data['signature'] === $partnerSignature) {
            if ($data['errorCode'] == '0') {
                $returnStatus = Transaction::STATUS_SUCCESS;
                $msg = 'Payment success';
                $status = Transaction::STATUS_SUCCESS;
            } else {
                $returnStatus = $data['errorCode'] === '49' ? Transaction::STATUS_CANCEL : Transaction::STATUS_FAILED;
                $msg = $data['message'] . '/' . $data['localMessage'];
                $status = $data['errorCode'] === '49' ? Transaction::STATUS_CANCEL : Transaction::STATUS_FAILED;
            }
        } else {
            $returnStatus = Transaction::STATUS_FAILED;
            $msg = 'Signature is invalid';
            $status = Transaction::STATUS_FAILED;
        }

        $extraData = json_decode($transaction->data, true);
        $extraData['validate_result'] = $request->validated();

        $transaction->update(['status' => $status, 'data' => json_encode($extraData)]);

        return [
            'status' => $returnStatus,
            'message' => $msg,
            'transaction_uuid' => $orderId
        ];
    }


}
