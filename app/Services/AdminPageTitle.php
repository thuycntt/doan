<?php

namespace App\Services;

class AdminPageTitle
{
    /**
     * @var string
     */
    protected $title = '';

    /**
     * @param $title
     */
    public function setPageTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @param $full
     * @return string
     */
    public function getPageTitle($full = true)
    {
        return !empty($this->title) ? ($this->title . ($full ? ' | YếnVN' : '')) : 'YếnVN';
    }
}
