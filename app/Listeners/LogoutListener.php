<?php

namespace App\Listeners;

use App\Models\AuditLog;
use Illuminate\Auth\Events\Logout;

class LogoutListener
{
    /**
     *
     */
    public function handle(Logout $logout)
    {
        try {
            AuditLog::create([
                'user_id' => $logout->user->id,
                'content' => 'Logout of system',
                'ip' => request()->ip(),
                'user_agent' => request()->userAgent()
            ]);
        } catch (\Exception $exception) {
            \Log::error($exception->getMessage());
        }
    }
}
