<?php

namespace App\Listeners;

use App\Models\AuditLog;
use Illuminate\Auth\Events\Login;

class LoginListener
{
    /**
     *
     */
    public function handle(Login $login)
    {
        try {
            AuditLog::create([
                'user_id' => $login->user->id,
                'content' => 'Login to system',
                'ip' => request()->ip(),
                'user_agent' => request()->userAgent()
            ]);
        } catch (\Exception $exception) {
            \Log::error($exception->getMessage());
        }
    }
}
