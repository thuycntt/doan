<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserReplyDiaryMail extends Mailable
{
    use Queueable, SerializesModels;

    public $customer;

    public $diary;

    /**
     * UserReplyDiaryMail constructor.
     * @param $customer
     * @param $diary
     */
    public function __construct($customer, $diary)
    {
        $this->customer = $customer;
        $this->diary = $diary;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.reply-customer-diary',[
            'customer' => $this->customer,
            'diary' => $this->diary
        ])->subject('YếnVN | Nhật ký được phản hồi');
    }
}
