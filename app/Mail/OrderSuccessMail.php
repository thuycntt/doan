<?php

namespace App\Mail;

use App\Models\Customer;
use App\Models\Order;
use App\Models\Transaction;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use function Psy\sh;

class OrderSuccessMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var Order
     */
    public $order;

    /**
     * @var Customer
     */
    public $customer;

    /**
     * @var Transaction
     */
    public $transaction;

    /**
     * @var
     */
    public $shipFee;

    /**
     * OrderSuccessMail constructor.
     * @param $customer
     * @param $order
     * @param $transaction
     * @param $shipFee
     */
    public function __construct($customer, $order, $transaction, $shipFee)
    {
        $this->customer = $customer;
        $this->order = $order;
        $this->transaction = $transaction;
        $this->shipFee = $shipFee;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $total = 0;
        $arr = [];
        $products = $this->order->products()->withPivot('quantity', 'product_price', 'product_discount')->get();
        for ($i = 0; $i < $products->count(); $i++) {
            $total = $total + ($products[$i]->pivot->product_price - $products[$i]->pivot->product_price * ($products[$i]->pivot->product_discount / 100)) * $products[$i]->pivot->quantity;
            $arr[] = [
                'name' => $products[$i]->name,
                'quantity' => $products[$i]->pivot->quantity,
                'price' => $products[$i]->pivot->product_price,
                'discount' => $products[$i]->pivot->product_discount,
                'thumbnail' => asset($products[$i]->thumbnail),
                'slug' => $products[$i]->slug,
                'sku' => $products[$i]->sku
            ];
        }


        return $this->view('mail.order-success', [
            'customer' => $this->customer,
            'order' => $this->order,
            'transaction' => $this->transaction,
            'products' => $arr,
            'total' => $total,
            'shipFee' => $this->shipFee
        ])->subject('[Yến VN] Đặt hàng thành công');
    }
}
