<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ReplyContactMail extends Mailable
{
    use Queueable, SerializesModels;

    public $contact;

    public $content;


    public function __construct($contact, $content)
    {
        $this->contact = $contact;
        $this->content = $content;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.reply-contact', [
            'content' => $this->content,
            'contact' => $this->contact
        ])->subject('YếnVN | Liên hệ được phản hồi');
    }
}
