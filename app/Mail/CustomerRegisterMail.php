<?php

namespace App\Mail;

use App\Models\Customer;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CustomerRegisterMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var Customer
     */
    protected $customer;

    protected $data;

    /**
     * CustomerRegisterMail constructor.
     * @param $customer
     * @param $data
     */
    public function __construct($customer, $data)
    {
        $this->customer = $customer;
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.customer-register', [
            'customer' => $this->customer,
            'data' => $this->data
        ])->subject('YếnVN | Xác nhận đăng ký tài khoản thành công!');
    }
}
