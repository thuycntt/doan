<?php

namespace App\Mail;

use App\Models\Customer;
use App\Models\Question;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserReplyQuestionMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var Customer
     */
    public $customer;

    /**
     * @var Question
     */
    public $question;

    /**
     * UserReplyQuestionMail constructor.
     * @param $customer
     * @param $question
     */
    public function __construct($customer, $question)
    {
        $this->customer = $customer;
        $this->question = $question;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.reply-customer-question', [
            'customer' => $this->customer,
            'diary' => $this->question
        ])->subject('YếnVN | Câu hỏi được phản hồi');
    }
}
