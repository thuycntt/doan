<?php


namespace App\Http\Controllers;


use App\Http\Requests\User\UpdatePasswordRequest;
use App\Http\Requests\User\UserUpdateInfoRequest;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function markAsReadNotify($id, Request $request)
    {
        $notification = auth()->user()->notifications()->where('id', $id)->first();

        if ($notification) {
            $notification->markAsRead();
        }
        return redirect()->to($request->input('redirect', '/'));
    }

    public function updatePassword()
    {
        set_admin_title('Đổi mật khẩu');
        return view('user.update_password');
    }

    public function postUpdatePassword(UpdatePasswordRequest $request)
    {
        $data = $request->validated();
        if (\Hash::check($data['old_password'], \Auth::user()->password)) {
            auth()->user()->update(['password'=>\Hash::make($data['password'])]);
            return redirect()->back()->withNoties([
                'heading' => 'Thành công',
                'message' => 'Cập nhật thành công',
                'type' => 'success'
            ]);
        }
        return response()->redirectToRoute('user.updatePassword')->withErrors([
            'old_password' => 'Mật khẩu cũ không đúng'
        ]);

    }
    public function updateInfo(){
        set_admin_title('Đổi thông tin User');
        return view('user.update_info');
    }
    public function postUpdateInfo(UserUpdateInfoRequest $request){
        $data = $request->validated();
        if (!empty($data['avatar'])) {
            /**
             * @var UploadedFile $avatar
             */
            $avatar = $data['avatar'];
            $fileName = $avatar->getClientOriginalName();
            if (!\File::exists(public_path('user_avatar'))) {
                \File::makeDirectory(public_path('user_avatar'), 0777);
            }

            $avatar->move(public_path('user_avatar/' . \Auth::user()->id), $fileName);

            $fileName = 'customer_avatar/' . \Auth::user()->id . '/' . $fileName;

        } else {
            $fileName = \Auth::user()->avatar;
        }
        $dataUpdate = [
            'dob' => empty($data['dob']) ? \Auth::user()->dob : $data['dob'],
            'phone' => empty($data['phone']) ? \Auth::user()->phone : $data['phone'],
            'about' => empty($data['about']) ? \Auth::user()->about : $data['about'],
            'name' => empty($data['name']) ? \Auth::user()->name : $data['name'],
            'avatar' => $fileName
        ];
        \Auth::user()->update($dataUpdate);

        return redirect()->back()->withNoties([
            'heading' => 'Thành công',
            'message' => 'Cập nhật thành công',
            'type' => 'success'
        ]);
    }
}
