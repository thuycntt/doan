<?php


namespace App\Http\Controllers;


use App\Mail\UserReplyDiaryMail;
use App\Models\Customer;
use App\Models\CustomerDiary;
use App\Models\Reply;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DiaryController extends Controller
{

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function index()
    {
        $customerId = request()->get('customer_id', null);
        $customer = Customer::find($customerId);
        $query = $customerId ? CustomerDiary::where('customer_id', $customerId)->orderBy('date', 'desc')->orderBy('seen_at', 'asc') : CustomerDiary::query();
        if (request()->wantsJson()) {
            return datatables()->eloquent($query)
                ->addColumn('seen', function ($diary) {
                    return $diary->seen_by ? $diary->seenBy->name : 'N/A';
                })
                ->addColumn('status', function ($diary) {
                    return '<span class="label label-lg font-weight-bold label-light-' . ($diary->status === 1 ? 'success' : 'info') . ' label-inline">' . ($diary->status === 1 ? 'Mở' : 'Đóng') . '</span>';
                })
                ->addColumn('customer', function ($diary) {
                    return $diary->customer->name;
                })
                ->addColumn('date', function ($diary) {
                    return Carbon::parse($diary->date)->format('d/m/Y');
                })
                ->addColumn('actions', function ($diary) {
                    return view('diary.table-action', compact('diary'));
                })
                ->rawColumns(['thumbnail', 'actions', 'status'])
                ->toJson();
        }
        set_admin_title($customer ? 'Nhật ký của khách hàng: ' . $customer->name : 'Nhật ký khách hàng');
        return view('diary.index');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function diaryDetail($id)
    {
        $diary = CustomerDiary::with(['replies' => function ($query) {
            $query->orderBy('created_at', 'asc');
        }, 'replies.customer', 'replies.user'])->findOrFail($id);
        if (empty($diary->seen_by)) {
            $diary->update(['seen_by' => auth()->user()->id]);
        } else if ($diary->seen_by != auth()->user()->id) {
            abort(404);
        }
        if ($diary->images) {
            $images = json_decode($diary->images, true);
            $arr = [];
            foreach ($images as $image) {
              $arr[] =asset($image);
            }
            $diary->images = $arr;
        }
        $diary->date = Carbon::parse($diary->date)->format('d/m/Y');
        return view('diary.detail', compact('diary'));
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function replyToDiary($id, Request $request)
    {
        $message = $request->input('message');
        $diary = CustomerDiary::with('customer')->findOrFail($id);
        $reply = Reply::create([
            'reply' => $message,
            'diary_id' => $diary->id,
            'user_id' => \Auth::user()->id,
        ]);

        $reply->created = $reply->created_at->diffForHumans();

        \Mail::to($diary->customer->email)->queue(new UserReplyDiaryMail($diary->customer, $diary));

        return response()->json($reply);
    }

    public function updateStatus()
    {
        $id = \request()->post('id', null);
        $status = \request()->post('status');
        if ($id) {
            $diary = CustomerDiary::find($id);
            if ($diary) {
                $diary->update([
                    'status' => $status
                ]);
                return redirect()->back()->withNoties([
                    'heading' => 'Thành công',
                    'message' => 'Cập nhật thành công',
                    'type' => 'success'
                ]);
            }
        }
        return redirect()->back()->withNoties([
            'heading' => 'Lỗi',
            'message' => 'Có lỗi xảy ',
            'type' => 'error'
        ]);
    }
}
