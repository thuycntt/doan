<?php


namespace App\Http\Controllers;


use App\Http\Requests\MenuItem\CreateMenuItemRequest;
use App\Http\Requests\MenuItem\MenuItemRequest;
use App\Models\Menu;
use App\Models\MenuItem;

class MenuItemController extends Controller
{
    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($id)
    {
        $menu = Menu::findOrFail($id);
        set_admin_title('Quản lý menu: ' . $menu->name);
        return view('menu-item.index', compact('menu'));
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDataNestable($id)
    {
        return response()->json(MenuItem::allMenus($id));
    }

    /**
     * @param $id
     * @param CreateMenuItemRequest $request
     * @return mixed
     */
    public function storeOrUpdate(MenuItemRequest $request)
    {
        $data = $request->validated();
        if (isset($data['id'])) {
            $updateMenuItem = [
                'name' => $data['name'],
                'url' => $data['url'],
                'description' => empty($data['description']) ? null : $data['description'],
            ];
            $menuItem = MenuItem::findOrFail($data['id']);
            $menuItem->update($updateMenuItem);
            return response()->redirectToRoute('menu_item.index', $data['menu_id'])->withNoties([
                'heading' => 'Thành công',
                'message' => 'Cập nhật thành công',
                'type' => 'success'
            ]);
        } else {
            $max = MenuItem::whereNull('parent_id')->where('menu_id', $data['menu_id'])->withTrashed()->max('order');
            $data['order'] = $max ? intval($max) + 1 : 1;
            $insertMenuItem = [
                'name' => $data['name'],
                'url' => $data['url'],
                'parent_id' => null,
                'description' => empty($data['description']) ? null : $data['description'],
                'order' => $data['order'],
                'menu_id' => $data['menu_id']
            ];
            MenuItem::create($insertMenuItem);
            return response()->redirectToRoute('menu_item.index', $data['menu_id'])->withNoties([
                'heading' => 'Thành công',
                'message' => 'Tạo mới thành công',
                'type' => 'success'
            ]);
        }

    }

    public function updateNestable()
    {
        $menuId = request()->input('menu_id');
        $data = json_decode(request()->input('data', '[]'), true);
        $this->processUpdate($data, $menuId);

        return response()->redirectToRoute('menu_item.index', $menuId)->withNoties([
            'heading' => 'Thành công',
            'message' => 'Cập nhật thành công',
            'type' => 'success'
        ]);
    }

    /**
     * @param $data
     * @param $menuId
     * @param null $parentId
     */
    private function processUpdate($data, $menuId, $parentId = null)
    {
        foreach ($data as $index => $menuItem) {
            $item = MenuItem::withTrashed()->find($menuItem['id']);
            $item->order = $index + 1;

            if (!is_null($parentId)) {
                $item->parent_id = $parentId;
            } else $item->parent_id = null;

            $item->save();
            if (isset($menuItem['children'])) {
                $this->processUpdate($menuItem['children'], $menuId, $item->id);
            }
        }
    }

    /**
     * @return mixed
     */
    public function softDelete()
    {
        try {
            $id = request()->post('id');
            $menuItem = MenuItem::findOrFail($id);
            $menuItem->delete();
            return response()->json('ok');
        } catch (\Exception $exception) {
            return response()->json('error', 500);
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function restore()
    {
        try {
            $id = request()->post('id');
            $menuItem = MenuItem::onlyTrashed()->findOrFail($id);
            $menuItem->restore();
            return response()->json('ok');
        } catch (\Exception $exception) {
            return response()->json('error', 500);
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
//    public function delete()
//    {
//        try {
//            $id = request()->post('id');
//            \Schema::disableForeignKeyConstraints();
//            $menuItem = MenuItem::withTrashed()->findOrFail($id);
//            $menuItem->forceDelete();
//            \Schema::enableForeignKeyConstraints();
//            return response()->json('ok');
//        } catch (\Exception $exception) {
//            return response()->json('error', 500);
//        }
//    }
}
