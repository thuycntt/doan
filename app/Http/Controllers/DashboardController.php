<?php


namespace App\Http\Controllers;


use App\Models\Article;
use App\Models\Customer;
use App\Models\Order;
use App\Models\Product;
use App\Models\ProductRate;
use App\Models\Transaction;
use Carbon\Carbon;

class DashboardController extends Controller
{
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        set_admin_title('Thống kê');
        $fromDate = request()->get('fromDate',date('d/m/Y'));
        $toDate = request()->get('toDate',date('d/m/Y'));
        $totalProduct = Product::count();
        $totalArticle = Article::count();
        $totalOrder = Order::count();
        $totalCustomer = Customer::count();

        $revenueOfMonth = Transaction::where(function ($query) {
            $start = Carbon::now()->startOfMonth()->startOfDay()->toDateTimeString();
            $query->where('created_at', '>=', $start)
                ->where('created_at', '<=', Carbon::now()->toDateTimeString())
                ->where('status', Transaction::STATUS_SUCCESS);
        })->get()->reduce(function ($total, $current) {
            return $total + intval($current->amount);
        }, 0);

        $revenueOf7Days = Transaction::where(function ($query) {
            $start = Carbon::now()->subDays(7)->startOfDay()->toDateTimeString();
            $query->where('created_at', '>=', $start)
                ->where('created_at', '<=', Carbon::now()->toDateTimeString())
                ->where('status', Transaction::STATUS_SUCCESS);
        })->get()->reduce(function ($total, $current) {
            return $total + intval($current->amount);
        }, 0);

        $revenueOfToDay = Transaction::where(function ($query) {
            $start = Carbon::now()->startOfDay()->toDateTimeString();
            $query->where('created_at', '>=', $start)
                ->where('created_at', '<=', Carbon::now()->toDateTimeString())
                ->where('status', Transaction::STATUS_SUCCESS);
        })->get()->reduce(function ($total, $current) {
            return $total + intval($current->amount);
        }, 0);


        return view('dashboard', compact(
            'totalArticle',
            'totalCustomer',
            'totalOrder',
            'totalProduct',
            'revenueOf7Days',
            'revenueOfMonth',
            'revenueOfToDay',
            'fromDate',
            'toDate'));
    }

    public function getData()
    {
        $fromDate = request()->get('fromDate',date('d/m/Y'));
        $toDate = request()->get('toDate',date('d/m/Y'));
        $fromDate = Carbon::parse(str_replace('/','-',$fromDate));
        $toDate = Carbon::parse(str_replace('/','-',$toDate));
        $days = [];
        $date = $fromDate->copy();
        while ($date <= $toDate) {
            $days[] = $date->toDateString();
            $date->addDays(1);
        }

        $orderByDays = [];
        $customerByDays = [];
        $productRateByDays = [];

        foreach ($days as $day) {
            $totalOrder = Order::where(function ($query) use ($day) {
                $query->where('created_at', '>=', Carbon::parse($day)->startOfDay()->toDateTimeString())
                    ->where('created_at', '<=', Carbon::parse($day)->endOfDay()->toDateTimeString());
            })->count();
            $orderByDays[Carbon::parse($day)->format('d/m/Y')] = $totalOrder;

            $totalCustomer = Customer::where(function ($query) use ($day) {
                $query->where('created_at', '>=', Carbon::parse($day)->startOfDay()->toDateTimeString())
                    ->where('created_at', '<=', Carbon::parse($day)->endOfDay()->toDateTimeString());
            })->count();
            $customerByDays[Carbon::parse($day)->format('d/m/Y')] = $totalCustomer;

            $totalRate = ProductRate::where(function ($query) use ($day) {
                $query->where('created_at', '>=', Carbon::parse($day)->startOfDay()->toDateTimeString())
                    ->where('created_at', '<=', Carbon::parse($day)->endOfDay()->toDateTimeString());
            })->count();
            $productRateByDays[Carbon::parse($day)->format('d/m/Y')] = $totalRate;
        }

        $countCOD = Transaction::where(function ($query) use ($fromDate, $toDate) {
            $query->where('type', Transaction::COD)
                ->where('status', Transaction::STATUS_SUCCESS)
                ->where('created_at','<=',$toDate->toDateTimeString())
                ->where('created_at','>=',$fromDate->toDateTimeString());
        })->count();
        $countMomo = Transaction::where(function ($query) use ($fromDate, $toDate){
            $query->where('type', Transaction::MOMO)
                ->where('status', Transaction::STATUS_SUCCESS)
                ->where('created_at','<=',$toDate->toDateTimeString())
                ->where('created_at','>=',$fromDate->toDateTimeString());
        })->count();

        return response()->json([
            'orders_7_day' => $orderByDays,
            'customers_7_day' => $customerByDays,
            'rate_7_day' => $productRateByDays,
            'transaction_ratio' => ['Ví Momo' => $countMomo, 'COD' => $countCOD]
        ]);
    }
}
