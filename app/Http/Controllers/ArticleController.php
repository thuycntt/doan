<?php


namespace App\Http\Controllers;


use App\Http\Requests\Article\CreateArticleRequest;
use App\Http\Requests\Article\UpdateArticleRequest;
use App\Models\Article;

class ArticleController
{
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function index()
    {
        if (request()->wantsJson()) {
            return datatables()->eloquent(Article::query())
                ->addColumn('thumbnail', function ($article) {
                    return '<img src="' . $article->thumbnail . '" alt="' . $article->title . '" style="width:150px">';
                })
                ->addColumn('actions', function ($article) {
                    return view('article.table-action', compact('article'))->render();
                })
                ->addColumn('status', function ($article) {
                    return '<span class="label label-lg font-weight-bold label-light-' . ($article->status === 1+Article::STATUS_ACTIVE ? 'success' : 'info') . ' label-inline">' . ($article->status === Article::STATUS_ACTIVE ? 'Xuất bản' : 'Nháp') . '</span>';
                })
                ->rawColumns(['thumbnail', 'actions', 'status'])
                ->toJson();
        }
        set_admin_title('Quản lý bài viết');
        return view('article.index');
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        set_admin_title('Tạo mới bài viết');
        return view('article.create');
    }

    /**
     * @param CreateArticleRequest $request
     * @return mixed
     */
    public function store(CreateArticleRequest $request)
    {
        $data = $request->validated();
        $dataInsertArticle = [
            'title' => $data['title'],
            'slug' => $data['slug'],
            'short_content' => $data['short_content'],
            'content' => $data['content'],
            'thumbnail' => $data['thumbnail'],
            'status' => $data['status'],
            'is_feature' => $data['is_feature'],
            'author_name' => $data['author_name'],
            'author_id' => $data['author_id']
        ];
        $article = Article::create($dataInsertArticle);
        $tags = $data['tags'];
        for ($i = 0; $i < count($tags); $i++) {
            // $article->tags()->attach($tag);
            $dataInsertTagArticle = [
                'article_id' => $article->id,
                'tag_id' => $tags[$i]
            ];
            \DB::table('article_tag')->insert($dataInsertTagArticle);
        }
        return redirect()->route('article.index')->withNoties([
            'heading' => 'Thành công',
            'message' => 'Tạo mới thành công',
            'type' => 'success'
        ]);
    }

    public function edit($id)
    {
        $article = Article::with('tags')->findOrFail($id);
        set_admin_title('Cập nhật: ' . $article->title);
        return view('article.update', ['article' => $article]);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function updateStatus($id)
    {
        $article = Article::findOrFail($id);
        $article->update([
            'status' => $article->status == 1 ? 0 : 1
        ]);

        return redirect()->route('article.index')->withNoties([
            'heading' => 'Thành công',
            'message' => 'Cập nhật thành công',
            'type' => 'success'
        ]);
    }

    /**
     * @param $id
     * @param UpdateArticleRequest $request
     * @return mixed
     */
    public function update($id, UpdateArticleRequest $request)
    {
        $data = $request->validated();
        $article = Article::findOrFail($id);
        $tags = $data['tags'];
        $updateArticle = [
            'title' => $data['title'],
            'slug' => $data['slug'],
            'short_content' => $data['short_content'],
            'content' => $data['content'],
            'thumbnail' => $data['thumbnail'],
            'status' => $data['status'],
            'is_feature' => $data['is_feature']
        ];
        $article->update($updateArticle);
        //$article->tags()->detach();
        \DB::table('article_tag')->where('article_id', $article->id)->delete();
        for ($i = 0; $i < count($tags); $i++) {
            $insertTag = [
                'article_id' => $article->id,
                'tag_id' => $tags[$i]
            ];
            \DB::table('article_tag')->insert($insertTag);
        }

        return redirect()->route('article.index')->withNoties([
            'heading' => 'Thành công',
            'message' => 'Cập nhật thành công',
            'type' => 'success'
        ]);

    }

    /**
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function destroy($id)
    {
        $article = Article::findOrFail($id);
        $article->delete();
        return redirect()->route('article.index')->withNoties([
            'heading' => 'Thành công',
            'message' => 'Xoá thành công',
            'type' => 'success'
        ]);
    }
}
