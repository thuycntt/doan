<?php


namespace App\Http\Controllers;


use App\Http\Requests\Menu\MenuRequest;
use App\Models\Menu;

class MenuController extends Controller
{
    public function index()
    {
        if (request()->wantsJson()) {
            return datatables()->eloquent(Menu::query())
                ->addColumn('actions', function ($menu) {
                    return view('menu.table-action', compact('menu'));
                })
                ->addColumn('position', function ($menu) {
                    $text = '';
                    switch ($menu->position) {
                        case Menu::POSITION_TOP:
                        {
                            $text = 'Top';
                            break;
                        }
                    }
                    return $text;
                })
                ->rawColumns(['actions'])
                ->toJson();
        }
        set_admin_title('Quản lý menu');
        return view('menu.index');
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        set_admin_title('Tạo mới menu');
        return view('menu.create');
    }

    /**
     * @param MenuRequest $request
     * @return mixed
     */
    public function store(MenuRequest $request)
    {
        $data = $request->validated();
        $insertMenu = [
            'name' => $data['name'],
            'description' => empty($data['description']) ? null : $data['description'],
            'position' => $data['position']
        ];
        Menu::create($insertMenu);
        return response()->redirectToRoute('menu.index')->withNoties([
            'heading' => 'Thành công',
            'message' => 'Tạo mới thành công',
            'type' => 'success'
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $menu = Menu::findOrFail($id);
        set_admin_title('Cập nhật: ' . $menu->name);
        return view('menu.update', compact('menu'));
    }

    /**
     * @param $id
     * @param MenuRequest $request
     * @return mixed
     */
    public function update($id, MenuRequest $request)
    {
        $menu = Menu::findOrFail($id);
        $data = $request->validated();
        $updateMenu = [
            'name' => $data['name'],
            'position' => $data['position'],
            'description' => empty($data['description']) ? null : $data['description']
        ];
        $menu->update($updateMenu);
        return response()->redirectToRoute('menu.index')->withNoties([
            'heading' => 'Thành công',
            'message' => 'Cập nhật thành công',
            'type' => 'success'
        ]);
    }
}
