<?php


namespace App\Http\Controllers;


use App\Http\Requests\Order\UpdateOrderStatusRequest;
use App\Models\District;
use App\Models\Order;
use App\Models\Product;
use App\Models\Province;
use App\Models\Transaction;
use App\Models\Ward;
use Carbon\Carbon;

class OrderController extends Controller
{
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function index()
    {
        $ward = request()->get('ward',null);
        $province = request()->get('province',null);
        $district = request()->get('district',null);
        $status = request()->get('status', Order::STATUS_VERIFY);
        if (request()->wantsJson()) {
            $query = Order::query();
            if($ward){
                $query = $query->where('ward_id',$ward);
            }
            $query = $query->where('status', $status)->orderBy('created_at', 'desc');
            return datatables()->eloquent($query)
                ->addColumn('actions', function ($order) {
                    return view('order.table-action', compact('order'))->render();
                })
                ->addColumn('customer', function ($order) {
                    return $order->customer ? $order->customer->name : 'N/A';
                })
                ->addColumn('amount', function ($order) {
                    return number_format(intval($order->transaction ? $order->transaction->amount : 0)+intval($order->ship_fee)) . ' VND';
                })
                ->addColumn('transaction', function ($order) {
                    $transaction = $order->transaction;
                    return $transaction ? ($transaction->type === Transaction::MOMO ? 'Ví Momo | <span class="text-success">Đã thanh toán</span>' : 'Thanh toán khi nhận hàng (COD)' . ($transaction->status === Transaction::STATUS_SUCCESS ? ' | <span class="text-success">Đã thanh toán</span>' : '')) : 'N/A';
                })
                ->addColumn('created_at', function ($order) {
                    return $order->created_at->format('d/m/Y H:i');
                })
                ->addColumn('status', function ($order) {
                    $text = '';
                    switch ($order->status) {
                        case Order::STATUS_VERIFY:
                        {
                            $text = '<span class="label label-lg font-weight-bold label-light-dark label-inline"> Chờ xác nhận</span>';
                            break;
                        }
                        case Order::STATUS_PENDING:
                        {
                            $text = '<span class="label label-lg font-weight-bold label-light-warning label-inline"> Chờ giao hàng</span>';
                            break;
                        }
                        case Order::STATUS_GETTING_PRODUCT:
                        {
                            $text = '<span class="label label-lg font-weight-bold label-light-info label-inline"> Chờ lấy hàng</span>';
                            break;
                        }
                        case Order::STATUS_SHIPPING:
                        {
                            $text = '<span class="label label-lg font-weight-bold label-light-primary label-inline"> Đang giao hàng</span>';
                            break;
                        }
                        case Order::STATUS_COMPLETE:
                        {
                            $text = '<span class="label label-lg font-weight-bold label-light-success label-inline"> Đã giao hàng</span>';
                            break;
                        }
                        case Order::STATUS_CANCEL:
                        {
                            $text = '<span class="label label-lg font-weight-bold label-light-dark label-inline"> Đã huỷ</span>';
                            break;
                        }

                    }
                    return $text;
                })
                ->rawColumns(['status', 'actions', 'transaction'])
                ->toJson();
        }
        $ward = Ward::find($ward);
        $district = District::find($district);
        $province = Province::find($province);
        set_admin_title('Quản lý đơn đặt hàng');
        return view('order.index', compact('status','ward','district','province'));
    }

    /**
     * @param UpdateOrderStatusRequest $request
     * @return mixed
     */
    public function updateOrderStatus(UpdateOrderStatusRequest $request)
    {
        $data = $request->validated();
        $order = Order::with('transaction')->where('uuid', $data['order_uuid'])->first();

        if ($order) {
            $order->update([
                'status' => $data['status']
            ]);

            if ($data['status'] == Order::STATUS_COMPLETE && $order->transaction->type == Transaction::COD) {
                $order->transaction->update([
                    'status' => Transaction::STATUS_SUCCESS
                ]);
            }

            if ($data['status'] == Order::STATUS_CANCEL){
                $products = $order->products()->withPivot('quantity')->get();
                for ($i = 0; $i<$products->count(); $i++){
                    $products[$i]->update([
                       'quantity'=>$products[$i]->pivot->quantity + $products[$i]->quantity
                    ]);
                }
            }

            return redirect()->route('order.index')->withNoties([
                'heading' => 'Thành công',
                'message' => 'Cập nhật thành công',
                'type' => 'success'
            ]);
        }

        return redirect()->back()->withNoties([
            'heading' => 'Lỗi',
            'message' => 'Cập nhật không thành công',
            'type' => 'error'
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function orderDetail($id)
    {
        $order = Order::with(['customer', 'transaction', 'products' => function ($query) {
            $query->withPivot(['quantity','product_price','product_discount']);
        }])->findOrFail($id);
        set_admin_title('Chi tiết đơn hàng: ' . $order->uuid);

        $transactionType = $order->transaction->type === \App\Models\Transaction::MOMO ? 'Ví Momo | <span class="text-success">Đã thanh toán</span>' : 'Thanh toán khi nhận hàng (COD)' . ($order->transaction->status === \App\Models\Transaction::STATUS_SUCCESS ? ' | <span class="text-success">Đã thanh toán</span>' : '');

        $status = '';
        switch ($order->status) {
            case Order::STATUS_VERIFY:
            {
                $status = '<span class="label label-lg font-weight-bold label-light-dark label-inline"> Chờ xác nhận</span>';
                break;
            }
            case Order::STATUS_PENDING:
            {
                $status = '<span class="label label-lg font-weight-bold label-light-warning label-inline"> Chờ giao hàng</span>';
                break;
            }
            case Order::STATUS_GETTING_PRODUCT:
            {
                $status = '<span class="label label-lg font-weight-bold label-light-info label-inline"> Chờ lấy hàng</span>';
                break;
            }
            case Order::STATUS_SHIPPING:
            {
                $status = '<span class="label label-lg font-weight-bold label-light-primary label-inline"> Đang giao hàng</span>';
                break;
            }
            case Order::STATUS_COMPLETE:
            {
                $status = '<span class="label label-lg font-weight-bold label-light-success label-inline"> Đã giao hàng</span>';
                break;
            }
            case Order::STATUS_CANCEL:
            {
                $status = '<span class="label label-lg font-weight-bold label-light-dark label-inline"> Đã huỷ</span>';
                break;
            }

        }
        return view('order.detail', compact('order', 'status', 'transactionType'));
    }

    public function updateDeliveryDate($id = null){
        $delivery = request()->post('delivery_date');
        $order = Order::find($id);
        if(!($delivery&&$order)){
            return redirect()->back()->withNoties([
                'heading' => 'Lỗi',
                'message' => 'Lỗi',
                'type' => 'error'
            ]);
        }
        $order->update([
            'delivery_date'=>Carbon::parse(str_replace('/','-',$delivery))
        ]);

        return redirect()->back()->withNoties([
            'heading' => 'Thành công',
            'message' => 'Cập nhật thành công',
            'type' => 'success'
        ]);
    }
    public function getProvince(){
        $term = request()->get('term','');
        $data = Province::where('name','like','%'.$term.'%')->paginate(10);
        return response()->json($data);
    }
    public function getDistrict(){
        $id = request()->get('province_id');
        $term = request()->get('term','');
        $data = District::where('province_id',$id)->where('name','like','%'.$term.'%')->paginate(10);
        return response()->json($data);
    }
    public function getWard(){
        $id = request()->get('district_id');
        $term = request()->get('term','');
        $data = Ward::where('district_id',$id)->where('name','like','%'.$term.'%')->paginate(10);
        return response()->json($data);
    }
}
