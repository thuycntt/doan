<?php


namespace App\Http\Controllers;


use App\Mail\UserReplyQuestionMail;
use App\Models\Question;
use App\Models\Reply;
use Illuminate\Http\Request;

class QuestionController extends Controller
{
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function index()
    {
        if (request()->wantsJson()) {
            return datatables()->eloquent(Question::query()->orderBy('status', 'desc')->orderBy('created_at', 'desc'))
                ->addColumn('actions', function ($question) {
                    return (($question->user && $question->user->id === auth()->user()->id) || empty($question->user)) ? view('question.table-action', compact('question'))->render() : '';
                })
                ->addColumn('status', function ($question) {
                    return '<span class="label label-lg font-weight-bold label-light-' . ($question->status === 1 ? 'success' : 'info') . ' label-inline">' . ($question->status === 1 ? 'Mở' : 'Đóng') . '</span>';
                })
                ->addColumn('customer', function ($question) {
                    return $question->customer->name;
                })
                ->addColumn('user', function ($question) {
                    return $question->user ? $question->user->name : 'N/A';
                })
                ->rawColumns(['actions', 'status'])
                ->toJson();
        }
        set_admin_title('Quản lý câu hỏi');
        return view('question.index');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function questionDetail($id)
    {
        $question = Question::with(['replies', 'replies.customer', 'replies.user'])->findOrFail($id);
        if (empty($question->user_id)) {
            $question->update(['user_id' => auth()->user()->id]);
        } else if ($question->user_id != auth()->user()->id) {
            abort(404);
        }
        set_admin_title('Chi tiết câu hỏi');
        return view('question.detail', compact('question'));
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function replyToQuestion($id, Request $request)
    {
        $question = Question::findOrFail($id);
        $user = $request->input('user');
        $message = $request->input('message');

        $reply = Reply::create([
            'reply' => $message,
            'question_id' => $question->id,
            'user_id' => $user,
        ]);

        $reply->created = $reply->created_at->diffForHumans();

        \Mail::to($question->customer->email)->queue(new UserReplyQuestionMail($question->customer, $question));

        return response()->json($reply);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function updateStatus($id)
    {
        $status = request()->input('status');
        $question = Question::findOrfail($id);
        $question->update(['status' => $status]);

        return redirect()->back()->withNoties([
            'heading' => 'Thành công',
            'message' => 'Cập nhật thành công',
            'type' => 'success'
        ]);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function unsetUser($id)
    {
        $question = Question::findOrfail($id);
        $question->update(['user_id' => null]);

        return redirect()->back()->withNoties([
            'heading' => 'Thành công',
            'message' => 'Cập nhật thành công',
            'type' => 'success'
        ]);
    }
}
