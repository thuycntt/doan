<?php


namespace App\Http\Controllers;


use App\Http\Requests\Product\ProductRequest;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductImage;

class ProductController extends Controller
{
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function index()
    {
        $status = request()->get('status', 'show');
        $type = request()->get('type', 'product');
        if (request()->wantsJson()) {
            $query = Product::query();
            if ($status === 'deleted') {
                $query = $query->onlyTrashed();
            }

            $query = $query->leftJoin('category_product', 'products.id', '=', 'category_product.product_id')
                ->leftJoin('categories', 'category_product.category_id', '=', 'categories.id')
                ->where('categories.type', '=', $type === 'equipment' ? Category::TYPE_EQUIPMENT : Category::TYPE_FOOD)
                //->whereNull('categories.deleted_at')
                ->select('products.*');

            return datatables()->eloquent($query)
                ->addColumn('thumbnail', function ($product) {
                    return '<img src="' . $product->thumbnail . '" alt="' . $product->name . '" style="width:150px">';
                })
                ->addColumn('actions', function ($product) {
                    return view('product.table-action', compact('product'))->render();
                })
                ->addColumn('price', function ($product) {
                    return number_format($product->price) . ' VNĐ';
                })
                ->addColumn('status', function ($product) {
                    return '<span class="label label-lg font-weight-bold label-light-' . (!$product->deleted_at ? 'success' : 'info') . ' label-inline">' . (!$product->deleted_at ? 'Hiển thị' : 'Đã xoá') . '</span>';
                })
                ->rawColumns(['thumbnail', 'actions', 'status'])
                ->toJson();
        }
        set_admin_title('Quản lý sản phẩm');
        return view('product.index', compact('status', 'type'));
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        set_admin_title('Tạo mới sản phẩm');

        $categories = Category::orderBy('categories.order', 'DESC')->get();
//        $categories = $this->sortCategories($categories);

        return view('product.create', compact('categories'));
    }

    /**
     * @param $list
     * @param array $result
     * @param null $parent
     * @param int $dept
     * @return array|mixed
     */
//    public function sortCategories($list, &$result = [], $parent = null, $dept = 0)
//    {
//
//        foreach ($list as $key => $object) {
//            if ($object->parent_id == $parent) {
//                $object->dept = $dept;
//                array_push($result, $object);
//                unset($list[$key]);
//                $this->sortCategories($list, $result, $object->id, $dept + 1);
//            }
//        }
//
//        return $result;
//    }

    /**
     * @param ProductRequest $request
     * @return mixed
     */
    public function store(ProductRequest $request)
    {
        $data = $request->validated();

        if (isset($data['auto_sku'])) {
            $data['sku'] = $this->generateSku();
            unset($data['auto_sku']);
        } else {
            $check = Product::withTrashed()->where('sku', $data['sku'])->first();
            if ($check) {
                return redirect()->back()->withErrors([
                    'sku' => 'Đã tồn tại mã Sku'
                ])->withInput($data);
            }
        }
        $insertProduct = [
            'sku' => $data['sku'],
            'name' => $data['name'],
            'slug' => $data['slug'],
            'price' => $data['price'],
            'quantity' => $data['quantity'],
            'description' => $data['description'],
            'thumbnail' => $data['thumbnail'],
            'discount' => $data['discount'],
            'trending'=>$data['trending']
        ];


        $images = json_decode($data['images'], true);

        $product = Product::create($insertProduct);

        foreach ($data['categories'] as $category) {
            $dataInsertCategoryProduct = [
                'product_id' => $product->id,
                'category_id' => $category
            ];
            \DB::table('category_product')->insert($dataInsertCategoryProduct);
        }

        foreach ($images as $image) {
            $arrReplace = [
                env('APP_URL') => '',
                'http://' => '',
                'https://' => ''
            ];
            $image = str_replace(array_keys($arrReplace), array_values($arrReplace), $image);
            ProductImage::create([
                'image' => $image,
                'product_id' => $product->id
            ]);
        }

        return response()->redirectToRoute('product.index')->withNoties([
            'heading' => 'Thành công',
            'message' => 'Tạo mới thành công',
            'type' => 'success'
        ]);
    }

    /**
     * @return string
     */
    private function generateSku()
    {
        $sku = strtoupper(\Str::random(8));

        while (Product::where('sku', $sku)->first() !== null) {
            $sku = strtoupper(\Str::random(8));
        }

        return $sku;
    }

    public function edit($id)
    {
        $product = Product::withTrashed()->with(['productImages', 'categories'])->find($id);
        $categoryType = $product->categories->first()->type;
        $product->categories = $product->categories->pluck('id')->toArray();
        set_admin_title('Cập nhật sản phẩm: ' . $product->name);
        $images = $product->productImages->map(function ($item) {
            return $item->image;
        })->toArray();
        $categories = Category::orderBy('categories.order', 'DESC')->get();
        return view('product.update', compact('product', 'images', 'categories', 'categoryType'));
    }

    /**
     * @param $id
     * @param ProductRequest $request
     * @return mixed
     */
    public function update($id, ProductRequest $request)
    {
        $product = Product::findOrFail($id);
        $data = $request->validated();

        /* get images from data post */

        $updateProduct = [
            'name' => $data['name'],
            'slug' => $data['slug'],
            'price' => $data['price'],
            'quantity' => $data['quantity'],
            'description' => $data['description'],
            'thumbnail' => $data['thumbnail'],
            'discount' => $data['discount'],
            'trending'=>$data['trending']
        ];


        $product->update($updateProduct);

        $product->productImages()->delete();
        \DB::table('category_product')->where('product_id', $product->id)->delete();

        foreach ($data['categories'] as $category) {
            $dataInsertCategoryProduct = [
                'product_id' => $product->id,
                'category_id' => $category
            ];
            \DB::table('category_product')->insert($dataInsertCategoryProduct);
        }
        $images = json_decode($data['images'], true);
        foreach ($images as $image) {
            $arrReplace = [
                env('APP_URL') => '',
                'http://' => '',
                'https://' => ''
            ];
            $image = str_replace(array_keys($arrReplace), array_values($arrReplace), $image);
            ProductImage::create([
                'image' => $image,
                'product_id' => $product->id
            ]);
        }

        return response()->redirectToRoute('product.index')->withNoties([
            'heading' => 'Thành công',
            'message' => 'Cập nhật thành công',
            'type' => 'success'
        ]);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $product = Product::findOrFail($id);
        $product->delete();

        return response()->redirectToRoute('product.index')->withNoties([
            'heading' => 'Thành công',
            'message' => 'Xoá thành công',
            'type' => 'success'
        ]);
    }


    /**
     * @param $id
     * @return mixed
     */
    public function restore($id)
    {
        $product = Product::onlyTrashed()->findOrFail($id);
        $product->restore();

        return response()->redirectToRoute('product.index')->withNoties([
            'heading' => 'Thành công',
            'message' => 'Khôi phục thành công',
            'type' => 'success'
        ]);
    }

}
