<?php


namespace App\Http\Controllers;


use App\Http\Requests\Tag\CreateTagRequest;
use App\Http\Requests\Tag\UpdateTagRequest;
use App\Models\Tag;

class TagController extends Controller
{

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function index()
    {
        if (request()->wantsJson()) {
            return datatables()->eloquent(Tag::query())
                ->addColumn('actions', function ($record) {
                    return view('tag.table-action', compact('record'))->render();
                })
                ->addColumn('name', function ($record) {
                    return $record->name;
                })
                ->rawColumns(['actions'])
                ->toJson();
        }

        set_admin_title('Quản lý tag');

        return view('tag.index');
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        set_admin_title('Tạp mới tag');
        return view('tag.create');
    }

    /**
     * @param CreateTagRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateTagRequest $request)
    {

        $data = $request->validated();
        $result = mb_strtolower($data['name']);
        $exists = Tag::where('name', $result)->first();
        if($exists){
            return response()->redirectToRoute('tag.create')->withNoties([
                'heađing'=>'Lỗi',
                'message'=>"Tag đã tồn tại",
                'type'=>'error'
            ]);
        }
        $insertTag = [
            'name' => $result,
            'slug' => $data['slug'],
            'description' => empty($data['description']) ? null : $data['description']
        ];
        Tag::create($insertTag);
        return response()->redirectToRoute('tag.index')->withNoties([
            'heading' => 'Thành công',
            'message' => 'Tạo mới thành công',
            'type' => 'success'
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $tag = Tag::findOrFail($id);
        set_admin_title('Cập nhật tag: ' . $tag->name);
        return view('tag.update', compact('tag'));
    }

    /**
     * @param $id
     * @param UpdateTagRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id, UpdateTagRequest $request)
    {
        $tag = Tag::find($id);
        $data = $request->validated();
        $result = mb_strtolower($data['name']);
        if($result !== $tag->name){
            $exist = Tag::where('name',$result)->first();
            if($exist){
                return response()->redirectToRoute('tag.edit',$tag->id)->withNoties([
                    'heađing'=>'Lỗi',
                    'message'=>"Tag đã tồn tại",
                    'type'=>'error'
                ]);
            }
        }
        $updateTag = [
            'name' => $result,
            'slug' => \Str::slug($result),
            'description' => empty($data['description']) ? null : $data['description']
        ];
        $tag->update($updateTag);
        return response()->redirectToRoute('tag.index')->withNoties([
            'heading' => 'Thành công',
            'message' => 'Cập nhật thành công',
            'type' => 'success'
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $tag = Tag::findOrFail($id);
        \DB::table('article_tag')->where('tag_id',$tag->id)->delete();
        $tag->delete();
        return response()->redirectToRoute('tag.index')->withNoties([
            'heading' => 'Thành công',
            'message' => 'Xoá thành công',
            'type' => 'success'
        ]);
    }

    public function select2()
    {
        $page = request()->post('page', 0);
        $term = request()->post('term', '');
        $tags = Tag::where('name', 'like', '%' . $term . '%')->paginate(10);
        return response()->json($tags);
    }
}
