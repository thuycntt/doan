<?php


namespace App\Http\Controllers;


use App\Mail\ReplyContactMail;
use App\Models\Contact;

class ContactController extends Controller
{
    public function index()
    {
        if (request()->wantsJson()) {
            return datatables()->eloquent(Contact::query()->orderBy('status', 'ASC')->orderBy('created_at', 'DESC'))
                ->addColumn('actions', function ($contact) {
                    return view('contact.table-action', compact('contact'))->render();
                })
                ->addColumn('created_at', function ($contact) {
                    return $contact->created_at->format('d/m/Y H:i');
                })
                ->addColumn('user', function ($contact) {
                    return $contact->replyBy ? $contact->replyBy->name : 'N/A';
                })
                ->addColumn('status', function ($contact) {
                    switch ($contact->status) {
                        case 0:
                        {
                            return '<span class="label label-lg font-weight-bold label-light-success label-inline"> Mở </span>';
                        }
                        case 1:
                        {
                            return '<span class="label label-lg font-weight-bold label-light-info label-inline"> Đóng </span>';
                        }
                    }
                })
                ->rawColumns(['actions', 'status'])
                ->toJson();
        }
        set_admin_title('Liên hệ');
        return view('contact.index');
    }

    /**
     * @return mixed
     */
    public function reply()
    {
        $id = request()->post('id', null);
        $content = request()->post('content', '');
        if ($id) {

            $contact = Contact::find($id);

            \Mail::to($contact->email)->queue(new ReplyContactMail($contact, $content));

            $contact->update(['status' => 1, 'reply_by' => auth()->user()->id]);

            return redirect()->back()->withNoties([
                'heading' => 'Thành công',
                'message' => 'Gửi phản hồi thành công',
                'type' => 'success'
            ]);
        }

        return redirect()->back()->withNoties([
            'heading' => 'Lỗi',
            'message' => 'Có lỗi xảy ',
            'type' => 'error'
        ]);
    }
}
