<?php


namespace App\Http\Controllers;


use App\Http\Requests\SlideItem\CreateSlideItemRequest;
use App\Http\Requests\SlideItem\UpdateSlideItemRequest;
use App\Models\Slide;
use App\Models\SlideItem;

class SlideItemController extends Controller
{
    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function index($id)
    {
        $slide = Slide::with('slideItems')->findOrFail($id);
        set_admin_title('Quản lý slide con');
        return view('slide-item.index', ['slide' => $slide]);
    }

    public function create($slide)
    {
        set_admin_title('Tạo mới slide con');
        $slide = Slide::findOrFail($slide);
        return view('slide-item.create', compact('slide'));
    }

    /**
     * @param $slide
     * @param CreateSlideItemRequest $request
     * @return mixed
     */
    public function store($slide, CreateSlideItemRequest $request)
    {
        $data = $request->validated();
        $maxOrder = SlideItem::where('slide_id', $slide)->max('order');
        $maxOrder = empty($maxOrder) ? 1 : (intval($maxOrder)+ 1);
        $insertSlideItem = [
            'slide_id' => $slide,
            'url' => $data['url'],
            'image' => $data['image'],
            'description' => empty($data['description']) ? null : $data['description'],
            'order' => $maxOrder,
            'status' => $data['status']
        ];
        SlideItem::create($insertSlideItem);
        return redirect()->route('slide_item.index', ['id' => $slide])->withNoties([
            'heading' => 'Thành công',
            'message' => 'Tạo mới thành công',
            'type' => 'success'
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $slideItem = SlideItem::with('slide')->findOrFail($id);
        set_admin_title('Cập nhật slide con');
        return view('slide-item.update', compact('slideItem'));
    }

    /**
     * @param $id
     * @param UpdateSlideItemRequest $request
     * @return mixed
     */
    public function update($id, UpdateSlideItemRequest $request)
    {
        $data = $request->validated();
        $updateSlideItem = [
            'url' => $data['url'],
            'image' => $data['image'],
            'description' => empty($data['description']) ? null : $data['description'],
            'status' => $data['status']
        ];
        $item = SlideItem::findOrFail($id);
        $item->update($updateSlideItem);
        return redirect()->route('slide_item.index', ['id' => $item->slide->id])->withNoties([
            'heading' => 'Thành công',
            'message' => 'Cập nhật thành công',
            'type' => 'success'
        ]);
    }

    /**
     * @param $id
     * @return mixed
     */
//    public function updateStatus($id)
//    {
//        $item = SlideItem::findOrFail($id);
//        $item->update([
//            'status' => $item->status == 1 ? 2 : 1
//        ]);
//        return redirect()->route('slide_item.index', ['id' => $item->slide->id])->withNoties([
//            'heading' => 'Thành công',
//            'message' => 'Cập nhật thành công',
//            'type' => 'success'
//        ]);
//    }

    public function delete($id)
    {
        $item = SlideItem::findOrFail($id);
        $item->delete();
        return redirect()->route('slide_item.index', ['id' => $item->slide->id])->withNoties([
            'heading' => 'Thành công',
            'message' => 'Xoá thành công',
            'type' => 'success'
        ]);
    }

    public function updateSort()
    {
        $data = request()->post('sort');
        $data = json_decode($data);
        foreach ($data as $index => $id) {
            $slideItem = SlideItem::find($id);
            $slideItem->update([
                'order' => $index + 1
            ]);
        }
        return redirect()->back()->withNoties([
            'heading' => 'Thành công',
            'message' => 'Cập nhật thành công',
            'type' => 'success'
        ]);
    }
}
