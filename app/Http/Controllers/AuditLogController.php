<?php


namespace App\Http\Controllers;


use App\Models\AuditLog;

class AuditLogController extends Controller
{
    public function index()
    {
        if (request()->wantsJson()) {
            return datatables()->eloquent(AuditLog::query()->orderBy('created_at', 'DESC'))
                ->addColumn('info', function ($log) {
                    $user = $log->user;
                    return '<strong>' . $user->name . '</strong> ' . $log->content . ' <span class="text-muted">' . $log->created_at->diffForHumans() . '</span> (<span class="text-primary">' . $log->ip . '</span>)';
                })
                ->rawColumns(['info'])
                ->toJson();
        }
        set_admin_title('Lịch sử truy cập');
        return view('audit-log.index');
    }
}
