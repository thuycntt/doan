<?php


namespace App\Http\Controllers;

use App\Models\Customer;

class CustomerController extends Controller
{
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function index()
    {
        if (request()->wantsJson()) {
            return datatables()->eloquent(Customer::query())
                ->addColumn('actions', function ($customer) {
                    return view('customer.table-action', compact('customer'))->render();
                })
                ->addColumn('created_at', function ($customer) {
                    return $customer->created_at->format('d/m/Y H:i');
                })
                ->rawColumns(['thumbnail', 'actions', 'status'])
                ->toJson();
        }
        set_admin_title('Khách hàng');
        return view('customer.index');
    }
}
