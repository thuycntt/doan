<?php


namespace App\Http\Controllers\Api\Auth;


use App\Http\Controllers\Controller;
use App\Mail\CustomerRegisterMail;
use App\Models\Customer;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $validate = \Validator::make($request->all(), [
            'username' => ['required', 'unique:customers,username'],
            'password' => ['required', 'confirmed'],
            'password_confirmation' => ['required'],
            'email' => ['required', 'unique:customers,email'],
            'name' => ['required']
        ], [], [
            'username' => 'tên đăng nhập',
            'password' => 'mật khẩu',
            'password_confirmation' => 'nhập lại mật khẩu',
            'email' => 'email',
            'name' => 'họ & tên'
        ]);

        if ($validate->fails()) {
            return response()->json([
                'errors' => $validate->errors()
            ], 422);
        }

        try {
            $data = $validate->validated();
            $insertData = [
                'username' => $data['username'],
                'password' => \Hash::make($data['password']),
                'email' => $data['email'],
                'name' => $data['name'],
                'avatar' => 'vendor/media/user-default.jpg'
            ];
            $customer = Customer::create($insertData);

            \Mail::to($customer->email)->queue(new CustomerRegisterMail($customer, $data));

            return response()->json([
                'status' => 1,
                'customer' => $customer
            ]);
        } catch (\Exception $exception) {
            return response()->json([
                'status' => 0,
            ]);
        }
    }
}
