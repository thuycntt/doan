<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use JWTAuth;
use Tymon\JWTAuth\Token;
use Validator;

class LoginController extends Controller
{
    /*
       |--------------------------------------------------------------------------
       | Login Controller
       |--------------------------------------------------------------------------
       |
       | This controller handles authenticating users for the application and
       | redirecting them to your home screen. The controller uses a trait
       | to conveniently provide its functionality to your applications.
       |
       */

    use AuthenticatesUsers;

    /**
     * @param $all
     * @return \Illuminate\Validation\Validator
     */
    private function validateLogin($all)
    {
        return Validator::make($all, [
            'username' => 'required',
            'password' => 'required|string|min:6'
        ], [], [
            'username' => 'Tên đăng nhập',
            'password' => 'Mật khẩu'
        ]);
    }

    /**
     * @return string
     */
    public function username()
    {
        return 'username';
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        $credentials = request(['username', 'password']);
        $validated = $this->validateLogin($credentials);
        if ($validated->fails()) return response()->json($validated->errors(), 422);

        if (!$token = auth('api')->attempt($credentials)) {
            return response()->json([
                'message' => 'Email/mật khẩu không đúng'
            ], 422);
        }
        return $this->respondWithToken($token);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        $auth = auth('api')->user();
        return response()->json($auth);
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth('api')->logout();
        return response()->json([
            'message' => 'Logout successful!'
        ]);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth('api')->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        $payload = JWTAuth::decode(new Token($token));
        $expireIn = auth('api')->factory()->getTTL() * 60;
        $expireAt = $payload['exp'];
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => $expireIn,
            'expires_at' => $expireAt,
            'user' => auth('api')->user()
        ]);
    }

}
