<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Models\Menu;
use App\Models\MenuItem;

class MenuController extends Controller
{
   public function getMenu(){
       $menuId = request()->get('menu_id',null);
       if($menuId!=null){
           $menu = Menu::find($menuId);
           if($menu!=null){
               return response()->json(['menu'=>$menu, 'menuItem'=>MenuItem::menuFrontend($menuId)]);
           }
       }
       return response()->json([
           'message'=>'Không có dữ liệu'
       ],500);
   }

}
