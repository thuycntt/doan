<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Customer\CustomerUpdateInfoRequest;
use App\Http\Requests\Api\Customer\UpdatePasswordRequest;
use App\Models\Customer;
use Illuminate\Http\UploadedFile;

class CustomerController extends Controller
{
    public function updateInfo(CustomerUpdateInfoRequest $request)
    {
        $data = $request->validated();
        $customer = Customer::find($data['customer_id']);
        if (!empty($data['avatar'])) {
            /**
             * @var UploadedFile $avatar
             */
            $avatar = $data['avatar'];
            $fileName = $avatar->getClientOriginalName();
            if (!\File::exists(public_path('customer_avatar'))) {
                \File::makeDirectory(public_path('customer_avatar'), 0777);
            }

            $avatar->move(public_path('customer_avatar/' . $customer->id), $fileName);

            $fileName = 'customer_avatar/' . $customer->id . '/' . $fileName;

        } else {
            $fileName = $customer->avatar;
        }
        $dataUpdate = [
            'dob' => empty($data['dob']) ? $customer->dob : $data['dob'],
            'phone' => empty($data['phone']) ? $customer->phone : $data['phone'],
            'about' => empty($data['about']) ? $customer->about : $data['about'],
            'name' => empty($data['name']) ? $customer->name : $data['name'],
            'avatar' => $fileName
        ];
        $customer->update($dataUpdate);
        $customer = $customer->toArray();
        $customer['avatar'] = asset($customer['avatar']);
        return response()->json($customer);
    }
    public function updatePassword(UpdatePasswordRequest $request){
        $data = $request->validated();
        $customer = Customer::find($data['customer_id']);
       if( \Hash::check($data['old_password'],$customer->password)){
           $customer->update(['password'=>\Hash::make($data['password'])]);
           return response()->json([
               'message'=>'Cập nhật thành công'
           ],200);
       }
       return response()->json([
           'errors'=>[
               'password'=>['Password không đúng']
           ]

       ],422);
    }
}
