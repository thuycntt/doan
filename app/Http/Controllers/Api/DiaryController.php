<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Diary\CreateDiaryRequest;
use App\Http\Requests\Api\Diary\CreateReplyDiaryRequest;
use App\Models\Customer;
use App\Models\CustomerDiary;
use App\Models\Reply;
use App\Notifications\CustomerReplyDiaryNotification;
use Carbon\Carbon;

class DiaryController extends Controller
{
    /**
     * @param CreateDiaryRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveDiary(CreateDiaryRequest $request)
    {
        $data = $request->validated();
        if(Carbon::parse($data['date'])->greaterThan(Carbon::today())){
            return response()->json([
               'errors'=>['date'=>['Không tạo nhật ký vượt quá ngày hiện tại']]
            ],422);
        }

        $exists = CustomerDiary::where(function ($query) use ($data) {
            $query->where('customer_id', $data['customer_id'])
                ->where('date', '=', Carbon::parse($data['date'])->toDateString());
        })->first();

        if ($exists) {
            return response()->json([
                'message' => 'Nhật ký của hôm nay đã được tạo'
            ], 500);
        }

        if (isset($data['images'])) {
            $arr = [];
            foreach ($data['images'] as  $image) {
                $imageName = $image->getClientOriginalName();
                $folder = 'customer_diary/' . $data['customer_id'] . '/' . date('d-m-y');
                if (!\File::exists(public_path($folder))) {
                    \File::makeDirectory(public_path($folder), 0777, true);
                }
                $image->move(public_path($folder), $imageName);
               // $data['images'][$key] = $folder . '/' . $imageName;
                $arr[] = $folder . '/' . $imageName;
            }
            $data['images'] = json_encode($arr);
        }

        $insertDiary = [
            'note' => strip_tags($data['note']),
            'images' => isset($data['images']) ? $data['images'] : null,
            'customer_id' => $data['customer_id'],
            'seen_at' => null,
            'seen_by' => null,
            'status' => 1,
            'date' => $data['date']
        ];
        $diary = CustomerDiary::create($insertDiary);
        $diary->date = Carbon::parse($diary->date)->format('d/m/Y');

        if (!empty($arr)) {
            $tmp = [];
            foreach ($arr as $image) {
                $tmp[] = asset($image);
            }
            $diary->images = $tmp;
        }
        return response()->json($diary);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDiary()
    {
        $per_page = request()->get('per_page', 10);
        $customer_id = request()->get('customer_id');
        if ($customer_id !== null) {
            $diaries = CustomerDiary::with(['replies' => function($query){
                $query->orderBy('created_at','asc');
            },'replies.user','replies.customer'])->where('customer_id', $customer_id)->orderBy('date', 'ASC')->paginate($per_page);
            $diaries->getCollection()->transform(function ($diary) {
                if ($diary->images) {
                    $arr =[];
                    $images = json_decode($diary->images, true);
                    foreach ($images as $image) {
                        $arr[] = asset($image);
                    }
                    $diary->images = $arr;
                }
               // $diary->replies_count = $diary->replies->count();
                $diary = $diary->toArray();
                $diary['date'] = Carbon::parse($diary['date'])->format('d/m/Y');
                $diary['created_at'] = Carbon::parse($diary['created_at'])->timezone(config('app.timezone'))->format('d/m/Y H:i');
                $diary['updated_at'] = Carbon::parse($diary['updated_at'])->timezone(config('app.timezone'))->format('d/m/Y H:i');
                foreach ($diary['replies'] as &$reply){
                    $reply['created_at'] = Carbon::parse($reply['created_at'])->timezone(config('app.timezone'))->diffForHumans();
                    $reply['updated_at'] = Carbon::parse($reply['updated_at'])->timezone(config('app.timezone'))->diffForHumans();
                }

                return $diary;
            });
            return response()->json($diaries);
        }
        return response()->json([
            'message' => 'Không có dữ liệu'
        ], 500);
    }

    public function saveReply(CreateReplyDiaryRequest $request)
    {
        $data = $request->validated();
        $insertData = [
            'reply' => strip_tags($data['reply']),
            'diary_id' => $data['diary_id'],
            'customer_id' => $data['customer_id']
        ];
        $reply = Reply::create($insertData);
        $reply = $reply->toArray();
        $reply['created_at'] = Carbon::parse($reply['created_at'])->diffForHumans();
        $customer = Customer::find($data['customer_id']);
        $reply['customer'] = $customer;
        $dataNotify = [
            'customer_name' => $customer->name,
            'id' => $data['diary_id']
        ];
        $diary = CustomerDiary::find($data['diary_id']);
        if ($diary->seen_by !== null) {
            $user = $diary->seenBy;
            $user->notify(new CustomerReplyDiaryNotification($dataNotify));
        }
        return response()->json($reply);

    }
}
