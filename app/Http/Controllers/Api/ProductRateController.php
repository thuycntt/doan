<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Http\Requests\Api\ProductRate\CreateProductRateRequest;
use App\Models\Customer;
use App\Models\Product;
use App\Models\ProductRate;
use Carbon\Carbon;


class ProductRateController extends Controller
{
    /**
     * @param CreateProductRateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveProductRate(CreateProductRateRequest $request)
    {
        $data = $request->validated();
        $exists = ProductRate::where(function ($query) use ($data) {
            $query->where('customer_id', $data['customer_id'])
                ->where('product_id', $data['product_id']);
        })->first();
        if ($exists != null) {
            return response()->json([
                'message' => 'Bạn đã đánh giá sản phẩm này'
            ], 500);
        }
        $data['status'] = ProductRate::STATUS_PENDING;
        if(isset($data['comment'])){
            $data['comment']=strip_tags($data['comment']);
        }
        $rate = ProductRate::create($data);
        $rate = $rate->toArray();
        $customer = Customer::find($data['customer_id']);
        $rate['created_at'] = Carbon::parse($rate['created_at'])->timezone(config('app.timezone'))->format('d/m/Y H:i');
        $rate['updated_at'] = Carbon::parse($rate['updated_at'])->timezone(config('app.timezone'))->format('d/m/Y H:i');
        $rate['customer'] = $customer;
        return response()->json($rate);
    }


    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProductRate()
    {
        $perPage = request()->get('per_page', 10);
        $product_id = request()->get('product_id');
        if ($product_id != null) {
            $product = Product::find($product_id);
            if ($product != null) {
                $productRate = ProductRate::with('customer')
                    ->where(function ($query) use ($product_id) {
                        $query->where('product_id', $product_id)
                            ->where('status', ProductRate::STATUS_ACCEPT);
                    })->orderBy('created_at', 'desc')
                    ->paginate($perPage);
                $productRate->getCollection()->transform(function ($rate) {
                    $rate = $rate->toArray();
                    $rate['created_at'] = Carbon::parse($rate['created_at'])->timezone(config('app.timezone'))->format('d/m/Y H:i');
                    $rate['updated_at'] = Carbon::parse($rate['updated_at'])->timezone(config('app.timezone'))->format('d/m/Y H:i');
                    return $rate;
                });
                return response()->json($product);
            }
        }
        return response()->json([
            'message' => 'Không có dữ liệu'
        ], 500);

    }
}
