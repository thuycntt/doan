<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Models\Category;

class CategoryController extends Controller
{
    public function getCategory()
    {
        $type = request()->get('type', Category::TYPE_FOOD);
        $categories = Category::where(function ($query) use ($type) {
            $query->where('type', $type)
                ->whereNull('deleted_at');
        })->orderBy('categories.order', 'DESC')->get();

        $categories = $this->sortCategories($categories);

        return response()->json($categories);
    }

    /**
     * @param $list
     * @param array $result
     * @param null $parent
     * @param int $dept
     * @return array|mixed
     */
    public function sortCategories($list, &$result = [], $parent = null, $dept = 0)
    {

        foreach ($list as $key => $object) {
            if ($object->parent_id == $parent) {
                $object->dept = $dept;
                array_push($result, $object);
                unset($list[$key]);
                $this->sortCategories($list, $result, $object->id, $dept + 1);
            }
        }

        return $result;
    }
}
