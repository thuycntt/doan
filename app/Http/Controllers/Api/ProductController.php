<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductRate;
use Carbon\Carbon;

class ProductController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function listProduct()
    {
        $per_page = request()->get('per_page', 10);
        $search = request()->get('search');

        $product = Product::with(['productRates' => function ($query) {
            $query->where('status', ProductRate::STATUS_ACCEPT)
                ->orderBy('created_at', 'desc');
        }])->leftJoin('category_product','products.id','=','category_product.product_id')
            ->leftJoin('categories','categories.id','=','category_product.category_id')
            ->whereNull('categories.deleted_at')
            ->whereNull('products.deleted_at');
        if ($search) {
            $product = $product->where('products.name', 'like', "%{$search}%");
        }

        $product = $product->select('products.*')
            ->orderBy('products.created_at', 'desc')
            ->paginate($per_page);

        $product->getCollection()->transform(function ($product) {
            $product->thumbnail = asset($product->thumbnail);
            $product->price = intval($product->price);
            $average = 0;
            for ($i = 0; $i < count($product->productRates); $i++) {
                $average = $average + $product->productRates[$i]->rate;
            }
            $average = $average == 0 ? 0 : $average / count($product->productRates);
            $product->rate = $average;
            unset($product->productRates);
            unset($product->description);
            $product = $product->toArray();
            $product['created_at'] = Carbon::parse($product['created_at'])->timezone(config('app.timezone'))->format('d/m/Y H:i');
            $product['updated_at'] = Carbon::parse($product['updated_at'])->timezone(config('app.timezone'))->format('d/m/Y H:i');
            return $product;
        });
        return response()->json($product);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function productDetail()
    {
        $sku = request()->get('sku', null);
        $customer = request()->get('customer_id', null);
        if ($sku) {
            $product = Product::with(['productImages', 'categories', 'productRates' => function ($query) {
                $query->where('status', ProductRate::STATUS_ACCEPT)
                    ->orderBy('created_at', 'desc');
            }, 'productRates.customer' => function ($query) {
                $query->select('customers.name', 'customers.id', 'customers.email');
            }])->where('sku', $sku)->first();
            if ($product) {
                $categories = $product->categories;
                $average = 0;
                for ($i = 0; $i < count($product->productRates); $i++) {
                    $average = $average + $product->productRates[$i]->rate;
                }
                $average = $average == 0 ? 0 : $average / count($product->productRates);
                $product = $product->toArray();
                $product['thumbnail'] = asset($product['thumbnail']);
                $product['price'] = intval($product['price']);
                $product['created_at'] = Carbon::parse($product['created_at'])->timezone(config('app.timezone'))->format('d/m/Y H:i');
                $product['updated_at'] = Carbon::parse($product['updated_at'])->timezone(config('app.timezone'))->format('d/m/Y H:i');
                $product['rate'] = $average;

                foreach ($product['product_images'] as &$image) {
                    $image['image'] = asset($image['image']);
                }

                foreach ($product['product_rates'] as &$rate) {
                    $rate['created_at'] = Carbon::parse($rate['created_at'])->timezone(config('app.timezone'))->format('d/m/Y H:i');
                    $rate['updated_at'] = Carbon::parse($rate['updated_at'])->timezone(config('app.timezone'))->format('d/m/Y H:i');
                }

                $productRelated = Product::with(['productRates' => function ($query) {
                    $query->where('status', ProductRate::STATUS_ACCEPT)
                        ->orderBy('created_at', 'desc');
                }])->leftJoin('category_product', 'products.id', '=', 'category_product.product_id')
                    ->leftJoin('categories', 'category_product.category_id', '=', 'categories.id')
                    ->where('categories.type', $categories->first()->type)
                    ->whereIn('categories.id', $categories->pluck('id')->toArray())
                    ->whereNull('categories.deleted_at')
                    ->whereNull('products.deleted_at')
                    ->select('products.*')
                    ->where(function ($query) use ($sku) {
                        $query->whereNotIn('products.sku', [$sku]);
                    })->orderBy('products.created_at', 'desc')->get(12)->toArray();

                foreach ($productRelated as &$news) {
                    $news['thumbnail'] = asset($news['thumbnail']);
                    $news['price'] = intval($news['price']);
                }

                $product['product_related'] = $productRelated;

                if ($customer) {
                    $customerRate = ProductRate::where(function ($query) use ($customer, $product) {
                        $query->where('customer_id', $customer)
                            ->where('product_id', $product['id']);
                    })->first();
                } else {
                    $customerRate = null;
                }

                $product['customer_rate'] = $customerRate;

                return response()->json($product);
            }
        }
        return response()->json([
            'message' => 'No data'
        ], 500);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function listProductNew()
    {
        $limit = request()->get('number', 6);

        $list = Product::with(['productRates' => function ($query) {
            $query->where('status', ProductRate::STATUS_ACCEPT)
                ->orderBy('created_at', 'desc');
        }])->leftJoin('category_product', 'products.id', '=', 'category_product.product_id')
            ->leftJoin('categories', 'category_product.category_id', '=', 'categories.id')
            ->where('categories.type', Category::TYPE_FOOD)
            ->whereNull('categories.deleted_at')
            ->whereNull('products.deleted_at')
            ->select('products.*')
            ->limit($limit)->orderBy('created_at', 'desc')->get();
        $arr = [];
        for ($i = 0; $i < $list->count(); $i++) {

            $temp = $list[$i];
            $temp = $temp->toArray();

            $average = collect($temp['product_rates'])->reduce(function ($c, $n) {
                return $c + $n['rate'];
            }, 0);

            $select = [
                'sku' => $temp['sku'],
                'name' => $temp['name'],
                'slug' => $temp['slug'],
                'price' => intval($temp['price']),
                'thumbnail' => asset($temp['thumbnail']),
                'created_at' => Carbon::parse($temp['created_at'])->format('d/m/Y H:i'),
                'discount' => $temp['discount'],
                'rate' => $average == 0 ? 0 : ($average / count($temp['product_rates'])),
                'quantity' => $temp['quantity']
            ];

            $arr[] = $select;
        }
        return response()->json($arr);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function listFilterProduct()
    {
        $category_id = request()->get('category_id', null);
        $from = request()->get('from_price', null);
        $to = request()->get('to_price', null);
        $order = request()->get('order', 'asc');
        $per_page = request()->get('per_page', 10);
        $result = Product::with(['productRates' => function ($query) {
            $query->where('status', ProductRate::STATUS_ACCEPT)
                ->orderBy('created_at', 'desc');
        }])->join('category_product', 'category_product.product_id', '=', 'products.id')
            ->join('categories', 'category_product.category_id', '=', 'categories.id')
            ->where(function ($query) use ($category_id, $from, $to, $order) {
                $query->where('categories.type', '=', Category::TYPE_FOOD)
                    ->whereNull('categories.deleted_at')
                    ->whereNull('products.deleted_at');

                if ($category_id !== null) {
                    $query->where('categories.id', '=', $category_id);
                }
                if ($from !== null) {
                    $query->where('products.price', '>', $from);
                }
                if ($to !== null) {
                    $query->where('products.price', '<', $to);
                }

            })->orderBy('products.price', $order)->select('products.*')->paginate($per_page);
        $result->getCollection()->transform(function ($product) {
            $product->thumbnail = asset($product->thumbnail);
            $product->price = intval($product->price);
            $average = 0;
            for ($i = 0; $i < count($product->productRates); $i++) {
                $average = $average + $product->productRates[$i]->rate;
            }
            $average = $average == 0 ? 0 : $average / count($product->productRates);
            $product->rate = $average;
            unset($product->productRates);
            unset($product->description);
            $product = $product->toArray();
            $product['created_at'] = Carbon::parse($product['created_at'])->timezone(config('app.timezone'))->format('d/m/Y H:i');
            $product['updated_at'] = Carbon::parse($product['updated_at'])->timezone(config('app.timezone'))->format('d/m/Y H:i');
            return $product;
        });
        return response()->json($result);
    }

    public function listFilterProductEquipment()
    {
        $category_id = request()->get('category_id', null);
        $from = request()->get('from_price', null);
        $to = request()->get('to_price', null);
        $order = request()->get('order', 'asc');
        $per_page = request()->get('per_page', 10);
        $result = Product::with(['productRates' => function ($query) {
            $query->where('status', ProductRate::STATUS_ACCEPT)
                ->orderBy('created_at', 'desc');
        }])->join('category_product', 'category_product.product_id', '=', 'products.id')
            ->join('categories', 'category_product.category_id', '=', 'categories.id')
            ->where(function ($query) use ($category_id, $from, $to, $order) {
                $query->where('categories.type', '=', Category::TYPE_EQUIPMENT)
                    ->whereNull('categories.deleted_at')
                    ->whereNull('products.deleted_at');
                if ($category_id !== null) {
                    $query->where('categories.id', '=', $category_id);
                }
                if ($from !== null) {
                    $query->where('products.price', '>', $from);
                }
                if ($to !== null) {
                    $query->where('products.price', '<', $to);
                }

            })->orderBy('products.price', $order)->select('products.*')->paginate($per_page);
        $result->getCollection()->transform(function ($product) {
            $product->thumbnail = asset($product->thumbnail);
            $product->price = intval($product->price);
            $average = 0;
            for ($i = 0; $i < count($product->productRates); $i++) {
                $average = $average + $product->productRates[$i]->rate;
            }
            $average = $average == 0 ? 0 : $average / count($product->productRates);
            $product->rate = $average;
            unset($product->productRates);
            unset($product->description);
            $product = $product->toArray();
            $product['created_at'] = Carbon::parse($product['created_at'])->timezone(config('app.timezone'))->format('d/m/Y H:i');
            $product['updated_at'] = Carbon::parse($product['updated_at'])->timezone(config('app.timezone'))->format('d/m/Y H:i');
            return $product;
        });
        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function listTrendingProducts()
    {
        $limit = request()->get('number', 20);
        $products = Product::with(['productRates' => function ($query) {
            $query->where('status', ProductRate::STATUS_ACCEPT)
                ->orderBy('created_at', 'desc');
        }])->join('category_product', 'products.id', '=', 'category_product.product_id')
            ->join('categories', 'categories.id', '=', 'category_product.category_id')
            ->where('products.trending', 1)
            ->whereNull('categories.deleted_at')
            ->whereNull('products.deleted_at')
            ->limit($limit)->select('products.*')->get();
        $arr = [];
        for ($i = 0; $i < $products->count(); $i++) {

            $temp = $products[$i];
            $temp = $temp->toArray();

            $average = collect($temp['product_rates'])->reduce(function ($c, $n) {
                return $c + $n['rate'];
            }, 0);

            $select = [
                'sku' => $temp['sku'],
                'name' => $temp['name'],
                'slug' => $temp['slug'],
                'price' => intval($temp['price']),
                'thumbnail' => asset($temp['thumbnail']),
                'created_at' => Carbon::parse($temp['created_at'])->format('d/m/Y H:i'),
                'discount' => $temp['discount'],
                'rate' => $average == 0 ? 0 : ($average / count($temp['product_rates'])),
                'quantity' => $temp['quantity']
            ];

            $arr[] = $select;
        }
        return response()->json($arr);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function listProductRandom()
    {
        $limit = request()->get('number', 12);
        $list = Product::with(['productRates' => function ($query) {
            $query->where('status', ProductRate::STATUS_ACCEPT)
                ->orderBy('created_at', 'desc');
        }])->get()->random($limit);

        $arr = [];
        for ($i = 0; $i < $list->count(); $i++) {
            $temp = $list[$i];
            $temp = $temp->toArray();
            if (empty($temp['product_rates'])) {
                $average = 0;
            } else {
                $average = collect($temp['product_rates'])->reduce(function ($c, $n) {
                    return $c + $n['rate'];
                }, 0);
            }


            $select = [
                'sku' => $temp['sku'],
                'name' => $temp['name'],
                'slug' => $temp['slug'],
                'price' => intval($temp['price']),
                'thumbnail' => asset($temp['thumbnail']),
                'created_at' => Carbon::parse($temp['created_at'])->format('d/m/Y H:i'),
                'discount' => $temp['discount'],
                'rate' => $average == 0 ? 0 : $average / count($temp['product_rates']),
                'quantity' => $temp['quantity']
            ];

            $arr[] = $select;
        }
        return response()->json($arr);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function listProductDiscount()
    {
        $limit = request()->get('number', 12);

        $list = Product::with(['categories', 'productRates' => function ($query) {
            $query->where('status', ProductRate::STATUS_ACCEPT)
                ->orderBy('created_at', 'desc');
        }])->join('category_product', 'products.id', '=', 'category_product.product_id')
            ->join('categories', 'categories.id', '=', 'category_product.category_id')
            ->where('categories.type', Category::TYPE_FOOD)
            ->limit($limit)->orderBy('products.discount', 'desc')->get();
        $arr = [];
        for ($i = 0; $i < $list->count(); $i++) {

            $temp = $list[$i];
            $temp = $temp->toArray();

            $average = collect($temp['product_rates'])->reduce(function ($c, $n) {
                return $c + $n['rate'];
            }, 0);

            $select = [
                'sku' => $temp['sku'],
                'name' => $temp['name'],
                'slug' => $temp['slug'],
                'price' => intval($temp['price']),
                'thumbnail' => asset($temp['thumbnail']),
                'created_at' => Carbon::parse($temp['created_at'])->format('d/m/Y H:i'),
                'discount' => $temp['discount'],
                'rate' => $average == 0 ? 0 : ($average / count($temp['product_rates'])),
                'quantity' => $temp['quantity']
            ];

            $arr[] = $select;
        }
        return response()->json($arr);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function listProductByCategory()
    {
        $per_page = request()->get('per_page', 10);
        $category = request()->get('cat_id', null);
        $category = Category::find($category);

        if (!$category) {
            return response()->json([
                "current_page" => 1,
                "per_page" => $per_page,
                'data' => []
            ]);
        }

        $products = Product::query()->with(['productRates' => function ($query) {
            $query->where('status', ProductRate::STATUS_ACCEPT)
                ->orderBy('created_at', 'desc');
        }])->leftJoin('category_product', 'products.id', '=', 'category_product.product_id')
            ->leftJoin('categories', 'category_product.category_id', '=', 'categories.id')
            ->where('categories.id', '=', $category)
            ->select('products.*')
            ->paginate($per_page);

        $products->getCollection()->transform(function ($product) {
            $product->thumbnail = asset($product->thumbnail);
            $product->price = intval($product->price);
            $average = 0;
            for ($i = 0; $i < count($product->productRates); $i++) {
                $average = $average + $product->productRates[$i]->rate;
            }
            $average = $average == 0 ? 0 : $average / count($product->productRates);
            $product->rate = $average;
            unset($product->productRates);
            unset($product->description);
            $product = $product->toArray();
            $product['created_at'] = Carbon::parse($product['created_at'])->timezone(config('app.timezone'))->format('d/m/Y H:i');
            $product['updated_at'] = Carbon::parse($product['updated_at'])->timezone(config('app.timezone'))->format('d/m/Y H:i');
            return $product;
        });

        return response()->json($products);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function listEquipmentProduct()
    {
        $number = request()->get('number', 10);

        $products = Product::query()->with(['productRates' => function ($query) {
            $query->where('status', ProductRate::STATUS_ACCEPT)
                ->orderBy('created_at', 'desc');
        }])->leftJoin('category_product', 'products.id', '=', 'category_product.product_id')
            ->leftJoin('categories', 'category_product.category_id', '=', 'categories.id')
            ->where('categories.type', Category::TYPE_EQUIPMENT)
            ->whereNull('categories.deleted_at')
            ->whereNull('products.deleted_at')
            ->select('products.*')
            ->limit($number)->get();

        $products->map(function ($product) {
            $product->thumbnail = asset($product->thumbnail);
            $product->price = intval($product->price);
            $average = 0;
            for ($i = 0; $i < count($product->productRates); $i++) {
                $average = $average + $product->productRates[$i]->rate;
            }
            $average = $average == 0 ? 0 : $average / count($product->productRates);
            $product->rate = $average;
            unset($product->productRates);
            unset($product->description);
            $product = $product->toArray();
            $product['created_at'] = Carbon::parse($product['created_at'])->timezone(config('app.timezone'))->format('d/m/Y H:i');
            $product['updated_at'] = Carbon::parse($product['updated_at'])->timezone(config('app.timezone'))->format('d/m/Y H:i');
            return $product;
        });

        return response()->json($products);
    }
}
