<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Question\CreateQuestionRequest;
use App\Http\Requests\Api\Question\CreateReplyRequest;
use App\Models\Customer;
use App\Models\Question;
use App\Models\Reply;
use App\Notifications\CustomerReplyQuestionNotification;
use Carbon\Carbon;

class QuestionController extends Controller
{
    /**
     * @param CreateQuestionRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveQuestion(CreateQuestionRequest $request)
    {
        $data = $request->validated();
        $insertQuestion = [
            'question' => strip_tags($data['question']),
            'customer_id' => $data['customer_id'],
            'status' => 1
        ];
        $question = Question::create($insertQuestion);
        $question = $question->toArray();
        $question['replies'] = [];
        $question['created_at'] = Carbon::parse($question['created_at'])->format('d/m/Y H:i');
        $question['updated_at'] = Carbon::parse($question['updated_at'])->format('d/m/Y H:i');
        return response()->json($question);
    }

    /**
     * @param CreateReplyRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveReply(CreateReplyRequest $request)
    {
        $data = $request->validated();
        $insertReply = [
            'reply' => strip_tags($data['reply']),
            'question_id' => $data['question_id'],
            'customer_id' => $data['customer_id']
        ];
        $reply = Reply::create($insertReply);
        $reply = $reply->toArray();
        $reply['created_at'] = Carbon::parse($reply['created_at'])->diffForHumans();
        $reply['updated_at'] = Carbon::parse($reply['updated_at'])->diffForHumans();

        $question = Question::find($data['question_id']);
        $customer = $question->customer;
        $user = $question->user;
        $reply['customer'] = $customer;

        if($user){
            $dataNotify = [
                'customer_name' => $customer->name,
                'id' => $question->id
            ];

            $user->notify(new CustomerReplyQuestionNotification($dataNotify));
        }


        return response()->json($reply);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function listQuestion()
    {
        $per_page = request()->get('per_page', 10);
        $customerId = request()->get('customer_id');
        if ($customerId != null) {
            $customer = Customer::find($customerId);
            if ($customer != null) {
                $questions = Question::with(['replies' => function($query){
                    $query->orderBy('created_at','asc');
                },'replies.user','replies.customer'])->where(function ($query) use ($customerId) {
                    $query->where('customer_id', $customerId);
                })->paginate($per_page);

                $questions->getCollection()->transform(function ($question) {
                    $question = $question->toArray();
                    $question['created_at'] = Carbon::parse($question['created_at'])->format('d/m/Y H:i');
                    $question['updated_at'] = Carbon::parse($question['updated_at'])->format('d/m/Y H:i');
                    foreach ($question['replies'] as &$reply){
                        $reply['created_at'] = Carbon::parse($reply['created_at'])->timezone(config('app.timezone'))->diffForHumans();
                        $reply['updated_at'] = Carbon::parse($reply['updated_at'])->timezone(config('app.timezone'))->diffForHumans();
                    }
                    return $question;
                });

                return response()->json($questions);
            }
        }
        return response()->json([
            'message' => 'Không có dữ liệu'
        ], 500);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function listReply()
    {
        $questionId = request()->get('question_id');
        if ($questionId != null) {
            $question = Question::find($questionId);
            if ($question != null) {
                $replies = Reply::with(['user', 'customer'])->where(function ($query) use ($questionId) {
                    $query->where('question_id', $questionId);
                })->orderBy('created_at', 'asc')->get();

                foreach ($replies as &$reply) {
                    $reply = $reply->toArray();
                    $reply['created_at'] = Carbon::parse($reply['created_at'])->format('d/m/Y H:i');
                    $reply['updated_at'] = Carbon::parse($reply['updated_at'])->format('d/m/Y H:i');
                }

                return response()->json($replies);
            }
        }
        return response()->json([
            'message' => 'Không có dữ liệu'
        ], 500);
    }
}
