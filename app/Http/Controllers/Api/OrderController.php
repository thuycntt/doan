<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Order\CancelOrderRequest;
use App\Http\Requests\Api\Order\CreateOrderRequest;
use App\Mail\OrderSuccessMail;
use App\Models\Customer;
use App\Models\Order;
use App\Models\Product;
use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;

class OrderController extends Controller
{

    public function validateOrder(CreateOrderRequest $request)
    {
        \Log::info($request->bearerToken());
        return response()->json([
            'message' => 'ok'
        ]);
    }

    /**
     * @param CreateOrderRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createOrder(CreateOrderRequest $request)
    {
        $data = $request->validated();
        if (empty($data['products'])) {
            return response()->json([
                'message' => 'products is required'
            ], 422);
        }

        $province = $data['province']->toString();
        $district = $data['district']->toString();
        $ward = $data['ward']->toString();
        $customer = Customer::find($data['customer_id']);
        $shipFee = $data['ward']->ship_fee;
        $delivery_date = Carbon::now()->addDays($data['ward']->delivery_date)->toDateString();


        $dataInsert = [
            'uuid' => $this->generateOrderId(),
            'customer_id' => $data['customer_id'],
            'email' => $data['email'],
            'full_name' => $data['full_name'],
            'phone' => $data['phone'],
            'address' => $data['address'] . ', ' . $ward . ', ' . $district . '. ' . $province,
            'note' => empty($data['note']) ? null : $data['note'],
            'status' => isset($data['transaction']) ? Order::STATUS_GETTING_PRODUCT : Order::STATUS_VERIFY,
            'ship_fee' => $shipFee,
            'delivery_date' => $delivery_date,
            'ward_id' => $data['ward']->id
        ];

        $order = Order::create($dataInsert);

        $products = $data['products'];
        $amount = 0;

//        $productSync = [];
        foreach ($products as $product) {
            $tmpProduct = Product::where('sku', $product['sku'])->first();
            if ($tmpProduct) {
                $amount = $amount + ((int)$tmpProduct->price - ((int)$tmpProduct->price * ((int)$tmpProduct->discount / 100))) * $product['quantity'];
//                $productSync[$tmpProduct->id] = [
//                    'quantity' => $product['quantity']
//                ];
                $dataOrderProduct = [
                    'order_id' => $order->id,
                    'product_id' => $tmpProduct->id,
                    'quantity' => $product['quantity'],
                    'product_price' => $tmpProduct->price,
                    'product_discount' => $tmpProduct->discount
                ];
                \DB::table('order_product')->insert($dataOrderProduct);
            }
            //
            $tmpProduct->update([
                'quantity' => $tmpProduct->quantity - $product['quantity']
            ]);
        };

        // $order->products()->sync($productSync);

        if (isset($data['transaction'])) {
            $data['transaction']->update(['order_id' => $order->id]);
        } else {
            $dataInsertTransaction = [
                'uuid' => $this->generateTransactionId(),
                'type' => Transaction::COD,
                'order_id' => $order->id,
                'customer_id' => $data['customer_id'],
                'transaction_info' => 'Thanh toán COD cho đơn hàng ' . $order['uuid'],
                'data' => null,
                'status' => Transaction::STATUS_PENDING,
                'amount' => $amount
            ];
            $data['transaction'] = Transaction::create($dataInsertTransaction);
        }

        \Mail::to($customer->email)->queue(new OrderSuccessMail($customer, $order, $data['transaction'], $shipFee));

        $order = $order->toArray();
        $order['created_at'] = Carbon::parse($order['created_at'])->format('d/m/Y H:i');
        $order['updated_at'] = Carbon::parse($order['updated_at'])->format('d/m/Y H:i');
        $order['delivery_date'] = Carbon::parse($order['delivery_date'])->format('d/m/Y');

        $transaction = $data['transaction']->toArray();
        $transaction['created_at'] = Carbon::parse($transaction['created_at'])->format('d/m/Y H:i');
        $transaction['updated_at'] = Carbon::parse($transaction['updated_at'])->format('d/m/Y H:i');

        return response()->json([
            'order' => $order,
            'transaction' => $transaction
        ]);
    }

    public function deliveryDate()
    {
        $date = request()->post('number', 0);
        $date = Carbon::now()->addDays($date)->format('d/m/Y');
        return response()->json($date);
    }

    public function cancelOrder(CancelOrderRequest $request)
    {
        $data = $request->validated();
        $order = Order::where('uuid', $data['order_uuid'])->first();
        $transaction = $order->transaction;
        if ($order->customer_id == $data['customer_id'] && $transaction->type == Transaction::COD && $order->status == Order::STATUS_VERIFY) {
            $order->update([
                'cancel_note' => !empty($data['cancel_note']) ? $data['cancel_note'] : null,
                'status' => Order::STATUS_CANCEL
            ]);
            return response()->json(
                [
                    'message' => 'Success'
                ]);
        }
        return response()->json([
            'message' => 'error'
        ], 500);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function listOrderByCustomer()
    {
        $per_page = request()->get('per_page', 10);
        $status = \request()->get('status', null);
        $customerId = request()->get('customer_id');
        if ($customerId != null) {
            $customer = Customer::find($customerId);
            if ($customer != null) {
                $orders = Order::with('transaction', 'customer')->where(function ($query) use ($customerId, $status) {
                    $query->where('customer_id', $customerId);
                    if ($status) {
                        $query->where('status', $status);
                    }
                })->orderBy('created_at', 'desc')->paginate($per_page);

                $orders->getCollection()->transform(function ($order) {
                    $products = $order->products()->withPivot('quantity', 'product_discount', 'product_price')->get()->map(function ($product) {
                        return [
                            'name' => $product->name,
                            'quantity' => $product->pivot->quantity,
                            'price' => $product->pivot->product_price,
                            'discount' => $product->pivot->product_discount,
                            'thumbnail' => asset($product->thumbnail)
                        ];
                    });
                    $order = $order->toArray();
                    $order['created_at'] = Carbon::parse($order['created_at'])->timezone(config('app.timezone'))->format('d/m/Y H:i');
                    $order['updated_at'] = Carbon::parse($order['updated_at'])->timezone(config('app.timezone'))->format('d/m/Y H:i');
                    $order['products'] = $products;
                    $order['delivery_date'] = Carbon::parse($order['delivery_date'])->format('d/m/Y');
                    return $order;
                });

                return response()->json($orders);
            }
        }
        return response()->json([
            'message' => 'Không có dữ liệu'
        ], 500);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function listTransactionByCustomer()
    {
        $per_page = request()->get('per_page', 10);
        $customerId = request()->get('customer_id');
        if ($customerId != null) {
            $customer = Customer::find($customerId);
            if ($customer != null) {
                $transactions = Transaction::where(function ($query) use ($customerId) {
                    $query->where('customer_id', $customerId);
                })->orderBy('created_at', 'desc')->paginate($per_page);

                $transactions->getCollection()->transform(function ($transaction) {
                    $transaction = $transaction->toArray();
                    $transaction['created_at'] = Carbon::parse($transaction['created_at'])->format('d/m/Y H:i');
                    $transaction['updated_at'] = Carbon::parse($transaction['updated_at'])->format('d/m/Y H:i');
                    return $transaction;
                });

                return response()->json($transactions);
            }
        }
        return response()->json([
            'message' => 'Không có dữ liệu'
        ], 500);
    }

    /**
     * @return string
     */
    private function generateOrderId()
    {
        $id = \Str::random(16);
        while (Order::where('uuid', $id)->first() !== null) {
            $id = \Str::random(16);
        }
        return $id;
    }

    /**
     * @return string
     */
    private function generateTransactionId()
    {
        $id = \Str::random(16);
        while (Transaction::where('uuid', $id)->first() !== null) {
            $id = \Str::random(16);
        }
        return $id;
    }
}
