<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Http\Requests\Contact\CreateContactRequest;
use App\Models\Contact;

class ContactController extends Controller
{
    /**
     * @param CreateContactRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createContact(CreateContactRequest $request)
    {
        $data = $request->validated();
        $data['status'] = 0;
        Contact::create($data);
        return response()->json([
            'message' => 'Success'
        ]);
    }
}
