<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\Tag;
use Carbon\Carbon;

class ArticleController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function listArticle()
    {
        $perPage = request()->get('per_page', 10);
        $articles = Article::where('status', 1)->select([
            'id',
            'title',
            'slug',
            'short_content',
            'thumbnail',
            'created_at',
            'author_name',
            'is_feature',
            'status'
        ])->paginate($perPage);

        $articles->getCollection()->transform(function ($article) {
            $article = $article->toArray();
            $article['created_at'] = Carbon::parse($article['created_at'])->format('d/m/Y H:i');
            $article['thumbnail'] = asset($article['thumbnail']);
            return $article;
        });
        return response()->json($articles);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function articleDetail()
    {
        $id = request()->get('id', null);
        if ($id) {
            $article = Article::with('tags')->select()->findOrFail($id)->toArray();
            $article['created_at'] = Carbon::parse($article['created_at'])->format('d/m/Y H:i');
            $article['thumbnail'] = asset($article['thumbnail']);

            $listNews = Article::select([
                'id',
                'title',
                'slug',
                'short_content',
                'thumbnail',
                'created_at',
                'author_name',
                'is_feature',
                'status'
            ])->where(function ($query) use ($id) {
                $query->where('status', Article::STATUS_ACTIVE)
                    ->whereNotIn('id', [$id]);
            })->orderBy('created_at', 'desc')->get(6);

            foreach ($listNews as &$news) {
                $news->thumbnail = asset($news->thumbnail);
            }

            $article['list_news'] = $listNews;

            return response()->json($article);
        }
        return response()->json([
            'message' => 'No data'
        ], 500);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function listTag()
    {
        $per_page = request()->get('per_page', 10);
        $tag = Tag::query()->paginate($per_page);
        return response()->json($tag);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function listArticleByTag()
    {
        $per_page = request()->get('per_page', 10);
        $tagId = request()->get('tag_id');
        if ($tagId != null) {
            $tag = Tag::find($tagId);
            if ($tag != null) {
                $article = $tag->articles()->where('status', 1)->paginate($per_page);
                $article->getCollection()->transform(function ($item) {
                    return [
                        'title' => $item->title,
                        'slug' => $item->slug,
                        'short_content' => $item->short_content,
                        'thumbnail' => asset($item->thumbnail),
                        'id' => $item->id
                    ];
                });
                return response()->json([
                    'data' => $article,
                    'tag' => $tag
                ]);
            }
        }
        return response()->json([
            'message' => 'Không có dữ liệu'
        ], 500);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function listNew()
    {
        $limit = request()->get('number', 6);
        $list = Article::where('status', Article::STATUS_ACTIVE)->orderBy('created_at', 'desc')->select([
            'id',
            'title',
            'slug',
            'short_content',
            'thumbnail',
            'created_at',
            'author_name',
            'status'
        ])->limit($limit)->get();

        $arr = [];

        for ($i = 0; $i < count($list); $i++) {
            $temp = $list[$i];
            $temp = $temp->toArray();
            $temp['thumbnail'] = asset($temp['thumbnail']);
            $temp['created_at'] = Carbon::parse($temp['created_at'])->format('d/m/Y H:i');
            array_push($arr, $temp);
        }
        return response()->json($arr);
    }
}
