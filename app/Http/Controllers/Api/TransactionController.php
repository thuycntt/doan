<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Momo\CreateMomoTransactionRequest;
use App\Http\Requests\Api\Momo\ValidateMomoRequest;
use App\Services\MomoService;

class TransactionController extends Controller
{
    /**
     * @var MomoService
     */
    protected $momo;

    /**
     * TransactionController constructor.
     * @param MomoService $momo
     */
    public function __construct(MomoService $momo)
    {
        $this->momo = $momo;
    }

    /**
     * @param CreateMomoTransactionRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createMomoTransaction(CreateMomoTransactionRequest $request)
    {
        $result = $this->momo->createTransaction($request);

        if ($result !== false) {
            return response()->json($result);
        }

        return response()->json([
            'message' => 'Error!'
        ], 500);
    }

    /**
     * @param ValidateMomoRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function validateMomoTransaction(ValidateMomoRequest $request)
    {
        $result = $this->momo->validateAfterTransaction($request);
        return response()->json($result);
    }
}
