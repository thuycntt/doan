<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Models\District;
use App\Models\Province;
use App\Models\Ward;

class AddressController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function listProvinces()
    {
        $key = request()->get('key', '');
        $provinces = Province::where('name', 'like', "%" . $key . "%")->get(['name', 'slug', 'id', 'prefix']);
        return response()->json($provinces);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function listDistricts()
    {
        $key = request()->get('key', '');
        $provinceId = request()->get('province_id', null);

        if ($provinceId) {
            $districts = District::where(function ($query) use ($key, $provinceId) {
                $query->where('name', 'like', "%" . $key . "%")
                    ->where('province_id', $provinceId);
            })->get(['name', 'slug', 'id', 'prefix', 'province_id']);
            return response()->json($districts);
        }

        return response()->json([]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function listWards()
    {
        $key = request()->get('key', '');
        $districtId = request()->get('district_id', null);
        if ($districtId) {
            $wards = Ward::where(function ($query) use ($key, $districtId) {
                $query->where('name', 'like', "%" . $key . "%")
                    ->where('district_id', $districtId);
            })->get(['name', 'slug', 'id', 'prefix', 'district_id', 'ship_fee','delivery_date']);
            return response()->json($wards);
        }
        return response()->json([]);
    }
}
