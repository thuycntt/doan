<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Models\Slide;
use App\Models\SlideItem;

class SlideController extends Controller
{
    public function getSlide()
    {
        $slideId = request()->get('slide_id', null);
        if ($slideId != null) {
            $slide = Slide::find($slideId);
            if ($slide != null) {
                $slideItems = SlideItem::where(function ($query) use ($slideId) {
                    $query->where('slide_id', $slideId);
                })->orderBy('order', 'asc')->get(['slide_id', 'url', 'image', 'description', 'order', 'status']);

                $slideItems->map(function ($item) {
                    $item->image = asset($item->image);
                    return $item;
                });
                return response()->json(['slide' => $slide, 'slideItem' => $slideItems]);
            }
        }
        return response()->json([
            'message' => 'Không có dữ liệu'
        ], 500);
    }
}
