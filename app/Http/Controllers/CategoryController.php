<?php


namespace App\Http\Controllers;


use App\Http\Requests\Category\CreateCategoryRequest;
use App\Models\Category;

class CategoryController extends Controller
{
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        set_admin_title('Chuyên mục sản phẩm');
        return view('category.index');
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDataNestable()
    {
        return response()->json(Category::allCategories());
    }

    /**
     * @param CreateCategoryRequest $request
     * @return mixed
     */
    public function store(CreateCategoryRequest $request)
    {
        $data = $request->validated();
        if (empty($data['id'])) {
            $max = Category::withoutTrashed()->max('order');
            $max = is_null($max) ? 1 : (intval($max) + 1);
            $data['order'] = $max;
            $insertCategory = [
                'name' => $data['name'],
                'slug' => $data['slug'],
                'type' => $data['type'],
                'description' => empty($data['description']) ? null : $data['description'],
                'order' => $max
            ];
            Category::create($insertCategory);

            return response()->redirectToRoute('category.index')->withNoties([
                'heading' => 'Thành công',
                'message' => 'Tạo mới thành công',
                'type' => 'success'
            ]);
        } else {
            $cat = Category::findOrFail($data['id']);
            $updateCategory = [
                'name' => $data['name'],
                'slug' => $data['slug'],
                'type' => $data['type'],
                'description' => empty($data['description']) ? null : $data['description']
            ];
            $cat->update($updateCategory);
            return response()->redirectToRoute('category.index')->withNoties([
                'heading' => 'Thành công',
                'message' => 'Cập nhật thành công',
                'type' => 'success'
            ]);
        }
    }

    /**
     * @return mixed
     */
    public function updateNestable()
    {
        $data = json_decode(request()->input('data', '[]'), true);
        foreach ($data as $index => $item) {
            $category = Category::withTrashed()->find($item['id']);
            $category->order = ($index + 1);
            $category->save();
        }

        return response()->redirectToRoute('category.index')->withNoties([
            'heading' => 'Thành công',
            'message' => 'Cập nhật thành công',
            'type' => 'success'
        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete()
    {
        try {
            $cat = Category::withTrashed()->findOrFail(request()->input('id'));
            \Schema::disableForeignKeyConstraints();
            $cat->forceDelete();
            \Schema::enableForeignKeyConstraints();
            return response()->json('ok');
        } catch (\Exception $exception) {
            return response()->json('error', 500);
        }

    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function softDelete()
    {
        try {
            $cat = Category::findOrFail(request()->input('id'));
            $cat->delete();
            return response()->json('ok');
        } catch (\Exception $exception) {
            return response()->json('error', 500);
        }
    }

    public function restore()
    {
        try {
            $cat = Category::onlyTrashed()->findOrFail(request()->input('id'));
            $cat->restore();
            return response()->json('ok');
        } catch (\Exception $exception) {
            return response()->json('error', 500);
        }
    }


}
