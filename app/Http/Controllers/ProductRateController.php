<?php


namespace App\Http\Controllers;


use App\Models\ProductRate;

class ProductRateController extends Controller
{
    public function index()
    {
        if (request()->wantsJson()) {
            $query = ProductRate::query()->orderBy('status', 'asc')->orderBy('created_at', 'desc');

            return datatables()->eloquent($query)
                ->addColumn('comment', function ($rate) {
                    return $rate->comment;
                })
                ->addColumn('product', function ($rate) {
                    return '<a target="_blank" href="https://yenvn.info/san-pham/' . $rate->product->slug . '.' . $rate->product->sku . '">' . $rate->product->name . '</a>';
                })
                ->addColumn('actions', function ($rate) {
                    return view('product-rate.table-action', compact('rate'))->render();
                })
                ->addColumn('user', function ($rate) {
                    return $rate->user ? $rate->user->name : 'N/A';
                })
                ->addColumn('customer', function ($rate) {
                    return $rate->customer->name;
                })
                ->addColumn('rate', function ($rate) {
                    return $rate->rate;
                })
                ->addColumn('status', function ($rate) {
                    $class = '';
                    $text = '';
                    switch ($rate->status) {
                        case ProductRate::STATUS_PENDING:
                        {
                            $class = 'warning';
                            $text = 'Chờ duyệt';
                            break;
                        }
                        case ProductRate::STATUS_REJECT:
                        {
                            $class = 'danger';
                            $text = 'Từ chối';
                            break;
                        }
                        case ProductRate::STATUS_ACCEPT:
                        {
                            $class = 'success';
                            $text = 'Đã duyệt';
                            break;
                        }
                    }
                    return '<span class="label label-lg font-weight-bold label-light-' . $class . ' label-inline">' . $text . '</span>';
                })
                ->rawColumns(['actions', 'status', 'product'])
                ->toJson();
        }
        set_admin_title('Quản lý đánh giá sản phẩm');
        return view('product-rate.index');
    }

    public function updateStatus()
    {
        $id = request()->post('id');
        $status = request()->post('status');
        $rate = ProductRate::find($id);
        if ($rate) {
            $rate->update([
                'status' => $status,
                'user_id' => auth()->user()->id
            ]);

            return response()->redirectToRoute('product_rate.index')->withNoties([
                'heading' => 'Thành công',
                'message' => 'Cập nhật thành công',
                'type' => 'success'
            ]);
        }

        return response()->redirectToRoute('product_rate.index')->withNoties([
            'heading' => 'Lỗi',
            'message' => 'Có lỗi xảy ra',
            'type' => 'error'
        ]);
    }
}
