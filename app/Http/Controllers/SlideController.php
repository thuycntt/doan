<?php


namespace App\Http\Controllers;


use App\Http\Requests\Slide\CreateSlideRequest;
use App\Http\Requests\Slide\UpdateSlideRequest;
use App\Models\Slide;
use http\Env\Request;

class SlideController extends Controller
{
    public function index()
    {
        if (request()->wantsJson()) {
            return datatables()->eloquent(Slide::query())
                ->addColumn('position', function ($slide) {
                    return $slide->position == 1 ? 'top' : 'middle';
                })
                ->addColumn('actions', function ($slide) {
                    return view('slide.table-action', compact('slide'));
                })
                ->addColumn('total', function ($slide) {
                    return $slide->slideItems->count();
                })
                ->rawColumns(['thumbnail', 'actions', 'status'])
                ->toJson();
        }
        set_admin_title('Quản lý slide');
        return view('slide.index');
    }

    public function create()
    {
        set_admin_title('Tạo mới slide');
        return view('slide.create');
    }

    public function store(CreateSlideRequest $request)
    {
        $data = $request->validated();
        $insertSlide = [
            'name' => $data['name'],
            'description' => empty($data['description']) ? null : $data['description'],
            'position' => $data['position']
        ];
        Slide::create($insertSlide);
        return response()->redirectToRoute('slide.index')->withNoties([
            'heading' => 'Thành công',
            'message' => 'Tạo mới thành công',
            'type' => 'success'
        ]);
    }

    public function edit($id)
    {
        $slide = Slide::findOrFail($id);
        set_admin_title('Chỉnh sửa slide: ' . $slide->name);
        return view('slide.update', compact('slide'));
    }

    public function update($id, UpdateSlideRequest $request)
    {
        $data = $request->validated();
        $updateSlide = [
            'name' => $data['name'],
            'description' => empty($data['description']) ? null : $data['description'],
            'position' => $data['position']
        ];
        $slide = Slide::findOrFail($id);
        $slide->update($updateSlide);
        return response()->redirectToRoute('slide.index')->withNoties([
            'heading' => 'Thành công',
            'message' => 'Chỉnh sửa thành công',
            'type' => 'success'
        ]);
    }

    public function delete($id)
    {
        $slide = Slide::findOrFail($id);
        $slide->delete();
        return response()->redirectToRoute('slide.index')->withNoties([
            'heading' => 'Thành công',
            'message' => 'Xoá thành công',
            'type' => 'success'
        ]);

    }
}
