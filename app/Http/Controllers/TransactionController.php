<?php


namespace App\Http\Controllers;


use App\Models\Transaction;

class TransactionController extends Controller
{
    public function index()
    {
        if (request()->wantsJson()) {
            return datatables()->eloquent(Transaction::query()->whereIn('status', [Transaction::STATUS_PENDING, Transaction::STATUS_SUCCESS]))
                ->addColumn('customer', function ($transaction) {
                    return $transaction->customer->name;
                })
                ->addColumn('order', function ($transaction) {
                    return '<a href="' . route('order.detail', $transaction->order->id) . '" target="_blank">' . $transaction->order->uuid . '</a>';
                })
                ->addColumn('type', function ($transaction) {
                    return $transaction->type === Transaction::COD ? 'Thanh toán khi nhận hàng (COD) ' : 'Ví Momo';
                })
                ->addColumn('amount', function ($transaction) {
                    return number_format($transaction->amount) . ' VND';
                })
                ->addColumn('status', function ($transaction) {
                    $text = '';
                    switch ($transaction->status) {
                        case Transaction::STATUS_PENDING:
                        {
                            $text = '<span class="label label-lg font-weight-bold label-light-warning label-inline"> Chờ thanh toán</span>';
                            break;
                        }
                        case Transaction::STATUS_SUCCESS:
                        {
                            $text = '<span class="label label-lg font-weight-bold label-light-success label-inline"> Đã thanh toán</span>';
                            break;
                        }
                    }
                    return $text;
                })
                ->rawColumns(['customer', 'actions', 'status', 'order'])
                ->toJson();
        }
        set_admin_title('Quản lý thanh toán');
        return view('transaction.index');
    }
}
