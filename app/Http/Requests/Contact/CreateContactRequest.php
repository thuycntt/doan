<?php
namespace App\Http\Requests\Contact;

use Illuminate\Foundation\Http\FormRequest;

class CreateContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'full_name' => ['required', 'string'],
            'phone' => ['nullable','string'],
            'email' => ['required','email'],
            'content'=>['required','string']
        ];
    }

    /**
     * @return array|string[]
     */
    public function attributes()
    {
        return [
            'full_name' => 'Tên',
            'email' => 'Email',
            'content'=>'Nội dung'
        ];
    }

    /**
     * @return array
     */

}
