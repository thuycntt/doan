<?php

namespace App\Http\Requests\Article;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;

class CreateArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['required', 'string'],
            'short_content' => ['required', 'string'],
            'content' => ['required'],
            'thumbnail' => ['required'],
            'tags' => ['required'],
            'status' => ['required'],
            'is_feature' => ['nullable']
        ];
    }

    /**
     * @return array|string[]
     */
    public function attributes()
    {
        return [
            'title' => 'tên',
            'short_content' => 'mô tả ngắn',
            'content' => 'nội dung',
            'thumbnail' => 'ảnh đại diện',
            'tags' => 'tag',
            'status' => 'trạng thái'
        ];
    }

    /**
     * @return array
     */
    public function validated()
    {
        $data = parent::validated();
        $data['slug'] = Str::slug($data['title']);
        if (empty($data['is_feature']))
            $data['is_feature'] = 0;
        $data['author_name'] = \Auth::user()->username;
        $data['author_id'] = \Auth::user()->id;
        $arrReplace = [
            env('APP_URL') => '',
            'http://' => '',
            'https://' => ''
        ];
        $data['thumbnail'] = str_replace(array_keys($arrReplace), array_values($arrReplace), $data['thumbnail']);
        return $data;
    }
}
