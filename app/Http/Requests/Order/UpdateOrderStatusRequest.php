<?php

namespace App\Http\Requests\Order;

use Illuminate\Foundation\Http\FormRequest;

class UpdateOrderStatusRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'order_uuid' => ['required', 'exists:orders,uuid'],
            'status' => ['required']
        ];
    }

    /**
     * @return array|string[]
     */
    public function attributes()
    {
        return [
            'order_uuid' => 'mã đơn hàng',
            'status' => 'trạng thái'
        ];
    }
}
