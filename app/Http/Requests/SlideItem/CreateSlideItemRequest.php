<?php

namespace App\Http\Requests\SlideItem;

use Illuminate\Foundation\Http\FormRequest;

class CreateSlideItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'url' => ['required', 'url'],
            'description' => ['nullable'],
            'status' => ['required'],
            'image' => ['required']
        ];
    }

    /**
     * @return array|string[]
     */
    public function attributes()
    {
        return [
            'url' => 'tên',
            'order' => 'Thứ tự',
            'description' => 'mô tả',
            'status' => 'Trạng thái'
        ];
    }
}
