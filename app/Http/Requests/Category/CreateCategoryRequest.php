<?php

namespace App\Http\Requests\Category;

use Illuminate\Foundation\Http\FormRequest;

class CreateCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string'],
            'description' => ['nullable'],
            'type' => ['required'],
            'id' => ['nullable']
        ];
    }

    /**
     * @return array|string[]
     */
    public function attributes()
    {
        return [
            'name' => 'tên',
            'description' => 'mô tả'
        ];
    }

    /**
     * @return array
     */
    public function validated()
    {
        $data = parent::validated();
        $data['slug'] = \Str::slug($data['name']);
        return $data;
    }
}
