<?php
namespace App\Http\Requests\Api\Customer;

use Illuminate\Foundation\Http\FormRequest;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;



class UpdatePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'customer_id' => ['required', 'exists:customers,id'],
            'password' => ['required', 'min:8','confirmed'],
            'old_password' => ['required'],
            'password_confirmation' => ['required'],
        ];
    }

    public function attributes()
    {
        return [
            'password' => 'mật khẩu mới',
            'old_password' => 'mật khẩu cũ',
            'password_confirmation' => 'nhập lại mật khẩu'
        ];
    }

    /**
     * @param Validator $validator
     */
    protected function failedValidation(Validator $validator)
    {
        $errors = $validator->errors();

        throw new HttpResponseException(
            response()->json(['errors' => $errors], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }
}
