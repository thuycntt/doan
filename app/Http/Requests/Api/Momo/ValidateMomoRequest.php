<?php


namespace App\Http\Requests\Api\Momo;


use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;

class ValidateMomoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return \string[][]
     */
    public function rules()
    {
        return [
            'partnerCode' => ['required'],
            'accessKey' => ['required'],
            'orderId' => ['required'],
            'localMessage' => ['required'],
            'message' => ['required'],
            'transId' => ['required'],
            'orderInfo' => ['required'],
            'amount' => ['required'],
            'errorCode' => ['required'],
            'responseTime' => ['required'],
            'requestId' => ['required'],
            'payType' => ['required'],
            'orderType' => ['required'],
            'extraData' => ['required'],
            'signature' => ['required']
        ];
    }

    /**
     * @param Validator $validator
     */
    protected function failedValidation(Validator $validator)
    {
        $errors = $validator->errors();

        throw new HttpResponseException(
            response()->json(['errors' => $errors], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }

}
