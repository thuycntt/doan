<?php

namespace App\Http\Requests\Api\Order;

use App\Models\District;
use App\Models\Province;
use App\Models\Transaction;
use App\Models\Ward;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;

class CreateOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return \string[][]
     */
    public function rules()
    {
        return [
            'customer_id' => ['required', 'exists:customers,id'],
            'email' => ['required', 'email'],
            'full_name' => ['required', 'string'],
            'phone' => ['required', 'string'],
            'address' => ['required', 'string'],
            'province_id' => ['required', 'exists:provinces,id'],
            'district_id' => ['required', 'exists:districts,id'],
            'ward_id' => ['required', 'exists:wards,id'],
            'transaction_uuid' => ['nullable', 'exists:transactions,uuid'],
            'note' => ['nullable', 'string'],
            'products' => ['required', 'string']
        ];
    }

    /**
     * @param Validator $validator
     */
    protected function failedValidation(Validator $validator)
    {
        $errors = $validator->errors();

        throw new HttpResponseException(
            response()->json(['errors' => $errors], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }

    /**
     * @return array
     */
    public function validated()
    {
        $data = parent::validated();
        $data['province'] = Province::find($data['province_id']);
        $data['district'] = District::find($data['district_id']);
        $data['ward'] = Ward::find($data['ward_id']);
        if (empty($data['note'])) $data['note'] = null;
        if (isset($data['transaction_uuid'])) $data['transaction'] = Transaction::where('uuid', $data['transaction_uuid'])->first();
        $data['products'] = json_decode($data['products'], true);


        return $data;
    }

    public function attributes()
    {
        return [
            'province_id' => 'tỉnh/thành',
            'district_id' => 'quận/huyện',
            'ward_id' => 'phường/xã',
            'address' => 'địa chỉ'
        ];
    }
}
