<?php

namespace App\Http\Requests\MenuItem;

use Illuminate\Foundation\Http\FormRequest;

class MenuItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'url' => ['required', 'string'],
            'description' => ['nullable'],
            'name' => ['required'],
            'id' => ['nullable'],
            'menu_id'=>['required']
        ];
    }

    /**
     * @return array|string[]
     */
    public function attributes()
    {
        return [
            'url' => 'Liên kết',
            'description' => 'mô tả',
            'name' => 'Tên'
        ];
    }
}
