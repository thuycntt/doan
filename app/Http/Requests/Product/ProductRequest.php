<?php

namespace App\Http\Requests\Product;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Response;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sku' => ['nullable'],
            'auto_sku' => ['nullable'],
            'name' => ['required'],
            'categories' => ['required', 'array'],
            'price' => ['required'],
            'quantity' => ['required'],
            'description' => ['required'],
            'thumbnail' => ['required'],
            'images' => ['required', 'string'],
            'discount' => ['nullable', 'min:0', 'max:50'],
            'trending'=>['nullable']
        ];
    }

    /**
     * @return array|string[]
     */
    public function attributes()
    {
        return [
            'sku' => 'SKU',
            'category_id' => 'Chuyên mục',
            'name' => 'Tên',
            'price' => 'Giá',
            'quantity' => 'Số lương',
            'description' => 'Mô tả',
            'thumbnail' => 'Hình đại diện',
            'images' => 'Hình sản phẩm',
            'discount' => 'Giảm giá'
        ];
    }

    public function validated()
    {
        $data = parent::validated();
        $data['slug'] = \Str::slug($data['name']);
        $data['price'] = str_replace(',', '', $data['price']);
        if (empty($data['discount'])) {
            $data['discount'] = 0;
        }
        if(empty($data['trending'])){
            $data['trending'] = 0;
        }else{
            $data['trending']=1;
        }
        return $data;
    }
}
