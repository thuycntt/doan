<?php


namespace App\Http\Requests\User;


use Illuminate\Foundation\Http\FormRequest;

class UpdatePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'password' => ['required', 'min:8','confirmed'],
            'old_password' => ['required'],
            'password_confirmation' => ['required'],
        ];
    }
}
