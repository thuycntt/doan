<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;


class UserUpdateInfoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'dob' => ['nullable', 'date_format:Y-m-d'],
            'phone' => ['nullable'],
            'about' => ['nullable'],
            'avatar' => ['nullable','file', 'mimes:jpg,png,jpeg'],
            'name' => ['nullable']
        ];
    }


}
