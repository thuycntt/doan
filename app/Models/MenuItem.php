<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MenuItem extends Model
{
    use SoftDeletes;

    /**
     * @var string[]
     */
    protected $fillable = [
        'name',
        'url',
        'parent_id',
        'description',
        'order',
        'menu_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children()
    {
        return $this->hasMany(MenuItem::class, 'parent_id', 'id')->orderBy('order', 'asc')->with(['children']);
    }

    /**
     * @param Menu $menu
     * @return mixed
     */
//    public static function siblingsNext(Menu $menu)
//    {
//        return static::where(function ($query) use ($menu) {
//            $query->where('parent_id', $menu->parent_id)
//                ->where('order', '>', $menu->order)
//                ->whereNotIn('id', [$menu->id]);
//            return $query;
//        })->orderBy('order')->get();
//    }

    /**
     * @param $menu
     * @return MenuItem[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Query\Builder[]|\Illuminate\Support\Collection
     */
    public static function allMenus($menu)
    {
        return static::withTrashed()->where(function ($query) use ($menu) {
            $query->where('menu_id', $menu)
                ->whereNull('parent_id');
            return $query;
        })
            ->with(['children' => function ($query) {
                $query->withTrashed();
                return $query;
            }])
            ->orderBy('order', 'ASC')
            ->get(['id', 'name', 'parent_id', 'description', 'deleted_at', 'url', 'menu_id']);
    }

    /**
     * @param $menu
     * @return mixed
     */
    public static function menuFrontend($menu)
    {
        return static::where(function ($query) use ($menu) {
            $query->where('menu_id', $menu)
                ->whereNull('parent_id');
            return $query;
        })->with('children')->orderBy('order', 'ASC')->get(['id', 'name', 'parent_id', 'description', 'url', 'menu_id']);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function menu()
    {
        return $this->belongsTo(Menu::class, 'menu_id', 'id');
    }
}
