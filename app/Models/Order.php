<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    const STATUS_VERIFY = 1;
    const STATUS_PENDING = 2;
    const STATUS_GETTING_PRODUCT = 3;
    const STATUS_SHIPPING = 4;
    const STATUS_CANCEL = 6;
    const STATUS_COMPLETE = 5;

    protected $fillable = ['uuid', 'customer_id', 'email', 'full_name', 'phone', 'address', 'note', 'status','ship_fee','cancel_note','delivery_date','ward_id', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function transaction()
    {
        return $this->hasOne(Transaction::class, 'order_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function products()
    {
        return $this->belongsToMany(Product::class, 'order_product', 'order_id', 'product_id');
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id', 'id');
    }

}
