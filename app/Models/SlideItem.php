<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class SlideItem extends Model
{
    protected $fillable = [
        'slide_id', 'url', 'image', 'description', 'order', 'status'
    ];

    public function slide()
    {
        return $this->belongsTo(Slide::class, 'slide_id', 'id');
    }
}
