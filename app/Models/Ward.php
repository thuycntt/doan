<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Ward extends Model
{
    protected $fillable = [
        'name',
        'slug',
        'district_id',
        'prefix',
        'ship_fee',
        'delivery_date'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function district()
    {
        return $this->belongsTo(District::class, 'district_id', 'id');
    }

    /**
     * @return string
     */
    public function toString()
    {
        return $this->prefix . ' ' . $this->name;
    }
}
