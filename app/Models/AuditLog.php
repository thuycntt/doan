<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * Class AuditLog
 * @package App\Models
 */
class AuditLog extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'content',
        'user_agent',
        'ip'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
