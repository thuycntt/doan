<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    const POSITION_TOP = 1;
    /**
     * @var string[]
     */
    protected $fillable = [
        'name',
        'description',
        'position'
    ];

    static function alPositions()
    {
        return [
            self::POSITION_TOP => 'Top'
        ];
    }
}
