<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Slide extends Model
{
    protected $fillable = [
        'name', 'description', 'position'
    ];

    public function slideItems()
    {
        return $this->hasMany(SlideItem::class, 'slide_id', 'id')->orderBy('order','asc');
    }
}


