<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    /**
     * @var string[]
     */
    protected $fillable = [
        'full_name',
        'phone',
        'email',
        'content',
        'created_at',
        'status',
        'reply_by'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function replyBy()
    {
        return $this->belongsTo(User::class, 'reply_by', 'id');
    }
}
