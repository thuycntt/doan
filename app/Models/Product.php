<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    /**
     * @var string[]
     */
    protected $fillable = ['id','sku', 'name', 'slug', 'category_id', 'price', 'quantity', 'description', 'trending','short_description', 'thumbnail', 'discount'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    function productImages()
    {
        return $this->hasMany(ProductImage::class, 'product_id', 'id');
    }

    /***
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class, 'category_product', 'product_id', 'category_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function productRates()
    {
        return $this->hasMany(ProductRate::class, 'product_id', 'id');
    }
}
