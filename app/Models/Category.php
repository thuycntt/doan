<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;

    const TYPE_FOOD = 1;
    const TYPE_EQUIPMENT = 2;

    protected $fillable = [
        'name',
        'slug',
        'type',
        'description',
        'order'
    ];
    /**
     * @return mixed
     */
    public static function allCategories()
    {
        return static::withTrashed()
            ->orderBy('order', 'ASC')
            ->get(['id', 'name', 'slug', 'type', 'description', 'deleted_at']);
    }

    public static function allCategoriesFrontend()
    {
        return static::sorderBy('order', 'ASC')->get(['id', 'name', 'slug', 'type', 'description']);
    }
}
