<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class CustomerDiary extends Model
{
    protected $fillable = [
        'note',
        'images',
        'customer_id',
        'seen_at',
        'seen_by',
        'status',
        'date'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function seenBy()
    {
        return $this->belongsTo(User::class, 'seen_by', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function replies()
    {
        return $this->hasMany(Reply::class, 'diary_id', 'id');
    }
}
