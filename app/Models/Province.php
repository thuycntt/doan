<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Province extends Model
{

    protected $fillable = [
        'name',
        'slug',
        'prefix',
        'ship_fee'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function districts()
    {
        return $this->hasMany(District::class, 'district_id', 'id');
    }

    /**
     * @return string
     */
    public function toString()
    {
        return $this->prefix . ' ' . $this->name;
    }
}
