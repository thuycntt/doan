<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class ProductRate extends Model
{

    const STATUS_PENDING = 0;
    const STATUS_ACCEPT = 10;
    const STATUS_REJECT = 1;

    protected $fillable = [
        'product_id',
        'customer_id',
        'rate',
        'comment',
        'status',
        'user_id',
        'created_at',
        'updated_at'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}

