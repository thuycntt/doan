<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    protected $fillable = [
        'name',
        'slug',
        'prefix',
        'province_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function province()
    {
        return $this->belongsTo(Province::class, 'province_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function wards()
    {
        return $this->hasMany(Ward::class, 'district_id', 'id');
    }

    /**
     * @return string
     */
    public function toString()
    {
        return $this->prefix . ' ' . $this->name;
    }

}
