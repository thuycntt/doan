<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Class AdminPageTitleFacade
 * @package App\Facades
 * @method static setPageTitle($title)
 * @method static getPageTitle($full = true)
 */
class AdminPageTitleFacade extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'page-title';
    }
}
